/**
 *
 *  This object is responsible for sending postMessage between the iframe and the extension
 *
 */

var msg = {
  /**
   *
   *  @param     objMessage      Object      Message object to send to the postMessage
   *
   *  @return                    Boolean     True or False
   *
   */
  sendMessage : function(objMessage = false) {
    if (objMessage !== false) {
      // Send the message to the parent window
      window.top.postMessage(objMessage, '*');
    } else {
      return false;
    }
  }
}

window.addEventListener("load", function() {
  // If we are on the payment successful page then postMessage below...
  let paymentSuccessful = document.getElementById("paymentSuccessful");
  if (paymentSuccessful) {
    // Create the message object to be sent
    var objMessage = {"status" : "success", "message" : "payment successful"};
    // Send the message to the parent window
    msg.sendMessage(objMessage);
  }
  // If #btnContinue is found in the DOM postMessage betlow...
  let btnContinue = document.querySelector("#btnContinue");
  if (btnContinue !== null && btnContinue !== undefined) {
    btnContinue.addEventListener("click", function() {
      var objMessage = {"status" : "success", "message" : "redirect login"};
      msg.sendMessage(objMessage);
    });
  }
});
