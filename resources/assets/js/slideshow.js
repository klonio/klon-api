var slideshow = {
  /**
   *
   *  Returns the specific value from window.location.search
   *  Example: https://api.klon.io/resources/views/upgrade.php?pid=1&step=2
   *  If you call slideshow.getUrlSearch("pid") the function will return the
   *  value of 1
   *
   */
  getUrlSearch : function(target = false) {
    // Determine what step we are on and show the correct slide
    let currentUrl  = new URLSearchParams(window.location.search);
    if (target !== false) {
      if (typeof target === "string") {
        if (currentUrl.get(target) !== null) {
          return currentUrl.get(target);
        } else {
          throw "target not found";
        }
      } else {
        throw "target is not a string";
      }
    } else {
      throw "valid target not supplied";
    }
  },

  /**
   *
   *  Set the current slide to the proper value
   *
   *  @param       slide       Number       The slide to set the slideshow to
   *
   *  @return                  Boolean      Returns true if successful or throws error
   *
   */
  setCurrentSlide : function(slideNumber = false) {
    // Check to see if slide is a number
    if (typeof slideNumber === "number" && !isNaN(slideNumber)) {
      // Get the slideshow element
      let elSlideshow = document.querySelector("#slideshow");
      // Make sure the slideshow is there
      if (elSlideshow !== null && elSlideshow !== undefined) {
        // Set the current slide
        try {
          elSlideshow.classList = "slideshow " + "pay--" + slideNumber;
          return true;
        } catch (e) {
          throw e;
        }
      } else {
        throw "slideshow element not found";
      }
    } else {
      throw "slideNumber must be a number";
    }
  },


  /**
   *
   *  Functionality for the navigation buttons
   *
   */
  navigateForward : function() {
    history.forward();
  },

  navigateBack : function() {
    history.back();
    return true;
  }
}


window.addEventListener("load", function() {
  let btnBack = document.querySelector("#btnNavBack");
  if (btnBack) {
    btnBack.addEventListener("click", slideshow.navigateBack);
  }
  //
  let btnForward = document.querySelector("#btnNavForward");
  if (btnForward) {
    btnForward.addEventListener("click", slideshow.navigateForward);
  }
  });
