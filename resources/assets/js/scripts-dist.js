"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

//@prepros-append slideshow.js
//@prepros-append payment.js
//@prepros-append loader.js

var slideshow = {
  /**
   *
   *  Returns the specific value from window.location.search
   *  Example: https://api.klon.io/resources/views/upgrade.php?pid=1&step=2
   *  If you call slideshow.getUrlSearch("pid") the function will return the
   *  value of 1
   *
   */
  getUrlSearch: function getUrlSearch() {
    var target = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

    // Determine what step we are on and show the correct slide
    var currentUrl = new URLSearchParams(window.location.search);
    if (target !== false) {
      if (typeof target === "string") {
        if (currentUrl.get(target) !== null) {
          return currentUrl.get(target);
        } else {
          throw "target not found";
        }
      } else {
        throw "target is not a string";
      }
    } else {
      throw "valid target not supplied";
    }
  },

  /**
   *
   *  Set the current slide to the proper value
   *
   *  @param       slide       Number       The slide to set the slideshow to
   *
   *  @return                  Boolean      Returns true if successful or throws error
   *
   */
  setCurrentSlide: function setCurrentSlide() {
    var slideNumber = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

    // Check to see if slide is a number
    if (typeof slideNumber === "number" && !isNaN(slideNumber)) {
      // Get the slideshow element
      var elSlideshow = document.querySelector("#slideshow");
      // Make sure the slideshow is there
      if (elSlideshow !== null && elSlideshow !== undefined) {
        // Set the current slide
        try {
          elSlideshow.classList = "slideshow " + "pay--" + slideNumber;
          return true;
        } catch (e) {
          throw e;
        }
      } else {
        throw "slideshow element not found";
      }
    } else {
      throw "slideNumber must be a number";
    }
  },

  /**
   *
   *  Functionality for the navigation buttons
   *
   */
  navigateForward: function navigateForward() {
    history.forward();
  },

  navigateBack: function navigateBack() {
    history.back();
    return true;
  }
};

window.addEventListener("load", function () {
  var btnBack = document.querySelector("#btnNavBack");
  if (btnBack) {
    btnBack.addEventListener("click", slideshow.navigateBack);
  }
  //
  var btnForward = document.querySelector("#btnNavForward");
  if (btnForward) {
    btnForward.addEventListener("click", slideshow.navigateForward);
  }
});

// Create a Stripe client.
var stripe = Stripe('pk_live_vN2Hje6AxvIjweLM0ifAJHvF00lzoBzL7J');
// Set the stripePlan
var stripePlan = document.querySelector("#stripePlan");

// Create an instance of Elements.
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    fontFamily: '"Montserrat", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element.
var card = elements.create('card', { style: style });

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function (event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission.
var form = document.getElementById('payment-form');
form.addEventListener('submit', function (event) {
  event.preventDefault();

  stripe.createToken(card).then(function (result) {
    if (result.error) {
      // Inform the user if there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
    }
  });
});

// Submit the form with the token ID.
function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('payment-form');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);

  // Submit the form
  form.submit();
}

/**

// Determine what step we are on and show the correct slide
let searchParams = new URLSearchParams(window.location.search);
// Need the step so we know what to display
let currentStep  = searchParams.get('step');
// Need the product ID so we know how much to charge the user
let productId    = searchParams.get('pid');

*/

window.addEventListener('load', function () {
  //
  try {
    // Fill out the stripeUid and stripeEmail fields
    // let uid   = document.querySelector("#stripeUid");
    // let email = document.querySelector("#stripeEmail");
    // Get the buttons
    var btnBasic = document.querySelector("#btnBuyBasic");
    var btnRegular = document.querySelector("#btnBuyRegular");
    var btnPremium = document.querySelector("#btnBuyPremium");
    //
    var btnAgreeTerms = document.querySelector("#btnAgreeTerms");
    var chkAgreeTerms = document.querySelector("#chkAgreeTerms");
    // Get the payment method buttons
    var btnPayWithCredit = document.querySelector("#payWithCredit");
    var btnPayWithCrypto = document.querySelector("#payWithCrypto");
    //
    // email.value = localStorage.getItem("user") || false;
    // uid.value   = localStorage.getItem("token") || false;
    //
    chkAgreeTerms.addEventListener("change", function () {
      if (chkAgreeTerms.checked === true) {
        btnAgreeTerms.disabled = false;
      } else {
        btnAgreeTerms.disabled = true;
      }
    });
    btnAgreeTerms.addEventListener("click", function () {
      var pid = slideshow.getUrlSearch('pid');
      window.history.pushState({}, '', '/?pid=' + pid + '&step=3');
      window.dispatchEvent(new Event('popstate'));
      slideshow.setCurrentSlide(3);
    });
    //
    if (btnBasic !== null && btnBasic !== undefined) {
      btnBasic.addEventListener("click", function () {
        window.history.pushState({}, '', '/?pid=1&step=2');
        console.log("Slideshow: ", slideshow);
        var step = slideshow.getUrlSearch('step');
        window.dispatchEvent(new Event('popstate'));
      });
    }
    if (btnRegular !== null && btnRegular !== undefined) {
      btnRegular.addEventListener("click", function () {
        window.history.pushState({}, '', '/?pid=2&step=2');
        var step = slideshow.getUrlSearch('step');
        window.dispatchEvent(new Event('popstate'));
      });
    }
    if (btnPremium !== null && btnPremium !== undefined) {
      btnPremium.addEventListener("click", function () {
        window.history.pushState({}, '', '/?pid=3&step=2');
        var step = slideshow.getUrlSearch('step');
        window.dispatchEvent(new Event('popstate'));
      });
    }
    //
    if (btnPayWithCredit !== null && btnPayWithCredit !== undefined) {
      btnPayWithCredit.addEventListener("click", function () {
        var pid;
        try {
          pid = slideshow.getUrlSearch('pid');
          window.history.pushState({}, '', '/?pid=' + pid + '&step=4');
          window.dispatchEvent(new Event('popstate'));
        } catch (e) {
          pid = false;
          window.history.pushState({}, '', '/?step=1');
          window.dispatchEvent(new Event('popstate'));
        }
      });
    }
    // if (btnPayWithCrypto !== null && btnPayWithCrypto !== undefined) {
    //
    // }
  } catch (e) {
    console.log("Error: ", e);
    return false;
  }
  return true;
});

window.addEventListener('popstate', function () {
  try {
    var step = slideshow.getUrlSearch('step');
    if (typeof Number(step) === "number" && !isNaN(Number(step))) {
      slideshow.setCurrentSlide(Number(step));
    }
    if (stripePlan !== null && stripePlan !== undefined) {
      var planId = slideshow.getUrlSearch("pid");
      if (planId !== null && planId !== undefined) {
        stripePlan.value = planId;
      }
    }
  } catch (e) {
    // If there's an error set the slide to 1
    slideshow.setCurrentSlide(1);
  }
  console.log("Stripe Plan: ", slideshow.getUrlSearch("pid"));
  console.log("Stripe Plan Field: ", stripePlan);
});

/**
 *
 *  This is the server side payment object
 *  Handles all the payment activity between the server and the client
 *
 */

var payment = {
  /**
   *
   *  Successful payment
   *
   *  @param       objMessage      Object         This is an object containing important information about the transaction
   *                                              Ex: {"status" : "success", "message" : "payment successful"}
   *
   *  @return                      Boolean
   *
   */
  sendMessage: function sendMessage() {
    var objMessage = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

    // Make sure a message object was passed in
    if (objMessage !== false && (typeof objMessage === "undefined" ? "undefined" : _typeof(objMessage)) === 'object') {
      // Send the message
      window.top.postMessage(objMessage, '*');
    } else {
      return false;
    }
  }
};

/**
 *
 *  Makes the loading page function
 *
 */
var loader = {

  // Return the loader element
  getLoader: function getLoader() {
    var loader = document.querySelector("#loader");
    if (loader !== null && loader !== undefined) {
      return loader;
    } else {
      return false;
    }
  },

  // The page has loaded time to destroy the loader element in the DOM
  pageLoaded: function pageLoaded() {
    var elLoader = loader.getLoader();
    if (elLoader !== false) {
      // Get the dots
      var dot1 = document.querySelector("#dot1");
      var dot2 = document.querySelector("#dot2");
      var text = document.querySelector(".loader__text");
      // Remove the dot--1 and dot--2 classes and add loader__dot--closed
      if (dot1 !== null && dot2 !== null) {
        dot1.classList.remove('loader__dot--1');
        dot1.classList.add('loader__dot--loaded');
        text.classList.add('loader__text--loaded');
        // If the element was found add the finished classes
        try {
          //
          dot2.remove();
          // Add the --closed class
          console.log("done");
        } catch (e) {
          console.log("Could not remove '#loader' from DOM.");
          console.log("Error: ", e);
        }
      }
      return true;
    } else {
      return true;
    }
  }

  // Run this dude when the page loads
};window.addEventListener("load", function () {
  loader.pageLoaded();
  var t = setTimeout(function () {
    var elLoader = document.querySelector("#loader");
    elLoader.style.zIndex = "-1";
  }, 350);
});