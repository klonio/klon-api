/**
 *
 *  Makes the loading page function
 *
 */
var loader = {


  // Return the loader element
  getLoader : function () {
    let loader = document.querySelector("#loader");
    if (loader !== null && loader !== undefined) {
      return loader;
    } else {
      return false;
    }
  },


  // The page has loaded time to destroy the loader element in the DOM
  pageLoaded : function() {
    let elLoader = loader.getLoader();
    if (elLoader !== false) {
      // Get the dots
      var dot1 = document.querySelector("#dot1");
      var dot2 = document.querySelector("#dot2");
      var text = document.querySelector(".loader__text");
      // Remove the dot--1 and dot--2 classes and add loader__dot--closed
      if (dot1 !== null && dot2 !== null) {
        dot1.classList.remove('loader__dot--1');
        dot1.classList.add('loader__dot--loaded');
        text.classList.add('loader__text--loaded');
        // If the element was found add the finished classes
        try {
          //
          dot2.remove();
          // Add the --closed class
          console.log("done");
        } catch (e) {
          console.log("Could not remove '#loader' from DOM.");
          console.log("Error: ", e);
        }
      }
      return true;
    } else {
      return true;
    }
  }
}

// Run this dude when the page loads
window.addEventListener("load", function() {
  loader.pageLoaded();
  var t = setTimeout(
    function() {
      if (document.querySelector("#loader")) {
        try {
          var elLoader = document.querySelector("#loader");
          elLoader.style.zIndex = "-1";
        } catch(err) {
          console.log("Error: ", err.message);
        }
      }
    }, 350
  );
});
