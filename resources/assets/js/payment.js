// Create a Stripe client.
var stripe = Stripe('pk_live_vN2Hje6AxvIjweLM0ifAJHvF00lzoBzL7J');
// Set the stripePlan
var stripePlan = document.querySelector("#stripePlan");

// Create an instance of Elements.
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    fontFamily: '"Montserrat", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element.
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission.
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
  event.preventDefault();

  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
    }
  });
});

// Submit the form with the token ID.
function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('payment-form');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);

  // Submit the form
  form.submit();
}

/**

// Determine what step we are on and show the correct slide
let searchParams = new URLSearchParams(window.location.search);
// Need the step so we know what to display
let currentStep  = searchParams.get('step');
// Need the product ID so we know how much to charge the user
let productId    = searchParams.get('pid');

*/

window.addEventListener('load', function() {
  //
  try {
    // Fill out the stripeUid and stripeEmail fields
    // let uid   = document.querySelector("#stripeUid");
    // let email = document.querySelector("#stripeEmail");
    // Get the buttons
    let btnBasic         = document.querySelector("#btnBuyBasic");
    let btnRegular       = document.querySelector("#btnBuyRegular");
    let btnPremium       = document.querySelector("#btnBuyPremium");
    //
    let btnAgreeTerms    = document.querySelector("#btnAgreeTerms");
    let chkAgreeTerms    = document.querySelector("#chkAgreeTerms");
    // Get the payment method buttons
    let btnPayWithCredit = document.querySelector("#payWithCredit");
    let btnPayWithCrypto = document.querySelector("#payWithCrypto");
    //
    // email.value = localStorage.getItem("user") || false;
    // uid.value   = localStorage.getItem("token") || false;
    //
    chkAgreeTerms.addEventListener("change", function() {
      if (chkAgreeTerms.checked === true) {
        btnAgreeTerms.disabled = false;
      } else {
        btnAgreeTerms.disabled = true;
      }
    });
    btnAgreeTerms.addEventListener("click", function() {
      var pid = slideshow.getUrlSearch('pid');
      window.history.pushState({} , '', '/?pid=' + pid + '&step=3');
      window.dispatchEvent(new Event('popstate'));
      slideshow.setCurrentSlide(3);
    });
    //
    if (btnBasic !== null && btnBasic !== undefined) {
      btnBasic.addEventListener("click", function() {
        window.history.pushState({} , '', '/?pid=1&step=2');
        console.log("Slideshow: ", slideshow);
        var step = slideshow.getUrlSearch('step');
        window.dispatchEvent(new Event('popstate'));
      });
    }
    if (btnRegular !== null && btnRegular !== undefined) {
      btnRegular.addEventListener("click", function() {
        window.history.pushState({} , '', '/?pid=2&step=2');
        var step = slideshow.getUrlSearch('step');
        window.dispatchEvent(new Event('popstate'));
      });
    }
    if (btnPremium !== null && btnPremium !== undefined) {
      btnPremium.addEventListener("click", function() {
        window.history.pushState({} , '', '/?pid=3&step=2');
        var step = slideshow.getUrlSearch('step');
        window.dispatchEvent(new Event('popstate'));
      });
    }
    //
    if (btnPayWithCredit !== null && btnPayWithCredit !== undefined) {
      btnPayWithCredit.addEventListener("click", function() {
        var pid;
        try {
          pid = slideshow.getUrlSearch('pid');
          window.history.pushState({} , '', '/?pid=' + pid + '&step=4');
          window.dispatchEvent(new Event('popstate'));
        } catch(e) {
          pid = false;
          window.history.pushState({} , '', '/?step=1');
          window.dispatchEvent(new Event('popstate'));
        }
      });
    }
    // if (btnPayWithCrypto !== null && btnPayWithCrypto !== undefined) {
    //
    // }
  } catch(e) {
    console.log("Error: ", e);
    return false;
  }
  return true;
});

window.addEventListener('popstate', function() {
  try {
    var step = slideshow.getUrlSearch('step');
    if (typeof Number(step) === "number" && !isNaN(Number(step))) {
      slideshow.setCurrentSlide(Number(step))
    }
    if (stripePlan !== null && stripePlan !== undefined) {
      var planId =  slideshow.getUrlSearch("pid");
      if (planId !== null && planId !== undefined) {
        stripePlan.value = planId;
      }
    }
  } catch (e) {
    // If there's an error set the slide to 1
    slideshow.setCurrentSlide(1);
  }
  console.log("Stripe Plan: ", slideshow.getUrlSearch("pid"));
  console.log("Stripe Plan Field: ", stripePlan);
});


/**
 *
 *  This is the server side payment object
 *  Handles all the payment activity between the server and the client
 *
 */

var payment = {
  /**
   *
   *  Successful payment
   *
   *  @param       objMessage      Object         This is an object containing important information about the transaction
   *                                              Ex: {"status" : "success", "message" : "payment successful"}
   *
   *  @return                      Boolean
   *
   */
   sendMessage : function(objMessage = false) {
     // Make sure a message object was passed in
     if (objMessage !== false && typeof objMessage === 'object') {
       // Send the message
       window.top.postMessage(objMessage, '*');
     } else {
       return false;
     }
   }
};
