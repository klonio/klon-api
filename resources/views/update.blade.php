<!DOCTYPE html>
<html>
  <head>
    <title>Klon</title>
    <link href={{ url('../resources/assets/css/style.css') }} type="text/css" rel="stylesheet">
    <meta charset="utf-8" lang="en">
    <meta content="width=device-width,initial-scale=1,shrink-to-fit=no" name="viewport">
  </head>
  <body>
    <!-- LOADER -->
    <div class="loader" id="loader">
      <div class="loader__dot loader__dot--1" id="dot1"></div>
      <div class="loader__dot loader__dot--2" id="dot2"></div>
      <div class="loader__text">Loading...</div>
    </div>
    <div class="grid grid--solidBlue grid--payment">
      <div class="heading">
        <img class="logo" src={{ url('../resources/assets/images/logo.svg') }} height="48" width="240" alt="Klon logo">
      </div>
      <div class="slideshow__navigation">
        <div>
          <button class="slideshow__btnNav" id="btnNavBack" style="transform: rotate(180deg)">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path fill="#fff" d="M8 24l-4-4 8-8-8-8 4-4 12 12z"/></svg>
          </button>
        </div>
        <div>
          <button class="slideshow__btnNav" id="btnNavForward">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path fill="#fff" d="M8 24l-4-4 8-8-8-8 4-4 12 12z"/></svg>
          </button>
        </div>
      </div>
      <div class="slideshow pay--1" id="slideshow">
        <div class="pay" id="pay1">
          <img src={{ url('../resources/assets/images/updateCreditCard.svg') }} alt="Update Credit Card graphic" height="200px" width="200px" style="display: block;margin: 0 auto;padding: 16px;">
          <div class="detail__title">
            <h2 id="detailTitle">Update Credit Card</h2>
          </div>
          <hr>
          <form action="https://api.klon.io/v1/subscriber/attemptUpdateCard" method="POST" name="updateCard" id="payment-form">
            <input type="hidden" name="token" value="<?php echo $token ?>">
            <div id="card-element"></div>
            <div id="card-errors" role="alert" class="stripe__error"></div>
            <button class="stripe__button">Update Card</button>
          </form>
        </div>
      </div>
    </div>
    <script src="https://js.stripe.com/v3/"></script>
    <script src={{ url('../resources/assets/js/scripts-dist.js') }} type="text/javascript"></script>
  </body>
</html>
