<html>
  <body>
    <!-- Logo -->
    <div>
      <img alt="Klon Logo" src="{{ $message->embed('../storage/app/images/klon-email-logo.png') }}" height="50" width="254">
    </div>
    <!-- -->
    <h2>Thank you for registering for Klon!</h2>
    <!-- -->
    <p>To activate your account please visit the following URL...<br><a href=https://api.klon.io/v1/activate/email/{{ $code }}>https://api.klon.io/v1/activate/email/{{ $code }}</a></p>
    <p>If you did not sign up for this service please disregard <br>this email. If you receive this email again in error please <br>contact us at admin@klon.io</p>
    <!-- -->
  </body>
</html>
