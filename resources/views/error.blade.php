<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1,shrink-to-fit=no" name=viewport>
    <title>Klon | Error!</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700&display=swap" rel="stylesheet">
    <link href="../../resources/assets/css/error.css" rel="stylesheet" type="text/css"/>
  </head>
  <body class="error">
    <div class="error__container">
      <h1>Error!</h1>
      <svg xmlns="http://www.w3.org/2000/svg" class="svgError" viewBox="0 0 1156 800">
        <filter id="a">
          <feGaussianBlur in="SourceGraphic" stdDeviation="6"/>
        </filter>
        <ellipse ry="215.74" rx="578.091" cy="583.431" cx="578.091" fill="#de2525"/>
        <path class="svgError__shadow" d="M355.34 356.222l-146.877 84.445L428.78 567.333 208.463 694l146.878 84.444 220.317-126.666 220.317 126.666L942.853 694 722.536 567.333l220.317-126.666-146.878-84.445L575.658 482.89z" fill="#c80000" filter="url(#a)"/>
        <g class="svgError__mark">
          <path d="M367.812 246l-138.564 80 207.846 120-207.846 120 138.564 80 207.846-120 207.846 120 138.564-80-207.846-120 207.846-120-138.564-80-207.846 120z" fill="#fcd8d8"/>
          <path d="M922.068 326v80l-138.564 80-69.282-40zm-346.41 200v80L367.812 726v-80z" fill="#fc5050"/>
          <path d="M229.248 326v80l138.564 80 69.282-40zm0 240v80l138.564 80v-80zm346.41-40l207.846 120v80L575.658 606z" fill="#fc7c7c"/>
          <path d="M922.068 566v80l-138.564 80v-80z" fill="#fc5050"/>
        </g>
      </svg>
      <div>
        <h2>We could not complete your request</h2>
        <p>Please try again or contact support by sending an email to help@klon.io</p>
      </div>
    </div>
  </body>
</html>
