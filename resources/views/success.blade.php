<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1,shrink-to-fit=no"name=viewport>
    <title>Klon | Success!</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700&display=swap" rel="stylesheet">
    <link href="https://api.klon.io/resources/assets/css/success.css" rel="stylesheet" type="text/css"/>
  </head>
  <body>
    <div class="success__container">
      <h1>Success!</h1>
      <svg id="success" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 359">
        <path fill="#255bde" d="M0 202c0 86.7 128.9 157 288 157s288-70.3 288-157S447.1 45 288 45 0 115.3 0 202"/>
        <path id="shadow" fill="#0c42c5" d="M463.5 95L231.4 227l-104.9-61-82 47L238 325l302.5-185z"/>
        <g id="checkmark">
          <path fill="#e4ffff" d="M454 0L231.9 121.1 127 60l-82 47 193.5 112L544 38z"/>
          <path fill="#659bff" d="M45.5 107v64l190.9 109.9L239 219z"/>
          <path fill="#8bc1ff" d="M544 38L239 218.7l-2.7 62L545.5 98z"/>
        </g>
      </svg>
      <h2>Your account is now activated!</h2>
      <p>You may now close this window or go to our home page by clicking <a href="https://klon.io" target="_self">here</a></p>
    </div>
  </body>
</html>
