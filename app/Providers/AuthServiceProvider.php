<?php

namespace App\Providers;

use App\User;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\RateLimitController as RateLimit;
use App\Http\Controllers\EncryptController as Encrypt;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
  /**
   * Register any application services.
   *
   * @return void
   */
  public function register()
  {
      //
  }

  /**
   * Boot the authentication services for the application.
   *
   * @return void
   */
  public function boot() {
    // Here you may define how you wish users to be authenticated for your Lumen
    // application. The callback which receives the incoming request instance
    // should return either a User instance or null. You're free to obtain
    // the User instance via an API token or any other method necessary.
    $this->app['auth']->viaRequest('api', function ($request) {
      if ($request->filled('payload')) {
        // Decrypt the payload
        $data = Encrypt::retrieveJSONData($request);
        if (isset($data['token'])) {
          $token = $data['token'];
          $isValid = AuthController::validateToken($token);
          $isValid = json_decode($isValid, true);
          if ($isValid['status'] == 'success') {
            if ($isValid['message'] == true) {
              return new User();
            } else {
              return null;
            }
          } else {
            return null;
          }
        } else {
          return null;
        }
      } else if ($request->filled('token')) {
        $token = $request->input('token');
        $isValid = AuthController::validateToken($token);
        $isValid = json_decode($isValid, true);
        if ($isValid['status'] == 'success') {
          if ($isValid['message'] == true) {
            return new User();
          } else {
            return null;
          }
        } else {
          return null;
        }
      } else {
        return null;
      }
    });
  }
}
