<?php

namespace App\Http\Controllers;

use App\Klon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\VerifySignupController as VerifySignup;

class SignupController extends Controller {

  /**
   *
   *  Create a new account with email and password provided
   *
   *  @param    $email       String      Email address for the new account
   *  @param    $password    String      Password for the new account
   *
   *  @return                JSONObject
   *
   */
  public static function createAccount(Request $request) {
    if ($request->filled("email") && $request->filled("password")) {
      $email = $request->input("email");
      $password = $request->input("password");
      // Setup the email address...
      // Turn the email string to lowercase
      $email = strtolower($email);
      // Validate email address
      $bool = self::validateEmail($email);
      // If the email is valid...
      if ($bool == true) {
        // Check to see if this email address already exists in the database
        $result = DB::select("SELECT `email` FROM `subscribers` WHERE `email` = :email", ['email' => $email]);
        // If the email address is already in the database
        if (count($result) > 0) {
          // Return an error to the client
          return response()->json(["status" => "fail", "message" => "email address already registered"], 400);
          // If the email address is NOT in the database
        } else {
          // Get the password length
          $pwLength = strlen($password);
          // Make sure password is 8 or more characters in length
          if ($pwLength >= 8) {
            // Hash the password
            $pwAlgo       = "sha512";
            $saltLength   = 128;
            $pwSalt       = bin2hex(openssl_random_pseudo_bytes($saltLength));
            $pwIterations = 1000;
            $pwHashLength = 512;
            $pwRawOutput  = false;
            $pwHash = hash_pbkdf2($pwAlgo, $password, $pwSalt , $pwIterations, $pwHashLength, $pwRawOutput);
            // If the hash was successful store the new user in the database
            if ($pwHash != false) {
              // Get the users ip
              $ip = $_SERVER['REMOTE_ADDR'];
              // Set the role
              // TODO: Make this automatically set when they pay online -- Or will this just be 'make a free trial account now'
              $role = 0;
              // Do not enable the account until the email is verified
              $enabled = 0;
              // The account is not expired.
              $expired = 0;
              // This is a new user account.
              $isNew = 1;
              // Set timestamps
              $created_at = time();
              $updated_at = time();
              // Generate a salt for the activation process
              $salt = bin2hex(random_bytes(32));
              // Generate the code that will activate the account
              $code = hash("sha256", $salt . $email . $salt);
              // Register the user into the database
              $addSubscriber = DB::insert("INSERT INTO `subscribers` (email, password, salt, role, enabled, expired, is_new, created_at, updated_at, ip) VALUES(:email, :password, :salt, :role, :enabled, :expired, :is_new, :created_at, :updated_at, :ip)", [':email' => $email, ':password' => $pwHash, ':salt' => $pwSalt, ':role' => $role, ':enabled' => $enabled, ':expired' => $expired, ':is_new' => $isNew, ':created_at' => $created_at, ':updated_at' => $updated_at, ':ip' => $ip]);
              // Add a record in the activation table
              $addActivation = DB::insert("INSERT INTO `activation` (email, code, salt) VALUES(:email, :code, :salt)", [':email' => $email, ':code' => $code, ':salt' => $salt]);
              // Send the user an email
              if ($addSubscriber && $addActivation) {
                // Send the email to the user
                $sendEmail = VerifySignup::sendVerificationEmail($code, $email);
                // Return response
                return response()->json(["status" => "success", "message" => "successfully registered user"], 200);
                //
              } else {
                // Something messed up with the activation and subscriber insertion
                return response()->json(["status" => "fail", "message" => "registration failed"], 400);
              }
            } else {
              // On password hashing error return error
              return response()->json(["status" => "fail", "message" => "password failed to hash"], 400);
            }
          } else {
            // Return password length error
            return response()->json(["status" => "fail", "message" => "supplied password was less than 8 characters"], 400);
          }
        }
      } else {
        return response()->json(["status" => "fail", "message" => "invalid email address"], 400);
      }
    } else {
      return response()->json(["status" => "fail", "message" => "incomplete request"], 400);
    }
  }


  /**
   *
   *  Determine if the supplied email address is valid based on the supplied PHP filter
   *
   *  @param   $email   String   An email address to be validated
   *
   *  @return           Boolean  true if valid, false otherwise
   *
   */
  private static function validateEmail($email = null) {
    // Email address not set
    if ($email == null) {
      return false;
    }
    // Validate the email address
    $bool = filter_var($email, FILTER_VALIDATE_EMAIL);
    // Return true if valid or false otherwise
    if ($bool != false) {
      // Make sure the email address doesnt have @klonmail.com
      if (strpos($email, "@klonmail.com") === false) {
        return true;
      } else {
        return false;
      }
    }
    return false;
  }


  /**
   *
   *  This code will validate an email address
   *  The user will visit a link something like https://klon.io/api/v1/validate/e35fadc87980b0a8f00345faff034
   *  If the code is correct then we will present them with a Success message, otherwise fail... Maybe redirect them somewhere or something
   *
   */
  public static function activateAccount($code = null) {
    // Make sure the $code was sent in the request
    if ($code != null) {
      // Sanitize $code
      $code = filter_var($code, FILTER_SANITIZE_STRING);
      // Check to see if the code is in the DB
      $getData = DB::select("SELECT `email`,`code`,`salt` FROM `activation` WHERE `code`=:code", ['code' => $code]);
      // Make sure there were results returned from the query
      if (count($getData) > 0) {
        //
        $queryCode;
        $querySalt;
        $queryEmail;
        // Pull the information out of the sql response
        forEach($getData as $data) {
          $queryCode  = $data->code;
          $querySalt  = $data->salt;
          $queryEmail = $data->email;
        }
        // Check to see if we can re-create this code with the information provided
        $hash = hash("sha256", $querySalt . $queryEmail . $querySalt);
        if ($hash === $queryCode) {
          // Find the $email in the subscribers database and enable the account
          $enableAccount = DB::update("UPDATE `subscribers` SET `enabled`=1 WHERE `email`=:email AND `enabled`=0", ['email' => $queryEmail]);
          // Delete the record from the activation table no longer required
          $enableAccount = DB::delete("DELETE FROM `activation` WHERE `email`=:email", ['email' => $queryEmail]);
          // return "Success! Your account is now activated";
          return view('success');
        } else {
          return "Fail: Incorrect activation code";
        }
      } else {
        // Code not found in the database
        echo "Fail: Invalid activation code";
        return "";
      }
    } else {
      // $code was not sent with the request
      echo "Fail: Activation code not set";
      return "";
    }
  }
  // public static function test() {
  //   $salt = bin2hex(random_bytes(32));
  //   echo "Salt: ". $salt;
  //   echo "<br>";
  //   $email = "bk@protonmail.com";
  //   echo "Email: " . $email;
  //   echo "<br>";
  //   echo "Code: " . hash("sha256", $salt . $email . $salt);
  //   return "true";
  // }

}
