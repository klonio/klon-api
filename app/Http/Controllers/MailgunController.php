<?php

namespace App\Http\Controllers;

use App\Klon;
use App\Http\Controllers\AuthController as Auth;
use App\Http\Controllers\DataManipulationController as DataManipulation;
use App\Http\Controllers\EncryptController as Encrypt;
use App\Http\Controllers\MessageController as Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MailgunController extends Controller {


  /**
   *
   *  Get the variables stored in the .env file regarding MailGun API
   *
   *  @return   Array   Returns the MailGun configuration in an array
  */
  private static function returnConfig() {
    $baseUrl = $_ENV['MAILGUNBASEURL'];
    $apiKey = $_ENV['MAILGUNKEY'];
    $config = array(
      'baseUrl' => $baseUrl,
      'apiKey'  => $apiKey
    );
    return $config;
  }


  /**
   *
   *  Adds a forwarding route in MailGun
   *
   *  @param   $fromMail   String   Email address to accept emails on behalf of subscriber
   *  @param   $toMail     String   Email address to forward emails to
   *
   *  @return              JSONObject
   *
   */
  public static function addForwardingAddress($fromEmail, $toEmail) {
    if ($fromEmail && $toEmail) {
      // Setup Mailgun config
      $mgConfig = self::returnConfig();
      // Initialize cURL
      $ch = curl_init('https://api.mailgun.net/v3/routes');
      // Set the cURL options
      $credentials = "api:" . $mgConfig['apiKey'];
      // Setup options array. POSTFIELDS had to be set as var=val&var=val because of double 'action' vars
      $chOptions = array(
        CURLOPT_HTTPAUTH => CURLAUTH_ANY,
        CURLOPT_USERPWD => $credentials,
        CURLOPT_POST => true,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POSTFIELDS => 'priority=2&expression=match_recipient("'. $fromEmail .'")&action=forward("'. $toEmail .'")&action=stop()'
      );
      curl_setopt_array($ch, $chOptions);
      // Execute the statement
      $chResponse = curl_exec($ch);
      // Decode the JSON Object returned from the server
      $chResponse = json_decode($chResponse, true);
      //
      $routeId = 0;
      //
      if (isset($chResponse['route']['id'])) {
        // Set route Id
        $routeId = $chResponse['route']['id'];
        // Close the resource
        curl_close($ch);
        return $routeId;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }


  /**
   *
   */
  public static function getRoutes() {
    // Setup Mailgun config
    $mgConfig = self::returnConfig();
    // Initialize cURL
    $ch = curl_init('https://api.mailgun.net/v3/routes');
    // Set the cURL options
    $credentials = "api:" . $mgConfig['apiKey'];
    // Setup options array. POSTFIELDS had to be set as var=val&var=val because of double 'action' vars
    $chOptions = array(
      CURLOPT_HTTPAUTH => CURLAUTH_ANY,
      CURLOPT_USERPWD => $credentials,
      CURLOPT_RETURNTRANSFER => true);
    curl_setopt_array($ch, $chOptions);
    // Execute the statement
    $chExec = curl_exec($ch);
    // Close the resource
    curl_close($ch);
    return json_decode($chExec, true);
  }


  /**
   *
   *  Send an email
   *
   *  @param      $to             String
   *  @param      $from           String
   *  @param      $subject        String
   *  @param      $message        String
   *
   *  @return                     JSONObject
   *
   *  curl -s --user 'api:YOUR_API_KEY' \
   *  https://api.mailgun.net/v3/YOUR_DOMAIN_NAME/messages \
   *  -F from='Excited User <mailgun@YOUR_DOMAIN_NAME>' \
   *  -F to=YOU@YOUR_DOMAIN_NAME \
   *  -F to=bar@example.com \
   *  -F subject='Hello' \
   *  -F text='Testing some Mailgun awesomeness!'
   *
   */
  public static function sendMail(Request $request) {
    // Decrypt the request
    $mail    = Encrypt::retrieveJSONData($request);
    // Check to make sure all the required fields are filled
    if (isset($mail["to"]) && isset($mail["from"]) && isset($mail["subject"]) && isset($mail["message"]) && isset($mail["token"])) {
      // Check if token is valid
      $token        = $mail["token"];
      $isTokenValid = Auth::validateToken($token);
      $isTokenValid = json_decode($isTokenValid, true);
      if ($isTokenValid["status"] == "success" && $isTokenValid["message"] == true) {
        //
        $to          = $mail["to"];
        $from        = $mail["from"];
        $subject     = $mail["subject"];
        $message     = $mail["message"];
        // Check to see if the user owns the email they want to send from
        $isFromOwner = Message::isEmailOwner($from, $token);
        if ($isFromOwner == true) {
          // Setup Mailgun config
          $mgConfig = self::returnConfig();
          // Initialize cURL
          $ch = curl_init('https://api.mailgun.net/v3/klonmail.com/messages');
          // Set the cURL options
          $credentials = "api:" . $mgConfig['apiKey'];
          // Setup options array. POSTFIELDS had to be set as var=val&var=val because of double 'action' vars
          $chOptions = array(
            CURLOPT_HTTPAUTH => CURLAUTH_ANY,
            CURLOPT_USERPWD => $credentials,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => 'from=' . $from . '&to=' . $to . '&subject=' . $subject . '&text=' . $message);
            curl_setopt_array($ch, $chOptions);
            // Execute the statement
            $chExec = curl_exec($ch);
            // Close the resource
            curl_close($ch);
            return json_decode($chExec, true);
        } else {
          return response()->json(["status" => "fail", "message" => "invalid \"from\" email"], 401);
        }
      } else {
        return response()->json(["status" => "fail", "message" => "invalid token"], 401);
      }
    } else {
      // Return error missing arguments
      return response()->json(["status" => "fail", "message" => "missing arguments"], 400);
    }
  }


  /**
   *
   *  Insert a record into the `sent` table
   *
   */
  private static function insertSentRecord($ownerId, $mailFrom, $mailTo, $subject, $message, $ip) {
    if (isset($ownerId) && isset($mailFrom) && isset($mailTo) && isset($subject) && isset($message) && isset($ip)) {
      // Get the from owner's email address
      $mailfromOwner = DB::select("SELECT `owner_id` FROM `email_addresses` WHERE `email`=:email",["email" => $mailFrom]);
      // If a record was found for this email address carry on...
      if (count($mailfromOwner) > 0) {
        $mailfromOwner = array_column($mailfromOwner, "owner_id");
        $mailfromOwner = $mailfromOwner[0];
      } else {
        return false;
      }
      // Check to see if the owner owns this email address
      if ($ownerId == $mailfromOwner) {
        // Encrypt the values before insertion
        $mailFrom = Encrypt::encryptMessageContents($mailFrom);
        $mailTo   = Encrypt::encryptMessageContents($mailTo);
        $subject  = Encrypt::encryptMessageContents($subject);
        $message  = Encrypt::encryptMessageContents($message);
        $times    = strtotime('now');
        $ip       = $_SERVER['REMOTE_ADDR'];
        $ip       = DataManipulation::convertIP($ip);
        // Add the record to the sent table
        $inserted = DB::insert("INSERT INTO `sent` (`owner_id`, `mail_from`, `mail_to`, `subject`, `message`, `timestamp`, `ip`) VALUES(:ownerId, :mailFrom, :mailTo, :subject, :message, :times, :ip)", ["ownerId" => $ownerId, "mailFrom" => $mailFrom, "mailTo" => $mailTo, "subject" => $subject, "message" => $message, "times" => $times, "ip" => $ip]);
        if ($inserted) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      // Missing arguments
      return false;
    }
  }


  /**
   *
   *  Send an email
   *
   *  @param      $to             String
   *  @param      $from           String
   *  @param      $subject        String
   *  @param      $message        String
   *
   *  @return                     JSONObject
   *
   *  curl -s --user 'api:YOUR_API_KEY' \
   *  https://api.mailgun.net/v3/YOUR_DOMAIN_NAME/messages \
   *  -F from='Excited User <mailgun@YOUR_DOMAIN_NAME>' \
   *  -F to=YOU@YOUR_DOMAIN_NAME \
   *  -F to=bar@example.com \
   *  -F subject='Hello' \
   *  -F text='Testing some Mailgun awesomeness!'
   *
   */
  public static function forwardMail(Request $request) {
    // Decrypt the request
    $mail    = Encrypt::retrieveJSONData($request);
    // Check to make sure all the required fields are filled
    if (isset($mail["mailTo"]) && isset($mail["mailSubject"]) && isset($mail["mailMessage"]) && isset($mail["token"])) {
      // Check if token is valid
      $token        = $mail["token"];
      $token        = filter_var($token, FILTER_SANITIZE_STRING);
      $isTokenValid = Auth::validateToken($token);
      $isTokenValid = json_decode($isTokenValid, true);
      if ($isTokenValid["status"] == "success" && $isTokenValid["message"] == true) {
        //
        $from        = $mail["mailTo"];
        $from        = filter_var($from, FILTER_SANITIZE_EMAIL);
        //
        $subject     = $mail["mailSubject"];
        $message     = $mail["mailMessage"];
        // Check to see if the user owns the email they want to send from
        $isFromOwner = Message::isEmailOwner($from, $token);
        if ($isFromOwner == true) {
          // Get owner id
          $owner      = Auth::tokenOwner($token);
          $ownerId    = $owner['id'];
          $ownerEmail = $owner['email'];
          // the "to" is the owner of the email
          $to = $ownerEmail;
          // Setup Mailgun config
          $mgConfig = self::returnConfig();
          // Initialize cURL
          $ch = curl_init('https://api.mailgun.net/v3/klonmail.com/messages');
          // Set the cURL options
          $credentials = "api:" . $mgConfig['apiKey'];
          // Setup options array. POSTFIELDS had to be set as var=val&var=val because of double 'action' vars
          $chOptions = array(
            CURLOPT_HTTPAUTH => CURLAUTH_ANY,
            CURLOPT_USERPWD => $credentials,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => 'from=' . $from . '&to=' . $to . '&subject=' . $subject . '&text=' . $message);
            curl_setopt_array($ch, $chOptions);
            // Execute the statement
            $chExec = curl_exec($ch);
            // Close the resource
            curl_close($ch);
            // Get details of curl response... If successful add the record to the sent box.
            $response = json_decode($chExec, true);
            // Get the IP and convert it to storable object
            $ip = DataManipulation::convertIP($_SERVER['REMOTE_ADDR']);
            if ($response["message"] == "Queued. Thank you.") {
              // INSERT INTO THE SENT DATABASE
              $insertion = self::insertSentRecord($ownerId, $from, $to, $subject, $message, $ip);
              if ($insertion) {
                //
                return response()->json(["status" => "success", "message" => "mail sent"], 200);
              } else {
                return response()->json(["status" => "fail", "message" => "mail not stored"], 200);
              }
            }
        } else {
          return response()->json(["status" => "fail", "message" => "invalid 'from' email"], 401);
        }
      } else {
        return response()->json(["status" => "fail", "message" => "invalid token"], 401);
      }
    } else {
      // Return error missing arguments
      return response()->json(["status" => "fail", "message" => "missing arguments"], 400);
    }
  }


  /**
   *
   *  Reply to an email
   *
   *  @param      $to             String
   *  @param      $from           String
   *  @param      $subject        String
   *  @param      $message        String
   *
   *  @return                     JSONObject
   *
   *  curl -s --user 'api:YOUR_API_KEY' \
   *  https://api.mailgun.net/v3/YOUR_DOMAIN_NAME/messages \
   *  -F from='Excited User <mailgun@YOUR_DOMAIN_NAME>' \
   *  -F to=YOU@YOUR_DOMAIN_NAME \
   *  -F to=bar@example.com \
   *  -F subject='Hello' \
   *  -F text='Testing some Mailgun awesomeness!'
   *
   */
  public static function replyMail(Request $request) {
    // Decrypt the request
    $mail    = Encrypt::retrieveJSONData($request);
    // Check to make sure all the required fields are filled
    if (isset($mail["mailFrom"]) && isset($mail["mailTo"]) && isset($mail["mailSubject"]) && isset($mail["mailMessage"]) && isset($mail["token"])) {
      // Check if token is valid
      $token        = $mail["token"];
      $isTokenValid = Auth::validateToken($token);
      $isTokenValid = json_decode($isTokenValid, true);
      if ($isTokenValid["status"] == "success" && $isTokenValid["message"] == true) {
        // From and To will be backward because we are sending the email from the "mailTo" address
        $from        = $mail["mailTo"];
        $to          = $mail["mailFrom"];
        $subject     = $mail["mailSubject"];
        $message     = $mail["mailMessage"];
        // Check to see if the user owns the email they want to send from
        $isFromOwner = Message::isEmailOwner($from, $token);
        if ($isFromOwner == true) {
          // Get owner id
          $owner      = Auth::tokenOwner($token);
          $ownerId    = $owner['id'];
          $ownerEmail = $owner['email'];
          // Setup Mailgun config
          $mgConfig = self::returnConfig();
          // Initialize cURL
          $ch = curl_init('https://api.mailgun.net/v3/klonmail.com/messages');
          // Set the cURL options
          $credentials = "api:" . $mgConfig['apiKey'];
          // Setup options array. POSTFIELDS had to be set as var=val&var=val because of double 'action' vars
          $chOptions = array(
            CURLOPT_HTTPAUTH => CURLAUTH_ANY,
            CURLOPT_USERPWD => $credentials,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => 'from=' . $from . '&to=' . $to . '&subject=' . $subject . '&text=' . $message);
            curl_setopt_array($ch, $chOptions);
            // Execute the statement
            $chExec = curl_exec($ch);
            // Close the resource
            curl_close($ch);
            // Get details of curl response... If successful add the record to the sent box.
            $response = json_decode($chExec, true);
            // Get the IP and convert it to storable object
            $ip = DataManipulation::convertIP($_SERVER['REMOTE_ADDR']);
            if ($response["message"] == "Queued. Thank you.") {
              // INSERT INTO THE SENT DATABASE
              $insertion = self::insertSentRecord($ownerId, $from, $to, $subject, $message, $ip);
              if ($insertion) {
                //
                return response()->json(["status" => "success", "message" => "mail sent"], 200);
              } else {
                return response()->json(["status" => "fail", "message" => "mail not stored"], 400);
              }
            }
        } else {
          return response()->json(["status" => "fail", "message" => "invalid 'from' email"], 401);
        }
      } else {
        return response()->json(["status" => "fail", "message" => "invalid token"], 401);
      }
    } else {
      // Return error missing arguments
      return response()->json(["status" => "fail", "message" => "missing arguments"], 400);
    }
  }

}
