<?php

namespace App\Http\Controllers;

use App\Klon;
use App\Http\Controllers\EncryptController as Encrypt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller {
  // Let the controller know which functions require authentication
  public function __construct() {
    self::middleware('auth',['only' => [
      'getSettings',
      'isSubscriber',
      'getRole'
      ]]);
  }

/**
 *
 *  Authentication based on JSON Web Token (JWT)
 *  Output example: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c
 *
 *  Anatomy of JWT.
 *  aaaaa.bbbb.cccc - Sections of a JSON Web Token
 *  a - Header    --  This section contains the type which is JWT and the algorithm (alg) which is HMAC SHA256 aka HS256
 *  b - Payload   --  Contains all the information we're trying to send
 *  c - Signature --  Base64 encoded and then signed with the before mentioned algorithm
 *
 */

//  Maximum number of login attempts before being banned
private static $maxAttempts = 5;

//  Ban expiration time 1800 == 30 minutes
private static $banTime = 1800;

/**
 *
 *  Generate the header portion of the JWT.
 *
 *  This portion contains the type(typ) and algorithm(alg) associated with the JWT's
 *
 *  @return  JSONObject
 *
 */
  private static function genHeader() {
    // Algorithm used for signing, HS256
    $algorithm = 'HS256';
    $header = ['alg' => $algorithm, 'typ' => 'JWT'];
    $headerEncoded = self::base64url_encode(json_encode($header));
    return $headerEncoded;
  }

  public static function success() {
    return view('success');
  }


  /**
   *
   *  Generates the payload portion of the JSON Web Token
   *
   *  Notes: All timestamps are generated in UTC timezone
   *
   *  Optional reserved values
   *  ************************
   *  iss -- Issuer of the token. In this case it is klon.io
   *  sub -- The subject of the token
   *  aud -- Audience of the token
   *  exp -- Expiration of the token in unix epoch format
   *  nbf -- Defines the time before the token must NOT be accepted (NotBefore)
   *  iat -- Issued at time.
   *  jti -- JSON Token ID. Mainly used for one time use tokens
   *
   *  @return JSONObject
   *
   */
  private static function genPayload($subject, $expiration = null) {
    $iss = 'klon.io';
    if ($expiration == null) {
      $exp = strtotime("+4 hours");
    } else {
      $exp = $expiration;
    }
    $iat = time();
    $nbf = $iat - 1;
    $payload = ['iss' => $iss, 'exp' => $exp, 'nbf' => $nbf, 'iat' => $iat, 'sub' => $subject];
    $payloadEncoded = self::base64url_encode(json_encode($payload));
    return $payloadEncoded;
  }


  /**
   *
   *  This is a hash of the header, payload, and secret
   *
   */
  private static function genSignature($subject, $expiration) {
    $key = $_ENV['PRIVATEKEY'];
    $key = hex2bin(Encrypt::clarifyKey($key));
    $signature = hash_hmac('SHA256', self::genHeader() . "." . self::genPayload($subject, $expiration), $key, true);
    $signatureEncoded = self::base64url_encode($signature);
    return $signatureEncoded;
  }


  /**
   *
   *  Assembles the token
   *
   */
  private static function genToken($subject, $expiration) {
    $token = self::genHeader() . "." . self::genPayload($subject, $expiration) . "." . self::genSignature($subject, $expiration);
    return $token;
  }


  /**
   *
   *  Base64Url Encode data
   *
   *  @param    $data    String    Data to be base64url encoded
   *
   *  @return            String    Base64url encoded data
   *
   */
  private static function base64url_encode($data) {
    return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
  }


  public static function base64url_decode($data) {
    return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
  }


  /**
   *
   *  Determine the email address of the owner associated with the active token
   *
   *  @param     $token     JSONWebToken       Active JWT
   *
   *  @return               Array             Email address of the owner of the provided token
   *
   */
  public static function tokenOwner($token) {
    $token = filter_var($token, FILTER_SANITIZE_STRING);
    // Get the email address of the token owner
    $results = DB::select("SELECT `email` FROM `active_tokens` WHERE `token`=:token",["token" => $token]);
    // If record found, retrieve the id of the subscriber with the corresponding email address
    if (count($results) > 0) {
      $result = json_decode(json_encode($results), true);
      $result = $result[0];
      $email = $result['email'];
      if (strpos($email, "@") != false) {
        // Retrieve the subscriber_id from the subscriber table
        $subscriber_id = DB::select("SELECT `id` FROM `subscribers` WHERE `email`=:email",['email' => $email]);
        // If results were found return the array of id & email otherwise return false
        if (count($subscriber_id) > 0) {
          // Turn the stdClass object into an associative array
          $id = json_decode(json_encode($subscriber_id), true);
          $id = $id[0];
          $id = $id['id'];
          // Form the array
          $ownerArray = array(
            "email" => $email,
            "id"    => $id
          );
          return $ownerArray;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }


  /**
   *
   *  Return the user's settings based on the token received
   *
   *  @param     $token           Request
   *
   *  @return                    JSONObject        JSONObject full of the users settings
   *
   */
  public static function getSettings(Request $request) {
    // Get the token from the request
    if ($request->filled('payload')) {
      // Decrypt the payload
      $data = Encrypt::retrieveJSONData($request);
      // Assign the token variable
      $token = $data["token"];
      $token = filter_var($token, FILTER_SANITIZE_STRING);
      // Verify token is legit
      $owner = self::tokenOwner($token);
      $owner = $owner['email'];
      if ($owner != false) {
        $isTokenValid = self::validateToken($token);
        $isTokenValid = json_decode($isTokenValid, true);
        $tokenStatus = $isTokenValid['status'];
        //
        if ($tokenStatus == 'success') {
          // Get the settings from the database
          $query = DB::select("SELECT `hash`,`role`,`enabled`, `autogenerate`, `autofill` FROM `subscribers` WHERE `email` = :email", ['email' => $owner]);
          return response()->json(["status" => "success", "data" => $query], 200);
          // return
        } else {
          return response()->json(["status" => "fail", "message" => "invalid token"], 400);
        }
      }
    }
  }



  /**
   *
   *  Validate the JSON Web Token
   *
   *  @param      $token      JSON Token     This is the token to be validated
   *
   *  @return                 Boolean        True if valid, False if otherwise
   *
   */
  public static function validateToken($token) {
    // Check if the JWT contains at least 2 periods
    $firstPeriod = strpos($token, ".");
    $lastPeriod  = strrpos($token, ".");
    if ($firstPeriod != false && $firstPeriod != $lastPeriod && $lastPeriod != false) {
      // Set the header portion of the token
      $header = substr($token, 0, $firstPeriod);
      // Base64Url decode the string
      $header = self::base64url_decode($header);
      // Decode the JSON
      $header = json_decode($header, true);
      // Get the algorithm used to sign the JWT
      $algorithm = $header['alg'];
      // Get the type of token
      $type = $header['typ'];
      // If the type and algorithm are set then proceed
      if (isset($type) && isset($algorithm)) {
        // Payload is the area between the first period and the second -or last- period.
        // You need ($lastPeriod - $firstPeriod) because starting at $firstPeriod changes the offset of $lastPeriod
        // ie: Just because the $lastPeriod is 128 characters into the string you do not necessarily want to grab
        //     128 characters from the $firstPeriod... That is too many characters so you have to subtract the offset
        $payload = substr($token, ++$firstPeriod, ($lastPeriod - $firstPeriod));
        // Base64Url decode the $payload
        $payload = self::base64url_decode($payload);
        // JSON decode the payload into an associative array
        $payload = json_decode($payload, true);
        // Get the expiration
        $expiration = $payload['exp'];
        // Get the issued at time
        $issuedAt = $payload['iat'];
        // Get the issuer
        $issuer = $payload['iss'];
        // Get the not before time
        $notBefore = $payload['nbf'];
        // Get the subject
        $subject = $payload['sub'];
        // Get the current time in unix epoch format
        $currentTime = time();
        // Check to see if the token has expired
        if ($currentTime < $expiration) {
          // Check if after the nbf time
          if ($currentTime > $notBefore) {
            // Check if the issuer was klon.io
            if ($issuer == 'klon.io') {
              // Get the signature
              $signature = substr($token, ++$lastPeriod);
              // Base64Url decode the signature
              $signatureHash = bin2hex(self::base64url_decode($signature));
              // Get the secret key
              $key = $_ENV['PRIVATEKEY'];
              $key = hex2bin(Encrypt::clarifyKey($key));
              // Assemble the new payload
              $newPayload = ['iss' => $issuer, 'exp' => $expiration, 'nbf' => $notBefore, 'iat' => $issuedAt, 'sub' => $subject];
              // base64url encode the new payload
              $newPayloadHash = self::base64url_encode(json_encode($newPayload));
              // Assemble the new signature and hash it
              $newSignatureHash = bin2hex(hash_hmac('SHA256', self::genHeader() . "." . $newPayloadHash, $key, true));
              // Determine if the hashes are the same
              $isSignatureValid = hash_equals($signatureHash, $newSignatureHash);
              // Check if token stored in database table active_tokens
              $isStored = DB::select('SELECT `email`,`token` FROM `active_tokens` WHERE `token` = :token',['token' => $token]);
              // TODO: Add email address validation
              if (count($isStored) > 0) {
                // Return the results in JSON format
                return json_encode(['status' => 'success', 'message' => $isSignatureValid]);
              } else {
                json_encode(['status' => 'fail', 'message' => 'token not recognized']);
              }
            } else {
              return json_encode(['status' => 'fail', 'message' => 'unauthentic token issuer']);
            }
          } else {
            // Token is being used before the NBF time.
            return json_encode(['status' => 'fail', 'message' => 'not before time has not passed']);
          }
        } else {
          // Token is expired let the user know
          return json_encode(['status'=>'fail', 'message'=>'expired token']);
          // return response()->json(['status' => 'fail', 'message' => 'expired token'], 401);
        }
      } else {
        return json_encode(['status' => 'fail', 'message' => 'malformed token header']);
      }
    } else {
      return json_encode(['status' => 'fail', 'message' => 'malformed token']);
    }
  }


  /**
   *
   *  Delete expired tokens
   *
   *  @param    $email        String       Email address used to sign up
   *
   *  @return                 Boolean      True upon success, false otherwise
   *
   */
  private static function deleteExpiredTokens($email) {
    // Set the default timezone just in case
    date_default_timezone_set('UTC');
    // Get the current time +5 minutes
    $expiration = strtotime('+5 minutes');
    // Query the database for all active tokens with the same email and lesser expiration time for deletion
    $results = DB::select('SELECT * FROM `active_tokens` WHERE `email` = :email AND `expiration` < :expiration', [':email' => $email, ':expiration' => $expiration]);
    // If there are results pulled from the database
    if (count($results) > 0) {
      // Delete the records from the table
      $deleted = DB::delete('DELETE FROM `active_tokens` WHERE `email` = :email AND `expiration` < :expiration', [':email' => $email, ':expiration' => $expiration]);
      // JSON encode the response
      return json_encode(['status' => 'success', 'rowsDeleted' => $deleted]);
    } else {
      // JSON encode the response let the user know no records were found
      return json_encode(['status' => 'success', 'no records found']);
    }
  }


  /**
   *
   *  Check if active tokens
   *
   *  @param    $email        String       Email address used to sign up
   *
   *  @return                 JSONObject
   *
   */
  private static function hasActiveToken($email) {
    $results = DB::select('SELECT * FROM `active_tokens` WHERE `email` = :email',[':email' => $email]);
    if (count($results) > 0) {
      // Set the default timezone just in case
      date_default_timezone_set('UTC');
      // Get the expiration time from the database
      $expiration = array_column($results, 'expiration');
      $expiration = $expiration[0];
      // Set a time to check against
      $timeToCheckAgainst = strtotime('+5 minutes');
      // Pull the token from the query
      $token = array_column($results, 'token');
      $token = $token[0];
      // Check to see if the expiration is more than
      if ($expiration > $timeToCheckAgainst) {
        // Return the token
        return json_encode(['status' => 'success', 'token' => $token]);
      } else {
        // Delete expired tokens
        $deleted = self::deleteExpiredTokens($email);
        $deleted = json_decode($deleted, true);
        // If there were expired tokens deleted I want to know about it
        if ($deleted['rowsDeleted'] > 0) {
          return json_encode(['status' => 'fail', 'message' => 'no active tokens found', 'deleted' => $deleted['rowsDeleted']]);
        }
        // Return response
        return json_encode(['status' => 'fail', 'message' => 'no active tokens found']);
      }
      // Get the token from the database
      $token = array_column($results, 'token');
      $token = $token[0];
    } else {
      json_encode(['status' => 'fail', 'message' => 'no active tokens found']);
    }
  }


  // TODO: is Banned? Function. Check to see if the user is banned and when the ban expires


  /**
   *
   *
   *
   *
   *
   */
  private static function verifyPassword($incoming, $stored) {
    if ($incoming === $stored) {
      return true;
    } else {
      return false;
    }
  }


  /**
   *
   *
   *
   *
   *
   */
  private static function pbkdf2Password($email, $password) {
    // Previously established that the email address exists in database
    $results = DB::select('SELECT `salt` FROM `subscribers` WHERE `email` = :email',[':email' => $email]);
    // If there are results from the query...
    if (count($results) > 0) {
      $salt = array_column($results, 'salt');
      $salt = $salt[0];
    } else {
      return false;
    }
    // Run 1000 rounds of this hashing algorithm
    $iterations = 1000;
    $hash = hash_pbkdf2('sha512', $password, $salt, $iterations);
    return $hash;
  }


  /**
   *
   *  Decrypt the payload portion of the JWT
   *
   *  @param    $payload    String              Payload portion of the JWT
   *
   *  @return               String|Boolean      Returns the decrypted data or false
   *
   */
  public static function decryptPayload($payload) {
    // Define keys
    $PUBLIC_KEY  = hex2bin($_ENV['PUBLICKEY']);
    $obfsPrvKey  = $_ENV['PRIVATEKEY'];
    $PRIVATE_KEY = hex2bin(Encrypt::clarifyKey($obfsPrvKey));
    // Don't try, do.
    try {
      // Generate keypair from keys
      $newKeyPair = sodium_crypto_box_keypair_from_secretkey_and_publickey($PRIVATE_KEY, $PUBLIC_KEY);
    } catch (SodiumException $e) {
      $error = $e->getMessage();
      return false;
    }
    // Payload to binary
    $payload = hex2bin($payload);
    // Don't try, do.
    try {
      // Decrypt payload
      $decrypted = sodium_crypto_box_seal_open($payload, $newKeyPair);
    } catch (SodiumException $e) {
      $error = $e->getMessage();
      return false;
    }
    // Return the decrypted payload
    return $decrypted;
  }


  /**
   *
   *  Further hash the incoming password hash to check if password is correct
   *
   *  @param    $email       String      Subscriber's email address
   *  @param    $password    String      Subscriber's password
   *
   *  @return                String      Password hash
   *
   */
  public static function hashPassword($email, $password) {
    // Hash the password
    $pwAlgo       = "sha512";
    $salt         = DB::select("SELECT `salt` FROM `subscribers` WHERE `email` = :email", ['email' => $email]);
    $salt         = array_column($salt, 'salt');
    $salt         = $salt[0];
    $pwIterations = 1000;
    $pwHashLength = 512;
    $pwRawOutput  = false;
    $pwHash       = hash_pbkdf2($pwAlgo, $password, $salt , $pwIterations, $pwHashLength, $pwRawOutput);
    return $pwHash;
  }


  /**
   *
   *  Determine if the given email address belongs to a subscriber of Klon or not
   *
   *  @param       String      $email       The email address to check against the database
   *
   *  @return      Boolean                  True if subscriber, false otherwise
   *
   *  @throws      Exception
   *
   */
  public static function isSubscriber($email) {
    // Make sure the email parameter was set and that it is a string
    if (isset($email) && gettype($email) === "string") {
      // Make sure its an actual email address
      $email = filter_var($email, FILTER_VALIDATE_EMAIL);
      if ($email == false) {
        // return response()->json(["status" => "success", "message" => "invalid email address 1"], 400);
        return null;
      }
      // Sanitize the email address
      $email = filter_var($email, FILTER_SANITIZE_EMAIL);
      // Make sure it is an email address
      if ($email !== false) {
        // Run the query
        $query = DB::select("SELECT `email` FROM `subscribers` WHERE `email`=:email", ['email' => $email]);
        // Check to see if we got results back
        if (gettype($query) === "array" && !empty($query)) {
          // return response()->json(["status" => "success", "message" => "true"], 200);
          return true;
          // If no results returned (empty array)
        } else {
          // return response()->json(["status" => "success", "message" => "false"], 200);
          return false;
        }
      } else {
        // throw new Exception("Invalid email address");
        // return response()->json(["status" => "success", "message" => "invalid email address 2"], 400);
        return null;
      }
    } else {
      // throw new Exception("Invalid data type for parameter 'email'");
      // return response()->json(["status" => "success", "message" => "Invalid data type for parameter 'email'"], 400);
      return null;
    }
  }


  /**
   *
   *  Log in function. Authenticates the user and returns a token.
   *
   *  @param    $email        String       Email address used to sign up
   *  @param    $password     String       Password used to sign up. Must be 8 characters or more in length.
   *
   *  @return                 JSONObject   Returns a JWT signed with HS256
   *
   */
  public static function login(Request $request) {
    $data = $request->input('payload');
    $decrypted = self::decryptPayload($data);
    $decrypted = json_decode($decrypted, true);
    // Check if the encrypted payload was set in the POST data
    if ($request->filled('payload')) {
      // Set the encrypted payload from the POST data
      $encryptedData = $request->input('payload');
      // Decrypt the encrypted payload
      $data = self::decryptPayload($encryptedData);
      // echo "Decrypted: " . $data;
      // Decode JSON data
      $data = json_decode($data, true);
      if ($data != null) {
        // Check to see if email was sent in JSON data
        if (isset($data['email'])) {
          // Check to see if email is a string
          if (gettype($data['email']) == "string") {
            // Set email from the decrypted data
            $email = $data['email'];
            $email = filter_var($email, FILTER_SANITIZE_EMAIL);
            // Check to see if the password is set
            if (isset($data['password'])) {
              // Get the password from the decrypted POST data
              $password = $data['password'];
              // Query the database for all registered email addresses matching the one supplied
              $subscribers = DB::select('SELECT email, password, salt, role, enabled, expired, is_new FROM subscribers WHERE email = :email', [':email' => $email]);
              // Make sure this is a countable object like an array
              if (gettype($subscribers) === "array") {
                // If there is a matching result...
                if (count($subscribers) > 0) {
                  // Pull the email address from the db query
                  $email = array_column($subscribers, 'email');
                  $email = $email[0];
                  // Pull the enabled status from the db query
                  $isEnabled = array_column($subscribers, 'enabled');
                  $isEnabled = $isEnabled[0];
                  // Check to see if the account is enabled
                  if ($isEnabled == 1) {
                    // Check to see if the account has expired
                    $isExpired = array_column($subscribers, 'expired');
                    $isExpired = $isExpired[0];
                    if ($isExpired === 0) {
                      // // Find out if the user is new
                      $isNew = array_column($subscribers, 'is_new');
                      $isNew = $isNew[0];
                      // // If the user is new, change the setting
                      // if ($isNew === 1) {
                      //   try {
                      //     DB::update("UPDATE `subscribers` SET `is_new`=0 WHERE email=:email", ['email' => $email]);
                      //   } catch (Exception $e) {
                      //     return response()->json(['status' => 'fail', 'message' => 'could not update user'], 500);
                      //   }
                      // }
                      // Pull the password hash from the db query
                      $hash = array_column($subscribers, 'password');
                      $hash = $hash[0];
                      // Pull the salt from the db query
                      $salt = array_column($subscribers, 'salt');
                      $salt = $salt[0];
                      // Verify password hash is correct
                      $incomingHash = self::hashPassword($email, $password);
                      // Check to see if the hashes are the same
                      $passwordCorrect = self::verifyPassword($incomingHash, $hash);
                      // If the hash is correct check to see if they have any active tokens
                      if ($passwordCorrect == true) {
                        // Check to see if the user has any active tokens
                        $hasActiveTokens = self::hasActiveToken($email);
                        // Decode the JSON repsonse from the function
                        $hasActiveTokens = json_decode($hasActiveTokens, true);
                        // Determine if a token has been returned
                        if (isset($hasActiveTokens['token']) && $hasActiveTokens['token'] != null) {
                          // Pull the token from the JSON response
                          $token = $hasActiveTokens['token'];
                          // Return the token in JSON format
                          return response()->json(['status' => 'success', 'message' => 'active token found', 'token' => $token, 'isNew' => $isNew], 200);
                          // If there are no active tokens found in the database generate a new one
                        } else {
                          // Set the default timezone just in case
                          date_default_timezone_set('UTC');
                          // set the expiration time of the new token
                          $expiration = strtotime('+2 hours');
                          // Generate a new token
                          $token = self::genToken($email, $expiration);
                          // Store the token in the database
                          $bool = DB::insert('INSERT INTO `active_tokens` (email, token, expiration) VALUES(:email, :token, :expiration)', [':email' => $email, ':token' => $token, ':expiration' => $expiration]);
                          if ($bool != false) {
                            // Return the token to the user
                            return response()->json(['status' => 'success', 'message' => 'new token created', 'token' => $token, 'isNew' => $isNew], 200);
                          } else {
                            return response()->json(['status' => 'fail', 'message' => 'could not generate token'], 401);
                          }
                        }
                      } else {
                        return response()->json(['status' => 'fail', 'message' => 'incorrect password'], 401);
                      }
                    } else {
                      return response()->json(['status' => 'fail', 'message' => 'account is expired'], 401);
                    }
                  } else {
                    return response()->json(['status' => 'fail', 'message' => 'account is not enabled'], 402);
                  }
                } else {
                  return response()->json(['status' => 'fail', 'message' => 'user not found'], 401);
                }
              } else {
                return response()->json(['status' => 'fail', 'message' => 'no records found'], 401);
              }
            } else {
              return response()->json(['status' => 'fail', 'message' => 'password not set'], 401);
            }
          } else {
            return response()->json(['status' => 'fail', 'message' => 'invalid email type'], 401);
          }
        } else {
          return response()->json(['status' => 'fail', 'message' => 'email not set'], 401);
        }
      } else {
        return response()->json(['status' => 'fail', 'message' => 'null data returned'], 401);
      }
    } else {
      return response()->json(['status' => 'fail', 'message' => 'payload not set'], 401);
    }
  }


  /**
   *
   *
   */
  public static function checkToken(Request $request) {
    if ($request->filled("token")) {
      $token = $request->input("token");
      $token = filter_var($token, FILTER_SANITIZE_STRING);
      $validated = self::validateToken($token);
      return $validated;
    } else {
      return response()->json(['status' => 'fail', 'message' => 'missing arguments'], 401);
    }
  }


  /**
   *
   *  Get the user's role from the `subscriber` table
   *
   *  @param    $request    POST JSON Object     {token : "rwe345ertertwer...23s4s3d34fs5df34s5df"}
   *
   *  @return               Integer | Boolean    Returns the users role (0,1,2,3) or false if unsuccessful
   *
   */
  public static function getRole(Request $request) {
    // Decrypt the data from the request
    $request = Encrypt::retrieveJSONData($request);
    // Determine if the token is set
    if (isset($request["token"])) {
      $token = $request["token"];
      $token = filter_var($token, FILTER_SANITIZE_STRING);
      // Check if the token is valid
      $isTokenValid = self::validateToken($token);
      $isTokenValid = json_decode($isTokenValid, true);
      if ($isTokenValid["message"] === true) {
        // Get the token owner
        $tokenOwner = self::tokenOwner($token);
        $ownerId    = $tokenOwner['id'];
        $ownerEmail = $tokenOwner['email'];
        $query = DB::select("SELECT `role` FROM `subscribers` WHERE `id`=:id AND `email`=:email", ["id" => $ownerId, "email" => $ownerEmail]);
        // If a record is found in the table spit it out
        if (count($query) > 0) {
          $query = array_column($query, "role");
          $query = $query[0];
          return response()->json(['status' => 'success', 'message' => $query], 200);
        } else {
          return response()->json(['status' => 'fail', 'message' => 'bad request'], 400);
        }
      } else {
        // Return 401 Unauthorized -- Bad token
        return response()->json(['status' => 'fail', 'message' => 'invalid token', 'token' => $token, 'isValidResponse' => $isTokenValid], 401);
      }
    } else {
      // Token was not received by the server
      return response()->json(['status' => 'fail', 'message' => 'token not set'], 401);
    }
  }

}
