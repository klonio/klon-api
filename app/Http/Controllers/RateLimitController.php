<?php
//
// namespace App\Http\Controllers;
//
// use App\Klon;
// use Illuminate\Http\Request;
// use Illuminate\Support\Facades\DB;
// use App\Http\Controllers\EncryptController as Encrypt;
//
// class RateLimitController extends Controller {
//
//
//   /**
//    *
//    *  Set the last request to right now
//    *
//    *  @param    token     String            The users active token
//    *
//    *  @return             Number|Boolean    Returns the last request time
//    *
//    */
//   private static function setLastRequest($token) {
//     // Check to see if this token exists in the database
//     $isActiveToken = DB::select("SELECT `token`,`expiration` FROM `active_tokens` WHERE `token`=:token", ['token' => $token]);
//     if (count($isActiveToken) > 0) {
//       // Get the token
//       $activeToken = array_column($isActiveToken, "token");
//       $activeToken = $activeToken[0];
//       // Get the token expiration
//       $tokenExpiration = array_column($isActiveToken, "expiration");
//       $tokenExpiration = $tokenExpiration[0];
//       // Get the current timestamp
//       $currentTime = time();
//       // Update the database last_request value
//       $updateLastRequest = DB::update("UPDATE `active_tokens` SET `last_request`=:currentTime WHERE `token`=:token",['currentTime' => $currentTime, 'token' => $token]);
//       if ($updateLastRequest == 1) {
//         return $currentTime;
//       } else {
//         return false;
//       }
//     } else {
//       return false;
//     }
//   }
//
//
//   /**
//    *
//    *  Get the last request time
//    *
//    *  @param     token      String             The users active token
//    *
//    *  @return                Number|Boolean    Returns the last request time or false
//    *
//    */
//   private static function getLastRequest($token) {
//     // Check to see if this token exists in the database
//     $lastRequestTime = DB::select("SELECT `last_request` FROM `active_tokens` WHERE `token`=:token", ['token' => $token]);
//     // Check to see if we have any results returned from the database
//     if (count($lastRequestTime) > 0) {
//       $lastRequestTime = array_column($lastRequestTime, "last_request");
//       $lastRequestTime = $lastRequestTime[0];
//       return (int)$lastRequestTime;
//     } else {
//       return false;
//     }
//   }
//
//
//   /**
//    *
//    *  Determine number of requests per minute the user is allowed to make based on their role
//    *  First, get their role from their token
//    *  Second, determine the rpm based on role
//    *  Finally return the result
//    *
//    *  @param     token      String             The users active token
//    *
//    *  @return               Integer|Boolean    The users limit in times per minute -or- false
//    *
//    */
//   private static function getUsersLimit($token) {
//     // Get the users email from their token
//     $email = DB::select("SELECT `email` FROM `active_tokens` WHERE `token`=:token", ['token' => $token]);
//     // Ensure there's a record
//     if (count($email) > 0) {
//       // Get the email address
//       $email = array_column($email, "email");
//       $email = $email[0];
//       // Get the users role
//       $role  = DB::select("SELECT `role` FROM `subscribers` WHERE `email`=:email", ['email' => $email]);
//       // Make sure that a record was returned
//       if (count($role) > 0) {
//         $role = array_column($role, "role");
//         $role = $role[0];
//         //
//         switch ($role) {
//           // The Void
//           case 0:
//             return 61;
//             break;
//           // Econo Plan
//           case 1:
//             return 20;
//             break;
//           // Regular Plan
//           case 2:
//             return 30;
//             break;
//           // Premium Plan
//           case 3:
//             return 50;
//             break;
//           // Trial Account
//           case 9:
//             return 10;
//             break;
//           default:
//             return false;
//         }
//       } else {
//         // Role not found
//         return false;
//       }
//     } else {
//       // Token holders email not found
//       return false;
//     }
//   }
//
//
//   /**
//    *
//    *  @param       Request       The request object containing the users token
//    *
//    *  @return      Boolean       To allow, or not to allow, that is the question this function seeks to answer
//    *
//    *
//    */
//   public static function allowRequest($token) {
//     // Get the last request
//     $lastRequestTime = self::getLastRequest($token);
//     if (!is_numeric($lastRequestTime)) {
//       return false;
//     }
//     // Get the users limit (Req Per Minute)
//     $maxRequestRate  = self::getUsersLimit($token);
//     if (!is_numeric($maxRequestRate)) {
//       return false;
//     }
//     // Get the current time
//     $currentTime     = time();
//     // Determine if the last request has enough time between it and now
//     $requestsPerSecond = round(60 / $maxRequestRate);
//     // Was the last request at least $requestsPerSecond ago?
//     if ($currentTime - $lastRequestTime >= $requestsPerSecond) {
//       $setRequestTime = self::setLastRequest($token);
//       if (is_numeric($setRequestTime)) {
//         return true;
//       } else {
//         return false;
//       }
//     } else {
//       return false;
//     }
//   }
//
// }

?>
