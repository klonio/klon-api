<?php

namespace App\Http\Controllers;

use App\Klon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AddressController extends Controller
{
    /**
     *
     *  Get the city, state, zip, coordinates, and county from the database
     *
     *  @return  Array   {firstname:$name,gender:$gender}
     *
     */
    private static function getLocation() {
      $rowCount = 41726;
      $randomInteger = rand(1,$rowCount);
      $values = DB::select("SELECT `city`,`state_code`,`zip`,`latitude`,`longitude`,`county` FROM `cities` WHERE id = :randomInteger",['randomInteger' => $randomInteger]);
      foreach($values as $value) {
        $city = $value->city;
        $state = $value->state_code;
        $zip = $value->zip;
        $latitude = $value->latitude;
        $longitude = $value->longitude;
        $county = $value->county;
      }
      return [
        "city" => $city,
        "state" => $state,
        "zip" => $zip,
        "latitude" => $latitude,
        "longitude" => $longitude,
        "county" => $county
    ];
  }


  /**
   *
   *  Pull a road name from the table `roads`
   *
   *
   */
  private static function getRoad() {
    // Number of rows in the `roads` table
    $rowCount = 52067;
    // Pick a random `id` from the `roads` table
    $randomInteger = rand(1,$rowCount);
    // Query the table
    $values = DB::select("SELECT `road` FROM `roads` WHERE id = :randomInteger", ['randomInteger' => $randomInteger]);
    // Pull the result out of the array
    $result = array_column($values, 'road');
    // Return the value
    return $result[0];
  }


  /**
   *
   *  Make up an address number
   *
   *  @return    Integer   Address number
   *
   */
  private static function genNumbers() {
    $address = rand(1, 99999);
    return $address;
  }


  /**
   *
   *
   *
   *
   */
  public static function genAddress() {
    $location = self::getLocation();
    $numbers = self::genNumbers();
    $road = self::getRoad();
    return [
      'streetAddress' =>  $numbers . " " . $road,
      'city'          =>  $location['city'],
      'state'         =>  $location['state'],
      'zip'           =>  $location['zip'],
      'county'        =>  $location['county']
      // Currently unused...
      // 'latitude'      =>  $location['latitude'],
      // 'longitude'     =>  $location['longitude']
    ];
  }

}
