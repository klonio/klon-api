<?php

namespace App\Http\Controllers;

use App\Klon;
use Illuminate\Http\Request;
use Illuminate\Http\Stripe;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\AuthController as Auth;


class StripeController extends Controller {
  public function __construct() {
    self::middleware('auth',['only' => [
      'setSubscriptionAutoRenew'
      ]]);
  }
  /**
   *
   *  Get the POST data from the extension
   *  Check to make sure this user is not already a paying customer
   *
   */
  public static function chargeBasicPlan($email) {
    //
    $emailRegex = '/^[a-zA-Z0-9_+&*-]+(?:\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$/';
    if (gettype($email) === "string" && preg_match($emailRegex, $email) === 1) {
      // Sanitize the email variable
      $email = filter_var($email, FILTER_SANITIZE_EMAIL);
      // Check to see if theres any active subscriptions for this user
      $subscription = self::hasActiveSubscription($email);
      // If the customer is already subscribed to a subscription, list the subscription and give the option of
      // Upgrading, downgrading, or cancelling at end of term buttons
      if ($subscription === false) {
        // Return the view of upgrade.php
        return view('upgrade', ['email' => $email]);
      } else if ($subscription === true) {
        //
        return view('activeSubscription', ['email' => $email]);
      } else {
        return view('error', ['email' => $email]);
      }
    }
  }


  /**
   *
   *  Update Credit Card Plans
   *
   *  @param      String      $token      JWT Token
   *
   *  @return     Object
   *
   */
  public static function updateCreditCard() {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      // Get the incoming data
      $post = $_POST;
      // Sanitize the $_POST array
      $post = filter_var_array($post, FILTER_SANITIZE_STRING);
      // Make sure we have the data that is required to update the users card
      if (isset($post['token']) && isset($post['stripeToken'])) {
        $token        = $post['token'];
        $stripeToken  = $post['stripeToken'];
        // ** BEGIN updating the users card ** //
        // Get the customer ID based on this token
        $tokenOwner = Auth::tokenOwner($token);
        // Make sure we receive valid data back from the tokenOwner function
        if (!empty($tokenOwner) && $tokenOwner !== false) {
          // Get the email address and make sure it is from a valid subscriber
          if (isset($tokenOwner['email']) && Auth::isSubscriber($tokenOwner['email']) ===  true) {
            // Good, set the email variable
            $email = $tokenOwner['email'];
            // Get the customer ID based on this email address
            $customerId = self::getStripeCustomer($email);
            // Make sure we have a legit customer id
            if ($customerId !== false) {
              // Use your test API key (switch to the live key later)
              \Stripe\Stripe::setApiKey($_ENV['STRIPE_KEY']);
              try {
                $cu = \Stripe\Customer::update(
                  $customerId, // stored in your application
                  [
                    'source' => $post['stripeToken'] // obtained with Checkout
                  ]
                );
                $success = "Your card details have been updated!";
                // return response()->json(["status" => "success", "message" => $success], 200);
                return view('succeed', ['success' => $success]);
              }
              catch(\Stripe\Exception\CardException $e) {
                // Use the variable $error to save any errors
                // To be displayed to the customer later in the page
                $body  = $e->getJsonBody();
                $err   = $body['error'];
                $error = $err['message'];
              }
            } else {
              return response()->json(["status" => "fail", "message" => "no customer found"]);
            }
          }
        } else {
          return response()->json(["status" => "fail", "message" => "no user found for this token"]);
        }
        //
      } else {
        return response()->json(["status" => "fail", "message" => "missing arguments"], 400);
      }
    }
  }


  public static function showUpdateCreditCard(Request $request) {
    if ($request->has('token')) {
      $token = $request->input('token');
      $token = filter_var($token, FILTER_SANITIZE_STRING);
      // Check if valid token... regex first
      $regExToken = '/^[A-Za-z0-9\-\_\=]+\.[A-Za-z0-9\-\_\=]+\.?[A-Za-z0-9\-\_\.\+\/\=]*$/';
      $tokenMatch = preg_match($regExToken, $token);
      if ($tokenMatch === 1) {
        // Check if this is an active token
        $isValidToken = Auth::validateToken($token);
        //
        $isValidToken = json_decode($isValidToken, true);
        if ($isValidToken["message"] === true) {
          // Looks good, show the page
          return view('update', ['token' => $token]);
        } else {
          return response()->json(["status" => "fail", "message" => "invalid token"], 401);
        }
      } else {
        return response()->json(["status" => "fail", "message" => "invalid token"], 401);
      }
    } else {
      return response()->json(["status" => "fail", "message" => "missing arguments"], 400);
    }
  }


  /**
   *
   *  Get the POST data from the extension
   *
   */
  public static function getBasicDetails() {
    // Sanitize the $_POST array
    $POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);
    // Get the email address from the $_POST request
    $email = $POST['stripeEmail'];
    $regexEmail = '/^[a-zA-Z0-9_+&*-]+(?:\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$/';
    if (preg_match($regexEmail, $email) === 1) {
      // Get the Stripe token from the $_POST request
      $token = $POST['stripeToken'];
      // Get the plan number from the $_POST request
      $plan  = (int)$POST['stripePlan'];
      // Set scope of variable
      $subscribeToPlan = null;
      // Make sure the email address is assigned to a real subscriber
      $checkIfSubscriber = self::isSubscriber($email);
      if ($checkIfSubscriber !== false) {
        // Check to see if the customer already has an active subscription
        if (!self::hasActiveSubscription($email)) {
          // Check if we already have a customer record for this customer
          $customer = self::getStripeCustomer($email);
          // If the email address does not belong to a current customer create a new one
          if ($customer === false) {
            // Create the customer record
            $customer = self::createCustomer($email, $token);
          }
          // These are the plan id's from Stripe
          $plans = [
            // Testing Basic Plan
            // "basic" => "plan_FNJeTpJzj94F5H",
            // Production Basic Plan
            "basic"   => "plan_FfxLTfDJCNqH4z",
            // Testing Regular Plan
            //"regular" => "plan_FNJeUqPpguW3KS",
            // Production Regular Plan
            "regular" => "plan_FfxMmtEU9CujO3",
            // Testing Premium Plan
            // "premium" => "plan_FNJfdcnNSsNAhK",
            "premium" => "plan_FfxMG7T0MLVCHo",
          ];
          //
          switch ($plan) {
            case 1:
            $subscribeToPlan = $plans["basic"];
            break;
            case 2:
            $subscribeToPlan = $plans["regular"];
            break;
            case 3:
            $subscribeToPlan = $plans["premium"];
            break;
          }
          // Subscribe the customer to the plan they selected
          $subscription = \Stripe\Subscription::create([
            'customer' => $customer,
            'items' => [
              ['plan' => $subscribeToPlan]
            ],
            'trial_end' => strtotime('+30 days'),
          ]);
        } else {
          // Show the user the ACTIVE SUBSCRIPTION FOUND page
          return view('activeSubscription');
        }
        // Verify that the transaction was successful
        if ($subscription->status === "active") {
          // Store the subscription data in the database
          $storeTransaction = self::storeTransactionData($email, $subscription);
          if ($storeTransaction) {
            // Set the users new expiration time on their plan to $subscription->period_end //
            $updateSubscriber = self::updateSubscriberPlan($email, $plan, $subscription->current_period_end);
            if ($updateSubscriber) {
              // Return a JSON message letting the extension know to display the success page and return them to the login page
              // return response()->json(["status" => "success", "message" => "subscription successful"], 200);
              // return view('activeSubscription');
              //
              //
              $query = DB::select("SELECT `is_new` FROM `subscribers` WHERE `email`=:email", ['email' => $email]);
              // Find out if the user is new
              if (gettype($query) === "array") {
                $isNew = array_column($query, 'is_new');
                $isNew = $isNew[0];
                // If the user is new, change the setting
                if ($isNew === 1) {
                  try {
                    DB::update("UPDATE `subscribers` SET `is_new`=0 WHERE email=:email", ['email' => $email]);
                  } catch (Exception $e) {
                    return response()->json(['status' => 'fail', 'message' => 'could not update user'], 500);
                  }
                }
              }
              return response()->make(view('paymentSuccess'), 200);
            } else {
              return response()->json(["status" => "fail", "message" => "error updating account status please contact support at help@klon.io"], 400);
            }
          } else {
            // The subscription was not successful. Display an error message to the user.
            return response()->json(["status" => "fail", "message" => "failed to store subscription data"], 400);
          }
          // Display a success message to the user and return to the login page (Run localStorage.clear())
        } else if ($subscription->status === "trialing") {
          // The user is now successfully trialing the software. Give them premium access and set the expiration time.
          //
          // STORE THE SUBSCRIPTION DATA IN THE DATABASE
          $storeSubscriptionData = self::storeTransactionData($email, $subscription);
          //  SET THE SUBSCRIBER UP AS A PREMIUM USER FOR 30 DAYS
          $updateSubscriber      = self::updateSubscriberPlan($email, 3, $subscription->current_period_end);
          //  RETURN A SUCCESS MESSAGE AND FORCE LOGIN (Run localStorage.clear in the extension)
          if ($updateSubscriber) {
            $query = DB::select("SELECT `is_new` FROM `subscribers` WHERE `email`=:email", ['email' => $email]);
            // Find out if the user is new
            if (gettype($query) === "array") {
              $isNew = array_column($query, 'is_new');
              $isNew = $isNew[0];
              // If the user is new, change the setting
              if ($isNew === 1) {
                try {
                  DB::update("UPDATE `subscribers` SET `is_new`=0 WHERE email=:email", ['email' => $email]);
                } catch (Exception $e) {
                  return response()->json(['status' => 'fail', 'message' => 'could not update user'], 500);
                }
              }
            }
            // Return a JSON message letting the extension know to display the success page and return them to the login page
            // return response()->json(["status" => "success", "message" => "subscription successful"], 200);
            return response()->make(view('paymentSuccess'), 200);
          } else {
            return response()->json(["status" => "fail", "message" => "error updating account status please contact support at help@klon.io"], 400);
          }
          //
        } else {
          // The subscription was not successful. Display an error message to the user.
          return response()->json(["status" => "fail", "message" => "payment failed"], 400);
        }
      } else {
        return response()->json(["status" => "fail", "message" => "please sign up first"], 400);
      }
    } else {
      return response()->json(["status" => "fail", "message" => "invalid email address"], 400);
    }
  }


  /**
   *
   *  Store the transaction details in the database
   *
   *  @param     String       $email           The users email address
   *  @param     Object       $subscription    The subscription object returned by the Stripe API
   *
   *  @return    Boolean                       Returns true if successful, false otherwise
   *
   */
  private static function storeTransactionData($email = false, $subscription = false) {
    // Make sure the required arguments were set
    if ($email !== false || $subscription !== false) {
      // Store the charge in the database
      $txDbStore = DB::insert("INSERT INTO `transactions` (id, amount, email, currency, customer, item, receipt, plan, product, subscription, status, period_start, period_end, created_at) VALUES(:id, :amount, :email, :currency, :customer, :item, :receipt, :plan, :product, :subscription, :status, :period_start, :period_end, :created_at)", [
        'id' => $subscription->id,
        'amount' => $subscription->plan['amount'],
        'email' => $email,
        'currency' => $subscription->plan['currency'],
        'customer' => $subscription->customer,
        'item' => $subscription->plan['nickname'],
        'receipt' => $subscription->items['url'],
        'plan' => $subscription->plan['id'],
        'product' => $subscription->plan['product'],
        'subscription' => $subscription->items['data'][0]['id'],
        'status' => $subscription->status,
        'period_start' => $subscription->current_period_start,
        'period_end' => $subscription->current_period_end,
        'created_at' => $subscription->created]);
        return true;
    } else {
      // Arguments were not sent with method call, return false
      return false;
    }
  }


  /**
   *
   *  Determine if an email address is already assigned to a customer, if so return their customer id otherwise return false
   *
   *  @param       String      $email       Customers email address
   *
   *  @return      String | Boolean         Returns the customer id if they are a customer, False otherwise
   *
   */
  private static function isCustomer($email = false) {
    // Sanitize the email address
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    // Validate the email address
    if ($email !== false) {
      $regexEmail = '/^[a-zA-Z0-9_+&*-]+(?:\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$/';
      if (preg_match($regexEmail, $email) === 1) {
        $query = DB::select("SELECT `customer` FROM `transactions` WHERE `email`=:email", ['email' => $email]);
        if (!empty($query)) {
          $customer = array_column($query, 'customer');
          if (!empty($customer)) {
            $customer = $customer[0];
            return $customer;
          } else {
            return false;
          }
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }


  /**
   *
   *  Create a Stripe customer
   *
   *  @param     String       $email       The customers email address
   *
   *  @return    Object                    Cuustomer object returned by Stripe
   *
   */
  private static function createCustomer($email, $token) {
    // Set the Stripe API key
    \Stripe\Stripe::setApiKey($_ENV['STRIPE_KEY']);
    // Check to see if we already have a customer for this user in stripe
    $customerExists = self::getStripeCustomer($email);
    if ($customerExists === false) {
      // Create the customer
      $customer = \Stripe\Customer::create(array(
        "email" => $email,
        "source" => $token
      ));
      // Return the customer object from Stripe
      return $customer;
    } else {
      return false;
    }
    return false;
  }


  /**
   *
   *  Check to see if this customer exists in the database and Stripe
   *
   *  @param       String             $email       The customers email address
   *
   *  @return      String | Boolean                Returns the customer_id or false
   *
   */
  private static function getStripeCustomer($email) {
    // Sanitize the variable
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    // Set the Stripe API key
    \Stripe\Stripe::setApiKey($_ENV['STRIPE_KEY']);
    $customers = \Stripe\Customer::all(["email" => $email, "limit" => 1]);
    // Check if empty
    if (empty($customers->data)) {
      // The customer email is not found
      return false;
    } else {
      // The customer was found in Stripe, return their ID
      return $customers->data[0]['id'];
    }
    return false;
  }


  /**
   *
   *  Check to see if a user is currently trialing
   *
   *  @param      String       $subscriptionId        The ID of the subscription we want to check
   *
   *  @return     Boolean                             Returns true if trialing, false otherwise
   *
   */
  private static function isTrialing($subscriptionId) {
    $subscriptionId = filter_var($subscriptionId, FILTER_SANITIZE_STRING);
    if (gettype($subscriptionId) === "string") {
      // Set the Stripe API key
      \Stripe\Stripe::setApiKey($_ENV['STRIPE_KEY']);
      // Retrieve the subscription
      $subscription = \Stripe\Subscription::retrieve($subscriptionId);
      // Determine if trialing
      $status = $subscription->status;
      if ($status === "trialing") {
        return true;
      } else {
        return false;
      }
    } else {
      // Invalid type for $subscriptionId
      return false;
    }
  }


  /**
   *
   *  Update a users role within the klon subscriber database
   *
   *  @param    Integer             $role        The subscribers new role within the system
   *  @param    String              $email       The subscribers email address
   *
   *  @return   Integer,Boolean                  Number of rows updated if successful, false otherwise
   *
   */
  private static function changeRole($role, $email) {
    if (gettype($role) === "integer") {
      // Sanitize the email variable
      $email = filter_var($email, FILTER_SANITIZE_EMAIL);
      // Validate the email address
      if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        // Make sure they are a subscriber
        if (self::isSubscriber($email)) {
          // Change the role
          $query = DB::update("UPDATE `subscribers` SET role=:role WHERE email=:email", ['role' => $role, 'email' => $email]);
          // Return the number of rows updated
          if (gettype($query) == "integer") {
            return $query;
          } else {
            return false;
          }
        } else {
          // Not a registered subscriber
          return false;
        }
      } else {
        // Invalid email address
        return false;
      }
    } else {
      // Wrong var type for $role -- Must be integer
      return false;
    }
  }


  /**
   *
   *  Update the users subscription in the subscribers table
   *  Sets the new expiration time and the plan that the user is signed up for
   *  .......Plans........
   *  0  -  The Void
   *  1  -  Basic   Plan
   *  2  -  Regular Plan
   *  3  -  Premium Plan
   *  ....................
   *  @param    String      $email          The subscribers email address
   *  @param    Integer     $plan           The plan the user subscribed to
   *  @param    Integer     $expiration     The subscribers new expiration date
   *
   *  @return   Boolean                     True if successful, false otherwise
   *
   */
  private static function updateSubscriberPlan($email = false, $plan = false, $expiration = false) {
    if ($email !== false || $plan !== false || $expiration !== false) {
      // Make sure the email address is valid
      $regex = '/^[a-zA-Z0-9_+&*-]+(?:\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$/';
      if (gettype($email) === "string" && preg_match($regex, $email) === 1) {
        // Update the users account standing within the database
        $updateSubscriber = DB::update("UPDATE `subscribers` SET `expires`=:periodEnd, `role`=:plan, `expired`=0 WHERE `email`=:email", ['email' => $email, 'plan' => $plan, 'periodEnd' => $expiration]);
        if ($updateSubscriber > 0) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      // throw new Exception('email, plan, and expiration must be set');
      return false;
    }
  }


  /**
   *
   *  Find out it an account has expired
   *
   *  @param    String     $email      The subscribers email address
   *
   *  @return   Boolean                 True if expired or if errors False if not expired
   *
   */
  private static function isExpired($email) {
    $regex = '/^[a-zA-Z0-9_+&*-]+(?:\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$/';
    if (gettype($email) === "string" && preg_match($regex, $email) === 1) {
      // Get the current time
      $currentTime = time();
      // Get the period_end time
      $query = DB::select("SELECT `expires` FROM `subscribers` WHERE `email`=:email", ['email' => $email]);
      // Make sure results were returned
      if (!empty($query)) {
        $expiration = array_column($query, 'expires');
        if (!empty($expiration)) {
          $expiration = $expiration[0];
          // Is the currentTime larger than the expiration ?
          if ($currentTime > $expiration) {
            return true;
          } else {
            return false;
          }
        } else {
          return true;
        }
      } else {
        return true;
      }
    } else {
      return null;
    }
  }


  /**
   *
   *  @param     String      $email      The users email address that they used when signing up for Klon
   *
   *  @return    Boolean                 True if has active subscription, false otherwise
   *
   */
  private static function hasActiveSubscription($email = false) {
    if ($email !== false) {
      // Sanitize the email address
      $email = filter_var($email, FILTER_SANITIZE_EMAIL);
      // Validate the email address
      $email = filter_var($email, FILTER_VALIDATE_EMAIL);
      if ($email !== false) {
        // Get any subscriptions the user may have
        $subscriptions = self::getSubscriptionId($email);
        if ($subscriptions !== false && gettype($subscriptions) === "array") {
          // Set the Stripe API Key
          \Stripe\Stripe::setApiKey($_ENV['STRIPE_KEY']);
          // Loop through and determine if any are status active
          foreach($subscriptions as $subscription) {
            // Retrieve the subscription
            try {
              $sub = \Stripe\Subscription::retrieve($subscription);
              if ($sub->status === "active" || $sub->status === "trialing") {
                // An active or trialing subscription was found for this user
                return true;
              }
            } catch(Exception $e) {
              return false;
            }
          }
          // No subscriptions were found to be active or trialing
          return false;
        } else {
          // No subscriptions found or error
          return false;
        }
      } else {
        // Email was not valid
        return false;
      }
    } else {
      // Email was not set
      return false;
    }
  }


  /**
   *
   *  Check to see if this user has a current subscription right now
   *
   *  @param     String                 $email         The users email address that they used when signing up for Klon
   *
   *  @return    String||Boolean                       Returns the users subscription, if not found returns false
   *
   */
  private static function getActiveSubscription($email = false) {
    if ($email !== false) {
      // Get the current time as a unix timestamp and compare to the users expiration
      $currentTime = time();
      // Get the period_end from the transaction table
      try {
        $query = DB::select("SELECT `email` FROM `transactions` WHERE `email`=:email AND `status`='active'", ['email' => $email]);
        if (gettype($query) === "array" || gettype($query) === "object") {
          if (!empty($query)) {
            // RETURN THE SUBSCRIPTION {email, status, plan, period_end}
            $subscriber = array_column($query, 'email');
            $subscriber = $subscriber[0];
            return $subscription;
          } else {
            return false;
          }
        } else {
          return false;
        }
      } catch (Exception $e) {
        return false;
        // return $e->getMessage();
      }
    } else {
      return false;
    }
  }


  /**
   *
   *  Get the `period_end` value from the transaction table
   *
   *  @param     String      $email      Subscribers email address
   *
   *  @return    Integer | Boolean       Return the period_end or false
   *
   */
  private static function getPeriodEnd($email = false) {
    // Make sure an email address was provided
    if ($email !== false) {
      // Sanitize the email address
      $email = filter_var($email, FILTER_SANITIZE_EMAIL);
      if (self::isSubscriber($email) !== false) {
        $query = DB::select("SELECT `period_end` FROM `transactions` WHERE `email`=:email", ['email' => $email]);
        if (!empty($query)) {
          $periodEnd = array_column($query, 'period_end');
          $periodEnd = $periodEnd[0];
          return $periodEnd;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }


  /**
   *
   *  Make sure the email address belongs to a user
   *
   *  @param    String      $email     Email address we are checking to see if they are a subscriber
   *
   *  @return   Boolean                True if they are a subscriber, False otherwise
   *
   */
  private static function isSubscriber($email = false) {
    // Make sure an email address was provided
    if ($email !== false) {
      // Sanitize the email address
      $email = filter_var($email, FILTER_SANITIZE_EMAIL);
      // Validate the email address
      if (filter_var($email, FILTER_VALIDATE_EMAIL) !== false) {
        // Check the database for this email address
        $isSubscriber = DB::select("SELECT `email` FROM `subscribers` WHERE `email`=:email", ['email' => $email]);
        // Check if we have any results
        if (!empty($isSubscriber)) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }


  /**
   *
   *  Get a users expiration time according to the subscribers table
   *
   *  @param     String      $email     Email address of the subscriber
   *
   *  @return    Integer | Boolean      Returns the timestamp -or- false
   *
   */
  private static function getExpirationTime($email = false) {
    // Make sure that the email address is that of a subscriber
    if (self::isSubscriber($email) !== false) {
      //
      $query = DB::select("SELECT `expires` FROM `subscribers` WHERE `email`=:email", ['email' => $email]);
      // Check to see if values are returned from the table
      if (!empty($query)) {
        // Return only the column `expires` and the first result from it
        $expiration = array_column($query, 'expires');
        $expiration = $expiration[0];
        return $expiration;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }


  /**
   *
   *  Basic Stripe Endpoint
   *
   *
   */

  public static function stripeEndpoint() {
    // Get your webhook secret from the Stripe webhook settings...
    $wh_secret = "whsec_JgSnFfgEB7tffpfuPPxUECAEiJECj4ja";
    // Get the requests webhook secret to verify this data is coming from Stripe
    $sig_header = $_SERVER["HTTP_STRIPE_SIGNATURE"];
    $payload = @file_get_contents('php://input');
    $event = null;
    try {
      $event = \Stripe\Webhook::constructEvent($payload, $sig_header, $wh_secret);
      // Handle the event
      switch ($event->type) {
        // Trial is about to end (in 3 days) Email the user and tell them to get ready to PAY ME!!!!!!
        // case 'customer.subscription.trial_will_end':
          // break;
        //
        // case 'payment_intent.succeeded':
          // $paymentIntent = $event->data->object; // contains a \Stripe\PaymentIntent
          // handlePaymentIntentSucceeded($paymentIntent);
          // break;
        // case 'payment_method.attached':
          // $paymentMethod = $event->data->object; // contains a \Stripe\PaymentMethod
          // handlePaymentMethodAttached($paymentMethod);
          // break;
        case 'invoice.payment_succeeded':
          // return response()->json(["status" => "success", "message" => "congratulations it frickin' works!"], 200);
          return self::endpointInvoicePaymentSucceed($event);
          break;
        case 'invoice.payment_failed':
          return self::endpointInvoicePaymentFailed($event);
          break;
        // ... handle other event types
        default:
          // Unexpected event type
          http_response_code(400);
          exit();
        }
      http_response_code(200);
    } catch(\UnexpectedValueException $e) {
      // Invalid payload
      http_response_code(400); // PHP 5.4 or greater
      exit();
    }
  }


  /**
  *
  *  Stripe invoice.payment_succeeded event handler
  *
  */
  public static function endpointInvoicePaymentSucceed($event) {
    //  Get all the data coming in from the object
    $eventId         = $event->id;
    $created         = $event->created;
    $amountDue       = $event->data['object']['amount_due'];
    $amountRemaining = $event->data['object']['amount_remaining'];
    $email           = $event->data['object']['customer_email'];
    $paid            = $event->data['object']['paid'];
    $status          = $event->data['object']['status'];
    $periodEnd       = $event->data['object']['period_end'];
    $periodStart     = $event->data['object']['period_start'];
    $subscription    = $event->data['object']['subscription'];
    $subtotal        = $event->data['object']['subtotal'];
    $total           = $event->data['object']['total'];
    $planAmount      = $event->data['object']['lines']['data'][0]['plan']['amount'];
    $planId          = $event->data['object']['lines']['data'][0]['plan']['id'];
    $query = DB::insert("INSERT INTO `stripeEvents`
      (event_id, created, amount_due, amount_remaining, email, paid, status, period_end, period_start, plan_id, plan_amount, subscription, subtotal, total)
      VALUES(:event_id, :created, :amount_due, :amount_remaining, :email, :paid, :status, :period_end, :period_start, :plan_id, :plan_amount, :subscription, :subtotal, :total)",
      [
        'event_id' => $eventId,
        'created' => $created,
        'amount_due' => $amountDue,
        'amount_remaining' => $amountRemaining,
        'email' => $email,
        'paid' => $paid,
        'status' => $status,
        'period_end' => $periodEnd,
        'period_start' => $periodStart,
        'plan_id' => $planId,
        'plan_amount' => $planAmount,
        'subscription' => $subscription,
        'subtotal' => $subtotal,
        'total' => $total
      ]
    );
    if ($query == true) {
      // return response()->json(["status" => "success", "message" => "data stored successfully"], 200);
      // Display the success view with proper js (window.postMessage)
      // return view('paymentSuccess');
      // Update the users account (set role and expires)
      $arrPlan = [
        // The Void -- For testing purposes
        0 => "plan_00000000000000",
        // Basic Plan
        1 => "plan_FfxLTfDJCNqH4z",
        // Regular Plan
        2 => "plan_FfxMmtEU9CujO3",
        // Premium Plan
        3 => "plan_FfxMG7T0MLVCHo"
      ];
      if (in_array($planId, $arrPlan)) {
        $role = array_search($planId, $arrPlan);
        if ($role !== false) {
          $updatePlan = self::updateSubscriberPlan($email, $role, $periodEnd);
          if ($updatePlan == true) {
            // Return Success!
            return response()->json(["status" => "success", "message" => "subscriber updated successfully"], 200);
          } else {
            // Return error!!
            return response()->json(["status" => "fail", "message" => "failed to update subscriber"], 400);
          }
        } else {
          //
          return response()->json(["status" => "fail", "message" => "bad argument for plan"], 400);
        }
      } else {
        // Return error
        return response()->json(["status" => "fail", "message" => "bad argument for plan"], 400);
      }
    } else {
      // Return error
      return response()->json(["status" => "fail", "message" => "failed to add record"], 400);
    }
  }


  /**
   *
   *  @param      Object      $event      Event from StripeEvent invoice.payment_failed
   *
   *  Invoice.payment_failed webhook event handler
   *
   */
  public static function endpointInvoicePaymentFailed($event) {
    // Get the customers email address
    $email = $event->data['object']['customer_email'];
    // Get the customer number
    // $event->data['object']['customer'];
    // Get the subscription that the payment failed for and cancel it
    $subscriptionId = $event->data['object']['subscription'];
    // Set the Stripe API key
    \Stripe\Stripe::setApiKey($_ENV['STRIPE_KEY']);
    // Retrieve the subscription
    $subscription = \Stripe\Subscription::retrieve($subscriptionId);
    // Cancel the subscription
    $subscription->cancel();
    // Change the users role to 0 -- THE VOID
    $updateRole = DB::update("UPDATE `subscribers` SET `role`=0,`expired`=1 WHERE `email`=:email", ["email" => $email]);
    if (gettype($updateRole) === "integer" && $updateRole > 0) {
      return response()->json(["status" => "success", "message" => "subscriber updated successfully"], 200);
    } else {
      // Subscriber role was not updated ----> BAD
      return response()->json(["status" => "fail", "message" => "unable to update subscribers role"], 400);
    }
  }


  /**
   *
   *  Cancel the users account
   *
   *  @param      String      $subscription     The subscription ID from Stripe
   *
   *  @return     Boolean                       True if successful, False otherwise
   *
   */
  // public static function cancelSubscription() {
  //   // Get the incoming POST data
  //   $post           = file_get_contents('php://input');
  //   // Decode the JSON object into an associative array
  //   $post           = json_decode($post, true);
  //   // Sanitize the variables
  //   $subscriptionId = filter_var($post['subscriptionId'], FILTER_SANITIZE_STRING);
  //   $token          = filter_var($post['token'], FILTER_SANITIZE_STRING);
  //   // Validate the token
  //   $isTokenValid = Auth::validateToken($token);
  //   $isTokenValid = json_decode($isTokenValid, true);
  //   $tokenStatus  = $isTokenValid['status'];
  //   if ($tokenStatus === "success") {
  //     // Get the token owner
  //     $tokenOwner = Auth::tokenOwner($token);
  //     // Get the email address of the token owner
  //     $ownerEmail = $tokenOwner['email'];
  //     // Set the Stripe API key
  //     \Stripe\Stripe::setApiKey($_ENV['STRIPE_KEY']);
  //     // Verify that the email/token owner is the same as the owner of this subscription
  //     // Get all the subscriptions for this email address
  //     $arrSubsForEmail = self::getSubscriptionId($ownerEmail);
  //     // Check to see if this particular subscription is in the array returned
  //     if (in_array($subscriptionId, $arrSubsForEmail)) {
  //       // Cancel the subscription at the end of the term
  //       $sub = \Stripe\Subscription::update($subscriptionId,
  //         [
  //           'cancel_at_period_end' => true,
  //         ]
  //       );
  //       //
  //       return response()->json(["status" => "success", "message" => "subscription will not renew automatically"], 200);
  //     } else {
  //       return response()->json(["status" => "failed", "message" => "subscription does not belong to email address provided"], 400);
  //     }
  //   } else {
  //     return response()->json(["status" => "failed", "message" => "invalid token", 401]);
  //   }
  // }


  /**
   *
   *  Toggle wether or not a subscription will cancel at the end of term or not
   *
   *  @param       String       $subscriptionId       The ID of the subscription to set to renew automatically
   *  @param       Boolean      $autoRenew            True or False based on wether or not the subscription will automatically renew
   *
   *  @return      Boolean                            True if successful, False otherwise
   *
   */
  public static function toggleAutoRenew() {
    // Get the incoming POST data
    $post           = file_get_contents('php://input');
    // Decode the JSON object into an associative array
    $post           = json_decode($post, true);
    // Sanitize the variables
    $subscriptionId = filter_var($post['subscriptionId'], FILTER_SANITIZE_STRING);
    $token          = filter_var($post['token'], FILTER_SANITIZE_STRING);
    $autoRenew      = filter_var($post['autoRenew'], FILTER_VALIDATE_BOOLEAN);
    // Make sure that autoRenew is a boolean and not null
    if ($autoRenew !== null) {
      // Validate the token
      $isTokenValid = Auth::validateToken($token);
      $isTokenValid = json_decode($isTokenValid, true);
      $tokenStatus  = $isTokenValid['status'];
      if ($tokenStatus === "success") {
        // Get the token owner
        $tokenOwner = Auth::tokenOwner($token);
        // Get the email address of the token owner
        $ownerEmail = $tokenOwner['email'];
        // Set the Stripe API key
        \Stripe\Stripe::setApiKey($_ENV['STRIPE_KEY']);
        // Verify that the email/token owner is the same as the owner of this subscription
        // Get all the subscriptions for this email address
        $arrSubsForEmail = self::getSubscriptionId($ownerEmail);
        // Check to see if this particular subscription is in the array returned
        if (in_array($subscriptionId, $arrSubsForEmail)) {
          // Cancel the subscription at the end of the term
          $sub = \Stripe\Subscription::update($subscriptionId,
            [
              'cancel_at_period_end' => !$autoRenew,
            ]);
        //
          if ($autoRenew == true) {
            return response()->json(["status" => "success", "message" => "subscription will renew automatically"], 200);
          } else {
            return response()->json(["status" => "success", "message" => "subscription will not renew automatically"], 200);
          }
        } else {
          return response()->json(["status" => "failed", "message" => "subscription does not belong to email address provided"], 400);
        }
      } else {
        return response()->json(["status" => "failed", "message" => "invalid token", 401]);
      }
    } else {
      return response()->json(["status" => "failed", "message" => "auto renew must be of type boolean"], 400);
    }
  }


  /**
   *
   *  Change the users subscription to a new plan
   *
   *  @param       String       $token                The users token
   *  @param       String       $subscriptionId       The ID of the subscription to set to renew automatically
   *  @param       String       $newPlan              The new plan to subscribe the user to
   *
   *  @return      Boolean                            True if successful, False otherwise
   *
   */
  public static function changePlan() {
    // Get the incoming POST data
    $post           = file_get_contents('php://input');
    // Decode the JSON object into an associative array
    $post           = json_decode($post, true);
    // Sanitize the variables
    $subscriptionId = filter_var($post['subscriptionId'], FILTER_SANITIZE_STRING);
    $token          = filter_var($post['token'], FILTER_SANITIZE_STRING);
    $newPlan        = filter_var($post['newPlan'], FILTER_SANITIZE_STRING);
    // Make sure that autoRenew is a boolean and not null
    if ($newPlan !== null) {
      // Validate the token
      $isTokenValid = Auth::validateToken($token);
      $isTokenValid = json_decode($isTokenValid, true);
      $tokenStatus  = $isTokenValid['status'];
      if ($tokenStatus === "success") {
        // Get the token owner
        $tokenOwner = Auth::tokenOwner($token);
        // Get the email address of the token owner
        $ownerEmail = $tokenOwner['email'];
        // Set the Stripe API key
        \Stripe\Stripe::setApiKey($_ENV['STRIPE_KEY']);
        // Verify that the email/token owner is the same as the owner of this subscription
        // Get all the subscriptions for this email address
        $arrSubsForEmail = self::getSubscriptionId($ownerEmail);
        // Check to see if this particular subscription is in the array returned
        if (in_array($subscriptionId, $arrSubsForEmail)) {
          switch($newPlan) {
            case 'basic':
              // $plan = 'plan_FNJeTpJzj94F5H';
              $plan = 'plan_FfxLTfDJCNqH4z';
              $role = 1;
              break;
            case 'regular':
              // $plan = 'plan_FNJeUqPpguW3KS';
              $plan = 'plan_FfxMmtEU9CujO3';
              $role = 2;
              break;
            case 'premium':
              // $plan = 'plan_FNJfdcnNSsNAhK';
              $plan = 'plan_FfxMG7T0MLVCHo';
              $role = 3;
              break;
            default:
              return response()->json(["status" => "failed", "message" => "could not change subscription"], 400);
          }
          //
          $subscription = \Stripe\Subscription::retrieve($subscriptionId);
          \Stripe\Subscription::update($subscriptionId, [
            'cancel_at_period_end' => false,
            'items' => [
              [
                'id' => $subscription->items->data[0]->id,
                'plan' => $plan,
              ],
            ],
          ]);
          // $updateRole = self::changeRole($role, $ownerEmail);
          // if ($updateRole !== false && gettype($updateRole) === "integer") {
            // Cancel the subscription at the end of the term
            // Return the number of roles updated to the user
            return response()->json(["status" => "success", "message" => " modified subscription changed"], 200);
          // } else {
          //   return response()->json(["status" => "failed", "message" => "could not update role. Please contact support at help@klon.io"], 400);
          // }
        //
        } else {
          return response()->json(["status" => "failed", "message" => "subscription does not belong to email address provided"], 400);
        }
      } else {
        return response()->json(["status" => "failed", "message" => "invalid token", 401]);
      }
    } else {
      return response()->json(["status" => "failed", "message" => "auto renew must be of type boolean"], 400);
    }
  }


  /**
   *
   *  Retrieve a Stripe customer object based on the customer_id
   *
   *  @param      String      $customerId      The customer_id for the customer we want to retrieve
   *
   *  @return     Object                       Returns the customer object from Stripe, or false
   *
   */
  private static function getCustomerObject($customerId = false) {
    $customerId = filter_var($customerId, FILTER_SANITIZE_STRING);
    // Make sure the customerId was set
    if ($customerId !== false) {
      // Set the API Key
      \Stripe\Stripe::setApiKey($_ENV['STRIPE_KEY']);
      // Get the customer object
      $customer = \Stripe\Customer::retrieve($customerId);
      // Check if empty
      if (empty($customer)) {
        // The customer email is not found
        return false;
      } else {
        // The customer was found in Stripe, return their ID
        return $customer;
      }
      // Return the customer object
    } else {
      // Please set the $customerId
      return response()->json(["status" => "failed", "message" => "customer id not set"], 400);
    }
  }


  /**
   *
   *  Get subscription objects for a user by email address
   *
   *  @param     String     $email      The customers email address
   *
   *  @return    Array                  Returns an array of subscription objects
   *
   */
  public static function getSubscriptionObj() {
    $post = file_get_contents('php://input');
    // This returns null if not valid json
    $post = json_decode($post, true);
    // Make sure the email is set
    if (isset($post['email']) && !empty($post['email']) && isset($post['token']) && !empty($post['token'])) {
      $email = $post['email'];
      $token = $post['token'];
      // Make sure the email address was set
      if (isset($email) && !empty($email)) {
        // Sanitize the email variable
        $email = filter_var($email, FILTER_SANITIZE_EMAIL);
        // Make sure the token was sent
        if (isset($token) && !empty($token)) {
          // Check if the token is valid
          $token        = filter_var($token, FILTER_SANITIZE_STRING);
          $isTokenValid = Auth::validateToken($token);
          $isTokenValid = json_decode($isTokenValid, true);
          $tokenStatus = $isTokenValid['status'];
          // Token is valid
          if ($tokenStatus == 'success') {
            // Get the token owner
            $owner = Auth::tokenOwner($token);
            $ownerEmail = $owner['email'];
            // Make sure the token owner and the $email value match
            if (strtolower($ownerEmail) === strtolower($email)) {
              // Get all the users subscriptions
              $subscriptionIds = self::getSubscriptionId($email);
              // Make sure that data was returned and not false
              if ($subscriptionIds !== false) {
                // Setup a blank array to store the data we need from Stripe
                $arrSubscriptionObj = array();
                // Setup iterator object
                $i = 0;
                // Get the details of each subscription from Stripe
                foreach($subscriptionIds as $subscriptionId) {
                  // Get the subscription object
                  $subscription = \Stripe\Subscription::retrieve($subscriptionId);
                  // Make sure there is data inside
                  if (!empty($subscription)) {
                    // Get the information we need from the database
                    $arrSubscriptionObj[$i]['id'] = $subscription->id;
                    $arrSubscriptionObj[$i]['price'] = $subscription->items['data'][0]['plan']['amount'];
                    $arrSubscriptionObj[$i]['expires'] = $subscription->current_period_end;
                    $arrSubscriptionObj[$i]['name'] = $subscription->items['data'][0]['plan']['nickname'];
                    $arrSubscriptionObj[$i]['status'] = $subscription->status;
                    $arrSubscriptionObj[$i]['cancelAtPeriodEnd'] = $subscription->cancel_at_period_end;
                  } else {
                    // Return a null object
                    $arrSubscriptionObj = null;
                  }
                  // Increment the iterator
                  $i++;
                }
                return response()->json(["status" => "success", "message" => $arrSubscriptionObj], 200);
              } else {
                return response()->json(["status" => "success", "message" => "no subscriptions found"], 200);
              }
            } else {
              // Email and holder are not the same
              return response()->json(["status" => "fail", "message" => "unauthorized request"], 401);
            }
          } else {
            // Invalid token
            return response()->json(["status" => "fail", "message" => "unauthorized request"], 401);
          }
        } else {
          return response()->json(["status" => "fail", "message" => "unauthorized request"], 401);
        }
      } else {
        // Let the user know that the email address was invalid or empty
        return response()->json(["status" => "fail", "message" => "no email address found"], 400);
      }
    } else {
      return response()->json(["status" => "fail", "message" => "too few arguments"], 400);
    }
  }


  /**
  *
  *  Get a customer's subscription based on their email
  *
  *  @param     String     $email     The customer's email address
  *
  *  @return    Array                 The customer's subscription ID from Stripe
  *
  */
  public static function getSubscriptionId($email) {
    // Get the customer ID based on their email address
    $customerId = self::getStripeCustomer($email);
    // Make sure some data was sent back from the server
    if (isset($customerId) && !empty($customerId)) {
      // Get the customer object
      $customerObj = self::getCustomerObject($customerId);
      // Make sure the customerObj returned some data
      if (isset($customerObj) && !empty($customerObj)) {
        // Get the subscription id(s) for the customer
        $subs = $customerObj->subscriptions;
        // Define an empty array
        $arrSubscription = array();
        // Loop through the contents of the object
        foreach($subs as $subscriptions) {
          // Push each subscription ID onto the array
          array_push($arrSubscription, $subscriptions->id);
        }
        // If no data was stored return false
        if (!empty($arrSubscription)) {
          // Return the array
          return $arrSubscription;
        } else {
          // No data found
          // return response()->json(["status" => "success", "message" => "no subscriptions found"], 200);
          return false;
        }
      } else {
        // Customer Object was empty or not set
        // return response()->json(["status" => "failed", "message" => "no data returned for customer"], 400);
        return false;
      }
    } else {
      // Customer ID is not set or empty
      // return response()->json(["status" => "failed", "message" => "customer id is not set or empty"], 400);
      return false;
    }
  }

}
