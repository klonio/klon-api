<?php

namespace App\Http\Controllers;

use App\Klon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\AuthController as Auth;
use App\Http\Controllers\SecurityController as Security;

class EncryptController extends Controller {

// Add in padding for strings under 16 characters -- just pad with 0x00
  public static function encryptData(Request $request) {
    $data = $request->input('data');
    // Check if the string is at least 15 characters
    $data = str_pad($data, 128, "\0", STR_PAD_RIGHT);
    // $dataLength = strlen($data);
    // if ($dataLength < 16) {
    //   $data = $data .
    // }
    // Set the cipher
    $cipher = "aes-256-xts";
    // Get the key
    $file = parse_ini_file(__DIR__ . '/../../Keys/aes.ini');
    $key = $file['key'];
    // Gets the cipher iv length or FALSE on fail
    $ivlen = openssl_cipher_iv_length($cipher);
    // Generate random data
    $iv = openssl_random_pseudo_bytes($ivlen);
    // Reverse the data
    $data = strrev($data);
    // Encrypt the data
    $ciphertext = openssl_encrypt($data, $cipher, $key, OPENSSL_RAW_DATA, $iv);
    $ciphertext = base64_encode($ciphertext);
    // base64 encode the iv
    $iv = base64_encode($iv);
    // Return cipher & base64_encode(iv)
    return response()->json(['cipher' => $ciphertext, 'iv' => $iv, 'key' => $key, 'data' => $data, 'keytype' => gettype($key)]);
  }


  /**
   *
   *  Decrypt Data
   *
   */
  public static function decryptData() {
    // Pull the data from the database
    $results = DB::select('select * from cipher where id = 36');
    $ciphertext = array_column($results, 'cipher');
    $ciphertext = $ciphertext[0];
    // Get iv
    $iv = array_column($results, 'iv');
    $iv = $iv[0];
    // Set the cipher
    $cipher = "aes-256-xts";
    // Get the key
    $file = parse_ini_file(__DIR__ . '/../../Keys/aes.ini');
    $key = $file['key'];
    // Decrypt
    $originalText = openssl_decrypt(base64_decode($ciphertext), $cipher, $key, OPENSSL_RAW_DATA, base64_decode($iv));
    return response()->json(['cipherText' => $ciphertext, 'iv' => $iv, 'key' => $key, 'originalText' => rtrim(strrev($originalText), "\0")]);
  }


  /**
   *
   *  Encrypt data
   *
   */
  public static function encryptOwnerEntry($data, $owner) {
    // Generate a salt
    $salt = bin2hex(random_bytes(64));
    // Generate a hash from the data and salt
    $key = hash('sha384', $owner . $salt);
    for ($i = 0; $i < 100000; $i++) {
      $key = hash('sha384', $owner . $salt);
    }
    // Set the encryption algorithm to be used
    $method = "AES-128-CBC";
    // Get the length of initialization vector we'll need for this algorithm
    $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
    // Create the initialization vector
    $iv = openssl_random_pseudo_bytes($ivlen);
    // Generate cipher text
    $cipherText = openssl_encrypt($data, $method, $key, $options=OPENSSL_RAW_DATA, $iv);
    // Run through the hmac hashing algorithm
    $hmac = hash_hmac('sha256', $cipherText, $key, $as_binary=true);
    // Base64 Encode the whole thing
    $b64Cipher = base64_encode($iv.$hmac.$cipherText);
    // return the data
    return array("ciphertext" => $b64Cipher, "salt" => $salt);
  }


  /**
   *
   *  Decrypt data
   *
   *  @param    $data     String    This is the data to be decrypted
   *  @param    $owner    String    The owner of this data
   *  @param    $salt     String    Salt used to encrypt the data
   *
   */
  public static function decryptOwnerEntry($data, $owner, $salt) {
    // Decode the data
    $data = base64_decode($data);
    // Setup the key
    $key = hash('sha384', $owner . $salt);
    for ($i = 0; $i < 100000; $i++) {
      $key = hash('sha384', $owner . $salt);
    }
    // Set the encryption method
    $method = "AES-128-CBC";
    // Get the length of the initialization vector for the given method
    $ivlen = openssl_cipher_iv_length($method="AES-128-CBC");
    // Cut out the IV
    $iv = substr($data, 0, $ivlen);
    // Cut out the hash
    $hmac = substr($data, $ivlen, $sha2len=32);
    // Cut out some more stuff
    $ciphertext_raw = substr($data, $ivlen+$sha2len);
    // Decrypt the text
    $original_plaintext = openssl_decrypt($ciphertext_raw, $method, $key, $options=OPENSSL_RAW_DATA, $iv);
    // Check if this is a valid decryption
    $calcmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
    // If the decryption is valid, print it out
    if (hash_equals($hmac, $calcmac)) {
      return $original_plaintext;
    } else {
      return false;
    }
  }


  /**
   *
   *  Get the public key
   *
   */
  public static function getPublicKey() {
    // Add attempt to the database
    $attempt = Security::addAttempt();
    // Check to see if the user is banned
    $isBanned = Security::isBanned();
    if ($isBanned == false || $isBanned == 0) {
      // Display the public key if the user is not banned from requesting
      $pubKey = $_ENV['PUBLICKEY'];
      return response()->json(["status" => "success", "message" => $pubKey], 200);
    } else {
      // Let the user know they're banned
      return response()->json(["status" => "fail", "message" => 'You are banned from accessing the API at this time'], 401);
    }
  }


  /**
   *
   *  Encryption for the email function
   *
   */
  public static function encryptMessageContents($data) {
    try {
      // Get the key
      $key = $_ENV['MAILKEY'];
      $key = hex2bin(self::clarifyKey($key));
      // Generate nonce
      $nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);
      // Encrypt the contents...
      $encrypted = sodium_crypto_secretbox($data, $nonce, $key);
    } catch(Exception $e) {
      return false;
    }
    // Return the encrypted contents
    return base64_encode($nonce) . "." . base64_encode($encrypted);
  }


  /**
   *
   *  Decrypt email messages
   *
   */
  public static function decryptMessageContents($cipher, $nonce) {
    try {
      // Get the key
      $key = $_ENV['MAILKEY'];
      $key = hex2bin(self::clarifyKey($key));
      // Decrypt the contents...
      $decrypted = sodium_crypto_secretbox_open($cipher, $nonce, $key);
    } catch(Exception $e) {
      return false;
    }
    // Return the encrypted contents
    return base64_encode($decrypted);
  }



  /**
   *
   *  This will reverse the obfuscation process
   *
   */
  public static function clarifyKey($key) {
    // Split the string into groups of 4
    $arrKey = str_split($key, 4);
    // Reverse the 4-character long array groups
    for ($i = 0; $i < count($arrKey); $i++) {
      $revKey[$i] = strrev($arrKey[$i]);
    }
    // Implode the string
    $strKey = implode($revKey);
    // Split the key into an array of 2 character groups
    $strKey = str_split($strKey, 2);
    // Convert the hex numbers to octal
    for ($n = 0; $n < count($strKey); $n++) {
      $keyOct[$n] = hexdec($strKey[$n]);
    }
    // Convert octal to decimal
    for ($d = 0; $d < count($keyOct); $d++) {
      $keyDec[$d] = octdec($keyOct[$d]);
    }
    // Convert decimal character code to the characters they represent
    for ($c = 0; $c < count($keyDec); $c++) {
      $clarifiedKey[$c] = chr($keyDec[$c]);
    }
    // Implode the thing
    $result = implode($clarifiedKey);
    return $result;
  }


  public static function retrieveJSONData($req) {
    // Set the encrypted payload from the POST data
    $encData = $req->input('payload');
    // Decrypt the payload
    $data = Auth::decryptPayload($encData);
    // If everything went okay with the decryption phase return the data
    if ($data != false) {
      // Convert the JSON Object into an associative array
      $data = json_decode($data, true);
      // Return the processed data
      return $data;
    } else {
      return false;
    }
  }


}
