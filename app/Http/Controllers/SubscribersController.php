<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subscribers;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\AuthController as Auth;
use App\Http\Controllers\EncryptController as Encrypt;

class SubscribersController extends Controller
{
  // Let the controller know which functions require authentication
  public function __construct() {
    self::middleware('auth',['only' => [
      'isNew'
      ]]);
  }
    /**
     *
     *  Returns a hashed password
     *
     *  @param   $password   String   Password to be hashed
     *
     *  @return  $password   String   Password that was sent to be hashed
     *  @return  $hash       String   Hashed version of the password
     *
     */
    private function hashPassword($password) {
      $options = ['cost' => 12];
      $hash = password_hash($password, PASSWORD_DEFAULT, $options);
      // return response()->json(['password' => $password, 'hash' => $hash]);
      return $hash;
    }


    public function createUser(Request $request) {
      $user = Subscribers::create($request->all());
      return response()->json($user);
    }


    /**
     *
     *  Find out if the user is new or not by checking the is_new field in the table
     *  Send the token and verify if this token holder is new or not
     *
     *  @return         Boolean      True if new False otherwise
     *
     */
    public static function isNew(Request $request) {
      // Make sure the user sent the payload
      if ($request->filled('payload')) {
        // Decrypt the payload
        $data  = Encrypt::retrieveJSONData($request);
        // Get the token from the payload
        $token = $data['token'];
      //
      } else if ($request->filled('token')) {
        $token  = $request->input('token');
      } else {
        return response()->json(["status" => "success", "message" => "missing arguments"], 400);
      }
      // Sanitize the token to be safe
      $token = filter_var($token, FILTER_SANITIZE_STRING);
      // Get the token owners email address
      $owner = Auth::tokenOwner($token);
      // Get the ID number of the subscriber with this email address
      $email = $owner['email'];
      // Query the database
      $query = DB::select("SELECT `is_new` FROM `subscribers` WHERE email=:email AND is_new=1", ['email' => $email]);
      // Make sure there were results returned
      if (gettype($query) === "array") {
        if (count($query) > 0) {
          return response()->json(["status" => "success", "message" => true], 200);
        } else {
          return response()->json(["status" => "success", "message" => false], 200);
        }
      } else {
        return response()->json(["status" => "success", "message" => "wrong type returned"], 400);
      }
    }


    /**
     *
     *  Get the subscribers expiration time
     *
     *  @param      String       $token        The users token is required
     *
     */
     public static function getExpirationTime() {
       // Get the raw POST data
      $post = file_get_contents("php://input");
      // This returns null if not valid json
      $post = json_decode($post, true);
      // Pull the token from the data
      if (isset($post['token'])) {
        // Get the token from the post data
        $token = $post['token'];
        // Sanitize the token string
        $token = filter_var($token, FILTER_SANITIZE_STRING);
         // Check if this is a valid token
         $isTokenValid = Auth::validateToken($token);
         $isTokenValid = json_decode($isTokenValid, true);
         $tokenStatus  = $isTokenValid['status'];
         //
         if ($tokenStatus == 'success') {
           // Get the token owner
           $tokenOwner = Auth::tokenOwner($token);
           $ownerEmail = $tokenOwner['email'];
           $query      = DB::select("SELECT `expires` FROM `subscribers` WHERE `email`=:email", ['email' => $ownerEmail]);
           if (!empty($query)) {
             // Pull the expires out from the object
             $expires  = array_column($query, 'expires');
             if (!empty($expires)) {
               $expires    = $expires[0];
               return response()->json(["status" => "success", "message" => $expires], 200);
             } else {
               //
               return response()->json(["status" => "fail", "message" => "no expiration results"], 400);
             }
           } else {
             //
             return response()->json(["status" => "fail", "message" => "no expiration results"], 400);
           }
         } else {
           //
           return response()->json(["status" => "fail", "message" => "no expiration results"], 400);
         }
       } else {
         return response()->json(["status" => "fail", "message" => "token not found"], 400);
       }
     }
}
