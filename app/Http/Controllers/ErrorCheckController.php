<?php

  namespace App\Http\Controllers;

  use App\Klon;
  use Illuminate\Support\Facades\DB;

  class ErrorCheckController extends Controller {

    /**
     *
     *  Checks a first or last name for formatting errors
     *
     *  @param   $name   String    Name to be checked
     *
     *  @return          Boolean   True if good, otherwise false
     *
     */
    public static function checkName($name) {
      // Make sure the name was sent to the function
      if (isset($name)) {
        // Verify name only contains letters
        if (preg_match('/^[A-Za-z]+$/', $name)) {
          // Longest name in the database currently is 13 characters long
          // SELECT `name` FROM `usa_last_names` WHERE length(`name`) = (SELECT max(length(`name`)) FROM `usa_last_names`)
          if (strlen($name) < 18 && strlen($name) > 1) {
            return true;
          } else {
            return false;
          }
        } else {
          return false;
        }
      } else {
        return false;
      }
    }


    /**
     *
     *  Checks a username for formatting errors
     *
     *  @param   $username   String    Username to be checked
     *
     *  @return              Boolean   True if good, otherwise false
     *
     */
    public static function checkUsername($username) {
      // Make sure the name was sent to the function
      if (isset($username)) {
        // Verify name only contains letters
        if (preg_match('/^[A-Za-z0-9]+$/', $username)) {
          if (strlen($username) < 32 && strlen($username) > 5) {
            return true;
          } else {
            return false;
          }
        } else {
          return false;
        }
      } else {
        return false;
      }
    }

    /**
     *
     *  Checks to see if $mi is a valid middle initial
     *
     *  @param   $mi        String     Checks to see if a single valid character
     *
     *  @return             Boolean    True if address valid false otherwise
     *
     */
    public static function checkMiddleInitial($mi) {
      if (preg_match('/^[A-Z]{1}$/', $mi)) {
        if (strlen(trim(stripslashes($mi))) == 1) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    }


    /**
     *
     *  Checks an address to see if it follows the format "123 Main Street"
     *
     *  @param   $address   Integer    Street Number
     *
     *  @return             Boolean    True if address valid false otherwise
     *
     */
    public static function checkAddress($address) {
      if ( gettype($address) == 'integer') {
        return true;
      } else {
        return false;
      }
    }


    /**
     *
     *  Checks an address to see if it follows the format "123 Main Street"
     *
     *  @param   $road      String     Road name
     *
     *  @return             Boolean    True if road name is valid false otherwise
     *
     */
    public static function checkRoad($road) {
      if (preg_match("/^[a-zA-Z0-9\s-]{1,50}$/", $road)) {
        return true;
      } else {
        return false;
      }
    }

    /**
     *
     *  Check the submitted streetnumber to see if it is valid
     *
     *  @param  $streetNumber   Integer | String | Float    The street number matches the following formats...
     *                                                      Example: 11234, 11234A, 11234.5, 11234 1/2
     *
     */
    public static function checkStreetNumber($streetNumber) {
      // Matches all numbers less than 7 digits
      if (preg_match("/^[0-9]{1,6}+$/", $streetNumber)) {
        return true;
        // Matches 123456.5
      } else if (preg_match("/^[0-9]{1,6}\.[0-9]{1,2}$/", $streetNumber)) {
        return true;
      } else {
        // Matches 135789A
        if (preg_match("/^[0-9]{1,6}[A-Za-z]{1,2}$/", $streetNumber)) {
          return true;
          // Matches 12345 1/2
        } else if(preg_match("/^[0-9]{1,6}\s[1]{1}\/[1-9]{1}$/", $streetNumber)) {
          return true;
        } else {
          return false;
        }
      }
    }


    /**
     *
     *  Checks an email address to see if it is valid
     *
     *  @param   $email   String   Email address to validate
     *
     *  @return           Boolean  True if valid otherwise false
     *
     */
    public static function checkEmail($email) {
      if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $unique = DB::select("SELECT `id` FROM `identities` WHERE `assigned_email`=:email",['email' => $email]);
        if (count($unique) < 1) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    }


    /**
     *
     *  Checks to see if the state code is valid
     *
     */
    public static function checkState($state) {
      if (strlen($state) == 2) {
        $stateArray = array("NY","PR","MA","RI","NH","ME","VT","CT","NJ","PA","DE","DC","VA","MD","WV","NC","SC","GA","FL","AL","TN","MS","KY","OH","IN","MI","IA","WI","MN","SD","ND","MT","IL","MO","KS","NE","LA","AR","OK","TX","CO","WY","ID","UT","AZ","NM","NV","CA","HI","OR","WA","AK");
        $state = strtoupper($state);
        if (in_array($state, $stateArray)) {
          return true;
        }
      } else {
        return false;
      }
    }


    /**
     *
     *  Check to see if the zip code is valid
     *
     */
    public static function checkZip($zip) {
      if (strlen($zip) == 5) {
        return true;
      } else {
        return false;
      }
    }

    /**
     *
     *  Check to see if this is a valid city based on the state and zip provided
     *
     */
    public static function checkCity($city, $state, $zip) {
      $checkState = self::checkState($state);
      if ($checkState == true) {
        $checkZip = self::checkZip($zip);
        if ($checkZip == true) {
          $results = DB::select("SELECT `city` FROM `cities` WHERE `city` = :city AND `state_code` = :state AND `zip` = :zip", ['city' => $city, 'state' => $state, 'zip' => $zip]);
          if (count($results) > 0) {
            return true;
          } else {
            return false;
          }
        } else {
          return false;
        }
      } else {
        return false;
      }
    }

    /**
     *
     *  Checks a DOB to see if it is valid
     *
     *  @param   $dob     Date       Date of birth
     *                    Format:    12-25-1960
     *                    Format:    02-06-1973
     *
     *  @return           Boolean    True if valid otherwise false
     *
     */
    public static function checkDob($dob) {
      if (preg_match("/^(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])-(19[0-9]{2}|20[0-9]{2})$/", $dob, $matches)) {
        if (checkdate($matches[1], $matches[2], $matches[3]) == true) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    }


    /**
     *
     *  Check month
     *
     */
    public static function checkMonth($dobMonth) {
      if (preg_match("/^(0[1-9]|1[0-2])$/", $dobMonth, $matches)) {
        return true;
      } else {
        return false;
      }
    }


    /**
     *
     *  Check day
     *
     */
    public static function checkDay($dobDay) {
      if (preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])$/", $dobDay, $matches)) {
        return true;
      } else {
        return false;
      }
    }


    /**
     *
     *  Check year
     *
     */
    public static function checkYear($dobYear) {
      if (preg_match("/^(19[0-9]{2}|20[0-9]{2})$/", $dobYear, $matches)) {
        return true;
      } else {
        return false;
      }
    }


    /**
     *
     *  Check the password for validation
     *
     *  @param    $password    String     Password generated for the ID
     *
     *  @return                Boolean    True if valid false otherwise
     *
     */
    public static function checkPassword($password) {
      if (strlen($password) < 37 && strlen($password) > 23) {
        if (preg_match("/^[A-z0-9!@#$%^&*_+~]{24,25}$|^[A-z0-9!@#$%^&*_+~]{27,28}$|^[A-z0-9!@#$%^&*_+~]{30}$|^[A-z0-9!@#$%^&*_+~]{32}$|^[A-z0-9!@#$%^&*_+~]{35,36}$/", $password)) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    }


    // Error check all the fields
    public static function errorCheckData($firstName, $lastName, $mInitial, $streetAddress, $state, $zip, $city, $dobDay, $dobMonth, $dobYear, $email, $username, $password) {
      // Error check first and last name
      if(self::checkName($firstName) == true && self::checkName($lastName) == true) {
        // Error check middle initial 1 letter and 1 character length
        if (self::checkMiddleInitial($mInitial) == true) {
          // Error check address
          if (self::checkStreetNumber($streetAddress) == true) {
            // Error check state
            if (self::checkState($state) == true) {
              // Error check zip code
              if (self::checkZip($zip) == true) {
                // Error check the city name
                if (self::checkCity($city, $state, $zip) == true) {
                  // Error check dob day
                  if (self::checkDOB($dobDay) == true) {
                    // Error check dob month
                    if (self::checkDOB($dobMonth) == true) {
                      // Error check dob year
                      if (self::checkDOB($dobYear) == true) {
                        // Error check email
                        if (self::checkEmail($email) == true) {
                          // Error check email
                          if (self::checkUsername($username) == true) {
                            // Error password
                            if (self::checkPassword($password) == true) {
                              return array("status" => true, "message" => "success");
                            } else {
                              return array("status" => false, "message" => "failed password check");
                            }
                          } else {
                            return array("status" => false, "message" => "failed username check");
                          }
                        } else {
                          return array("status" => false, "message" => "failed email check");
                        }
                      } else {
                        return array("status" => false, "message" => "failed dob year check");
                      }
                    } else {
                      return array("status" => false, "message" => "failed dob month check");
                    }
                  } else {
                    return array("status" => false, "message" => "failed dob day check");
                  }
                } else {
                  return array("status" => false, "message" => "failed city check");
                }
              } else {
                return array("status" => false, "message" => "failed zip check");
              }
            } else {
              return array("status" => false, "message" => "failed state check");
            }
          } else {
            return array("status" => false, "message" => "failed address check");
          }
        } else {
          return array("status" => false, "message" => "failed middle initial check");
        }
      } else {
        return array("status" => false, "message" => "failed name check");
      }
      return array("status" => false, "message" => "failed check");
    }


  }
