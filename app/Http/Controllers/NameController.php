<?php

namespace App\Http\Controllers;
require('AddressController.php');
require('BirthdayController.php');

use App\Klon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\AddressController as Address;
use App\Http\Controllers\BirthdayController as DOB;
use App\Http\Controllers\PasswordController as Password;

class NameController extends Controller
{
    /**
     *
     *  Get the first name and gender of the identity
     *
     *  @return  Array   {firstname:$name,gender:$gender}
     *
     */
    private static function getFirstName() {
      // 2556 rows in DB
      $rowCount = 2556;
      // Create a random integer between 1 and the max number of rows in table
      $randomInteger = rand(1,$rowCount);
      // Return the results as a StdClass
      $values = DB::select("SELECT `name`,`gender` FROM `first_names` WHERE id = :randomInteger",['randomInteger' => $randomInteger]);
      // Get results in a returnable format
      foreach ($values as $value) {
        $name = $value->name;
        $gender = $value->gender;
      }
      // Return results as JSON
      return ["firstname" => $name, "gender" => $gender];
    }


    /*
     *
     *  Return a random last name from the table `usa_last_names`
     *
     *  @return   String   "Smith"
     *
     */
    private static function getLastName() {
      $rowCount = 86988;
      $randomInteger = rand(1, $rowCount);
      $values = DB::select("SELECT `name` FROM `usa_last_names` WHERE `id` = :randomInteger",['randomInteger' => $randomInteger]);
      $lname = array_column($values, 'name');
      return $lname[0];
    }


    /**
     *
     *  This is based on research of the most common middle names by gender in the USA from 1940-2009
     *
     *  @param   $gender   Char   "M"|"F" Male of Female gender
     *
     *  @return            JSON   Returns a middle initial
     *
     */
    private static function getMiddleInitial($gender) {
      if (isset($gender)) {
        // If the $gender is Female...
        if ($gender == "F") {
          $female = ['A', 'D', 'E', 'F', 'G', 'J', 'K', 'L', 'M', 'N', 'R', 'S'];
          $initial = $female[rand(0,11)];
          return $initial;
        // If the $gender is Male...
        } else if($gender == "M") {
          $male   = ['A', 'D', 'E', 'J', 'L', 'M', 'R', 'S', 'T', 'W'];
          $initial = $male[rand(0,9)];
          return $initial;
        // If $gender is neither F or M
        } else {
          return response()->json(["status" => "fail", "message" => "gender not set properly"]);
        }
      // If $gender is not set
      } else {
        return response()->json(["status" => "fail", "message" => "gender not set"]);
      }
    }


  /**
   *
   *
   *
   *
   */
  public static function getName() {
    // Get the first name (and gender)
    $nameArray = self::getFirstName();
    $fName = $nameArray['firstname'];
    // First name determines gender
    $gender = $nameArray['gender'];
    // Get the middle initial based on gender
    $mName = self::getMiddleInitial($gender);
    // Generate a last name
    $lName = self::getLastName();
    $name = array(
      'firstName'      =>  $fName,
      'middleInitial'  =>  $mName,
      'lastName'       =>  $lName,
      'gender'         =>  $gender
    );
    return $name;
  }
}
