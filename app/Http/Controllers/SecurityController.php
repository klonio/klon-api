<?php

namespace App\Http\Controllers;

use App\Klon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\AuthController as Auth;

class SecurityController extends Controller {


  /**
   *
   *
   *
   */
  public static function isBanned() {
    $ip = $_SERVER['REMOTE_ADDR'];
    date_default_timezone_set('UTC');
    $epoch = strtotime('now');
    $isBanned = DB::select("SELECT * FROM `security` WHERE `ip`=:ip", ['ip' => $ip]);
    if ($isBanned != false || $isBanned != 0) {
      if (count($isBanned) > 0) {
        // The user could be banned, better dig further
        // Convert the stdClassObject into an Associative Array
        $results = json_decode(json_encode($isBanned), true);
        $results = $results[0];
        // Extract the useful information from the array
        $banId = $results['id'];
        $banisBanned = $results['is_banned'];
        $banExpiration = $results['ban_expiration'];
        // Check to see if the ban expiration has been set
        if ($banExpiration > 0) {
          // Check to see if the ban time has expired
          if ($banExpiration < $epoch) {
            // The ban time has worn off, remove the user from the banlist and allow them access
            DB::update("UPDATE `security` SET `is_banned` = 0, `ban_expiration` = 0, `reason` = '', `attempt` = 1 WHERE `id` = :id", ['id' => $banId]);
            // DB::update("UPDATE `security` SET `is_banned`=0, `ban_expiration`=0, `reason`='', `attempt`=1 WHERE `id`=:id", ['id', $banId]);
          } else {
            // Else... Ban time has not yet passed
            $newExpiration = $banExpiration + 300;
            // Add 5 minutes to the ban time
            DB::update("UPDATE `security` SET `ban_expiration`=:newExpiration, `reason`='Too many requests' WHERE `id`=:id", ['id'=>$id, 'newExpiration'=>$newExpiration]);
            // Return true, meaning the user is banned
            return true;
          }
        } else {
          // Ban time has not been set user is therefore not banned
          return false;
        }
        //

      } else {
        // The user is not banned
        return false;
      }
    } else {
      // Request is messed up just assume they're banned
      return true;
    }
  }


  /**
   *
   *  Add attempt to the `security` table
   *  This will record requests and ban users based on the frequency of their requests
   *
   *
   */
  public static function addAttempt() {

  }


  /**
   *
   *  Brute force protection
   *
   *  @param     $ip             String       IP Address of the offender IPV4 and IPV6 supported
   *  @param     $browser        String       User-Agent of the offender
   *  @param     $timestamp      Timestamp    Timestamp of the alleged offense
   *  @param     $ban            Boolean      To ban or not to ban the user
   *
   *  @return                    JSONObject   Information regarding the actions taken and status
   *
   */
  public static function addLoginAttempt(Request $request) {
    // $ip, $browser, $timestamp, $ban
    date_default_timezone_set('UTC');
    // Get the users IP Address
    $ip = $_SERVER['REMOTE_ADDR'];
    // Get the user's browser
    $browser = stripslashes(trim($_SERVER['HTTP_USER_AGENT']));
    // Get the email address if it is set
    if ($request->has('email')) {
      $email = $request->input('email');
    }
    // Set the timestamp
    $timestamp = strtotime('+5 minutes');
    // See if this person is in the database already
    $isPresent = DB::select("SELECT * FROM `security` WHERE `ip`=:ip AND `browser`=:browser AND `timestamp` < :timestamp",[':timestamp' => $timestamp, ':browser' => $browser, ':ip' => $ip]);
    // If there are records in the database...
    if (count($isPresent) > 0) {
      // Convert the stdClass to an associative array
      $results = json_decode(json_encode($isPresent), true);
      $results = $results[0];
      // Check to see if the timestamp is still within a 5 minute timeframe from now
      if ($results['timestamp'] < $timestamp) {
        // The user is within the bannable time slot
        //-----------------------------//
        // Get the current attempt number
        $attempt = $results['attempt'];
        // Increment the attempts
        $currentAttempt = ++$attempt;
        // If the attempt is greater than or equal to the number of maximum attempts then ban the user
        if ($attempt >= self::$maxAttempts) {
          date_default_timezone_set("UTC");
          $banExpiration = time() + self::$banTime;
          // Update the database to ban the user
          $banHammer = DB::update("UPDATE `security` SET `is_banned`=1, `ban_expiration`=:banTime, `attempt`=:attempt WHERE `ip`=:ip AND `browser`=:browser AND `timestamp` < :timestamp", [':banTime' => $banExpiration, ':attempt' => $currentAttempt, ':timestamp' => $timestamp, ':browser' => $browser, ':ip' => $ip]);
          // At this point the user is banned!
          echo "Ban Hammer Slammed<br>";
        } else {
          $updateAttempts = DB::update('UPDATE `security` SET `attempt` = :attempt, `timestamp` = :timestamp WHERE `ip`=:ip AND `browser`=:browser', [':attempt' => $currentAttempt, ':timestamp' => $timestamp, ':browser' => $browser, ':ip' => $ip]);
          echo "Attempts Updated <br>";
          var_dump($updateAttempts);
        }

      }
      // Reset the timestamp and attempts... Or just delete the log... Hmmm
      else {
        $updateSecurity = DB::update("UPDATE `security` SET `is_banned` = 0, `attempt` = 1, `timestamp` = :timestamp WHERE `ip`=:ip AND `browser`=:browser'", [':timestamp' => $timestamp, ':browser' => $browser, ':ip' => $ip]);
        echo "Security Updated <br>";
        var_dump($updateSecurity);
      }

    }
  }

}
