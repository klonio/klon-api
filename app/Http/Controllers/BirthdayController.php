<?php

namespace App\Http\Controllers;

use App\Klon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BirthdayController extends Controller
{
    /**
     *
     *  Generate the year of birth for our identity
     *
     *  @return  Array  ['year' => 1984]
     *
     */
    private static function genYear() {
      // Minimum year
      $minYear = 1960;
      // Maximum year
      $maxYear = 1993;
      //
      $year = rand($minYear, $maxYear);
      //
      return ['year' => $year];
    }


    /**
     *
     *  Get the month and date for the DOB of our new identity
     *  Sets the day range based on the month selected
     *
     *  @return  Array  ['day' => 18, 'month' => 3]
     *
     */
    private static function genMonthAndDate() {
      // Set the month
      $month = rand(1,12);
      // If it is February just set the range from 1 to 28 days
      if ($month == 2) {
        $day = rand(1, 28);
        return ['day' => $day, 'month' => $month];
      }
      // If the month has 31 days set the 'days' range from 1 to 31
      if ($month == 1 || $month == 3 || $month == 5 || $month == 7 || $month == 8 || $month == 10 || $month == 12) {
        $day = rand(1, 31);
        return ['day' => $day, 'month' => $month];
      }
      // If the month has 30 days set the 'days' range from 1 to 30
      if ($month == 4 || $month == 6 || $month == 9 || $month == 11) {
        $day = rand(1, 30);
        return ['day' => $day, 'month' => $month];
      }
    }

    public static function genDOB() {
      $monthDay = self::genMonthAndDate();
      $year = self::genYear();
      $dob = ['month' => $monthDay['month'], 'day' => $monthDay['day'], 'year' => $year['year']];
      return $dob;
    }
}
