<?php

namespace App\Http\Controllers;

use App\Klon;
use Illuminate\Http\Request;

class PasswordController extends Controller
{

  /**
   *
   *  Generates a password of any length if specified.
   *  If length is not specified default is 24 characters.
   *
   *  @param   $length     Integer   Length of the password to be generated
   *
   *  @return  $password   String    Randomly generated password
   *
   */
  public static function generatePassword($length = 24) {
    $chars = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789~!@#$%^&*_+";
    while(in_array(($length = rand(24,36)), array(26, 29, 31, 33, 34)));
    $password = substr(str_shuffle($chars), 0, $length);
    return $password;
    // return response()->json(['password' => $password]);
  }
}
