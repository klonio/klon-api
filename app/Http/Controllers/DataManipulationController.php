<?php

namespace App\Http\Controllers;

use App\Klon;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ErrorCheckController as ErrorCheck;
use App\Http\Controllers\EncryptController as Encrypt;

class DataManipulationController extends Controller
{

  /**
   *
   *  Convert an ip address into inet_pton compliant string
   *  Allows the ability to store IPv4 and IPv6 in the database
   *
   *
   */
  public static function convertIP($ip = null) {
    if (isset($ip)) {
      // Strip out the netmask, if there is one.
      $cx = strpos($ip, '/');
      if ($cx) {
        $subnet = (int)(substr($ip, $cx+1));
        $ip = substr($ip, 0, $cx);
      }
      // Convert address to packed format
        $addr = inet_pton($ip);
      return $addr;
    } else {
      return false;
    }
  }


  /**
   *
   *  Get all of the factors of a number
   *
   *  @param   Int     Number
   *
   *  @return  Array   The common factors of $num
   *
   */
  private static function getFactors($num) {
    $factors = [];
    // Get factors of the numerator
    for ($x = 1; $x <= $num; $x ++) {
      if ($num % $x == 0) {
        if ($x === 1 || $x === $num) {
          continue;
        }
        $factors[] = $x;
      }
    }
    return $factors;
  }


  /**
   *
   *  Get the largest factors of the number
   *  to act as a column and row to plot the
   *  data on.
   *
   *  @param     Array      List of factors of a number
   *
   *  @return    Int        Example:  1,2
   *
   */
  private static function refactorMe($factors) {
    $count = count($factors);
    if ($count > 2) {
      $rows = ($count / 2);
      $cols = ($rows - 1);
      return $factors[$cols] . "," . $factors[$rows];
    } else {
      $rows = $factors[0];
      $cols = $factors[0];
      return $factors[0] . "," . $factors[0];
    }
  }


  /**
   *
   *  Form the matrix of the data
   *
   *  @param    String           Password or other data
   *  @param    Integer          Number of columns to use for the data
   *  @param    Integer          Number of rows to use for the data
   *
   *  @return   Array|Boolean    Form the array to prepare for data manipulation
   *
   */
  private static function formArray($password, $cols, $rows) {
    $result = array();
    $total = $cols * $rows;
    // Split the string into an array
    $strArray = str_split($password, $cols);
    //
    if ($strArray) {
      return $strArray;
    } else {
      // str_split returned an error
      return false;
    }
  }


  /**
   *
   *  Reverse particular strings within the dataset
   *
   *  @param    Array
   *
   *  @return   String
   *
   */
  private static function reverseString($frmArray) {
    for ($i = 0; $i < count($frmArray); $i++) {
      // Determine if number is even or odd
      if ($i % 2 != 0) {
        $strHolder = $frmArray[$i];
        $frmArray[$i] = strrev($strHolder);
      } else {
        continue;
      }
    }
    return implode("", $frmArray);
  }


  /**
   *
   *  Preformat data for encryption
   *
   *  @param   String    String to be formatted
   *
   *  @return  String    Formatted string
   *
   */
  public static function manipulateData($data) {
    // Factor the length of the data string
    $factors = self::getFactors(strlen($data));
    // Get the middle factors to put as rows/cols
    $reFactor = self::refactorMe($factors);
    // Split the factors into rows/cols
    $splitFactor = explode(",", $reFactor);
    // Form the array into a matrix
    $formArray = self::formArray($data, $splitFactor[0], $splitFactor[1]);
    // Perform the manipulation
    $formattedData = self::reverseString($formArray);
    // Return the formatted data as a string
    return $formattedData;
  }


  /**
   *
   *  Modifying the firstName field
   *
   *  @param    String     firstName     New value to put in the firstName field
   *  @param    String     gender        Value to put in the gender field. Helps to find the firstName in the DB
   *
   *  @return   Boolean                  True upon success False otherwise
   *
   */
  public static function modifyFirstName($firstName, $gender, $id, $url, $owner) {
    // Check to see if this username is in the database
    $firstNameId = DB::select("SELECT `id` FROM `first_names` WHERE `name`=:firstName AND `gender`=:gender", [':firstName' => $firstName, ':gender' => $gender]);
    // If there was a result returned...
    if (count($firstNameId) > 0) {
      // Turn the result into an associative array
      $result = json_decode(json_encode($firstNameId), true);
      $result = $result[0];
      // Get the ID of the firstName found in the database
      $firstNameId = $result['id'];
      // Update the value
      $updateFirstName = DB::update("UPDATE `identities` SET `first_name_id` = :firstNameId WHERE `id`=:id AND `url`=:url AND `subscriber_id`=:ownerId", [':firstNameId' => $firstNameId, ':id' => $id, ':url' => $url, ':ownerId' => $owner]);
      if ($updateFirstName == true) {
        return true;
      } else {
        return false;
      }
    } else {
      // Result was not found in the database
      if (ErrorCheck::checkName($firstName) == true) {
        $addName = DB::insert("INSERT INTO `first_names` (name, gender) VALUES (:name, :gender)", [':name' => $firstName, ':gender' => $gender]);
        // If the name was inserted into the database get the ID and update the identity
        if ($addName == true) {
          $nameID = DB::select("SELECT `id` FROM `first_names` WHERE `name`=:name AND `gender`=:gender", [':name' => $firstName, ':gender' => $gender]);
          if (count($nameID) > 0) {
            $nameID = json_decode(json_encode($nameID), true);
            $nameID = $nameID[0];
            $nameID = $nameID['id'];
            // Update the Database
            $updateFirstName = DB::update("UPDATE `identities` SET `first_name_id`=:firstNameId WHERE `id`=:id AND `url`=:url AND `subscriber_id`=:ownerId", [':firstNameId' => $nameID, ':id' => $id, ':url' => $url, ':ownerId' => $owner]);
            //
            if ($updateFirstName == true) {
              return true;
            }
          } else {
            return false;
          }
        } else {
          return false;
        }
      }
    }
  }


  /**
   *
   *  Modifying the lastName field
   *
   *  @param    String     lastName     New value to put in the lastName field
   *  @param    Integer    id           ID of the identity
   *  @param    String     url          URL that the identity is used on
   *  @param    Integer    owner        ID of the identity owner
   *
   *  @return   Boolean                  True upon success False otherwise
   *
   */
  public static function modifyLastName($lastName, $id, $url, $owner) {
    // Check to see if this username is in the database
    $lastNameId = DB::select("SELECT `id` FROM `usa_last_names` WHERE `name`=:lastName", [':lastName' => $lastName]);
    // If there was a result returned...
    if (count($lastNameId) > 0) {
      // Turn the result into an associative array
      $result = json_decode(json_encode($lastNameId), true);
      $result = $result[0];
      // Get the ID of the lastName found in the database
      $lastNameId = $result['id'];
      // Update the value
      $updateLastName = DB::update("UPDATE `identities` SET `last_name_id`=:lastNameId WHERE `id`=:id AND `url`=:url AND `subscriber_id`=:ownerId", [':lastNameId' => $lastNameId, ':id' => $id, ':url' => $url, ':ownerId' => $owner]);
      // If the records were successfully updated, say so
      if ($updateLastName == true) {
        return true;
      } else {
        return false;
      }
    } else {
      // Result was not found in the database
      if (ErrorCheck::checkName($lastName) == true) {
        $addName = DB::insert("INSERT INTO `usa_last_names` (name) VALUES (:name)", [':name' => $lastName]);
        // If the name was inserted into the database get the ID and update the identity
        if ($addName == true) {
          $nameID = DB::select("SELECT `id` FROM `usa_last_names` WHERE `name`=:name", [':name' => $lastName]);
          if (count($nameID) > 0) {
            $nameID = json_decode(json_encode($nameID), true);
            $nameID = $nameID[0];
            $nameID = $nameID['id'];
            // Update the Database
            $updateLastName = DB::update("UPDATE `identities` SET `last_name_id` = :lastNameId WHERE `id`=:id AND `url`=:url AND `subscriber_id`=:ownerId", [':lastNameId' => $nameID, ':id' => $id, ':url' => $url,  ':ownerId' => $owner]);
            // If the records were successfully updated then say so
            if ($updateLastName == true) {
              return true;
            } else {
              return false;
            }
          } else {
            return false;
          }
        } else {
          return false;
        }
      }
    }
  }


  /**
   *
   *  Modifying the middleInitial field
   *
   *  @param    String     middleInitial     New value to put in the middleInitial field
   *  @param    Integer    id                ID number of the identity
   *  @param    String     url               URL that the identity is used on
   *  @param    Integer    owner             ID number of the identity owner
   *
   *  @return   Boolean                      True upon success False otherwise
   *
   */
  public static function modifyMiddleInitial($middleInitial, $id, $url, $owner) {
    // Remove any junk from the string
    $middleInitial = strtoupper(stripslashes(strip_tags(trim($middleInitial))));
    // Run the string through the proper ErrorCheck function
    $boolMiddleInitialCheck = ErrorCheck::checkMiddleInitial($middleInitial);
    // If the ErrorCheck comes back successfully then update the value in the identity
    if ($boolMiddleInitialCheck == true) {
      // Update the identity
      $updateMiddleInitial = DB::update("UPDATE `identities` SET `middle_initial` = :middleInitial WHERE `id`=:id AND `url`=:url AND `subscriber_id`=:owner", [':middleInitial' => $middleInitial, ':id' => $id, ':url' => $url, ':owner' => $owner]);
      // Returns true if it was successful
      return $updateMiddleInitial;
    } else {
      return false;
    }
  }


  /**
   *
   *  Modifying the city field
   *
   *  @param    String     city         New value to put in the city field
   *  @param    String     state        2 Character state abbreviation
   *  @param    String     zip          5 digit zip code of the city
   *  @param    Integer    id           ID number of the identity
   *  @param    String     url          URL that the identity is used on
   *  @param    Integer    owner        ID of the identity owner
   *
   *  @return   Boolean                 True upon success False otherwise
   *
   */
  public static function modifyCity($city, $state, $zip, $id, $url, $owner) {
    $boolCityCheck = ErrorCheck::checkCity($city, $state, $zip);
    if ($boolCityCheck == true) {
      // Get the ID number of the city they want to update to
      $cityID = DB::select("SELECT `id` FROM `cities` WHERE `city`=:city AND `state_code`=:state AND `zip`=:zip", ['city' => $city, 'state' => $state, 'zip' => $zip]);
      // If the city is found in the database
      if (count($cityID) > 0) {
        $cityID = json_decode(json_encode($cityID), true);
        $cityID = $cityID[0];
        $cityID = $cityID['id'];
        // Update the identity to now refer to the new city id
        $updateCityQuery = DB::update("UPDATE `identities` SET `city_id`=:cityID WHERE `id`=:id AND `url`=:url AND `subscriber_id`=:owner", ['cityID' => $cityID, 'id' => $id, 'url' => $url, 'owner' => $owner]);
        // If the table was successfully updated return true otherwise return false
        if ($updateCityQuery == true) {
          return true;
        } else {
          return false;
        }
      } else {
        // City not found in the database
        return false;
      }
    } else {
      // City not found with the given parameters
      return false;
    }
  }


  /**
   *
   *  Modifying the streetNumber field
   *
   *  @param    Mixed      streetNumber   Accepts an Integer, Double, or String
   *                                      Examples: 5550, 5550.5, 5550A, 5550 1/2
   *  @param    Integer    id             ID number of the identity
   *  @param    String     url            URL that the identity is used on
   *  @param    Integer    owner          ID of the identity owner
   *
   *  @return   Boolean                   True upon success False otherwise
   *
   */
  public static function modifyStreetNumber($streetNumber, $id, $url, $owner) {
    $boolStreetNumberCheck = ErrorCheck::checkStreetNumber($streetNumber);
    if ($boolStreetNumberCheck == true) {
      $updateStreetNumber = DB::update("UPDATE `identities` SET `street_number`=:streetNumber WHERE `id`=:id AND `url`=:url AND `subscriber_id`=:owner", ['streetNumber' => $streetNumber, 'id' => $id, 'url' => $url, 'owner' => $owner]);
      if ($updateStreetNumber == true) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }


  /**
   *
   *  Modifying the dob field
   *
   *  @param    Date       dob            MM-DD-YYYY
   *                                      Examples: 5550, 5550.5, 5550A, 5550 1/2
   *  @param    Integer    id             ID number of the identity
   *  @param    String     url            URL that the identity is used on
   *  @param    Integer    owner          ID of the identity owner
   *
   *  @return   Boolean                   True upon success False otherwise
   *
   */
  public static function modifyDob($dob, $id, $url, $owner) {
    $boolDobCheck = ErrorCheck::checkDob($dob);
    if ($boolDobCheck == true) {
      $updateDob = DB::update("UPDATE `identities` SET `DOB`=:dob WHERE `id`=:id AND `url`=:url AND `subscriber_id`=:owner", ['dob' => $dob, 'id' => $id, 'url' => $url, 'owner' => $owner]);
      if ($updateDob == true) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }


  /**
   *
   *  Modifying the password field
   *
   *  @param    String     password       New password
   *  @param    Integer    id             ID number of the identity
   *  @param    String     url            URL that the identity is used on
   *  @param    Integer    owner          ID of the identity owner
   *
   *  @return   Boolean                   True upon success False otherwise
   *
   */
  public static function modifyPassword($password, $ownerEmail, $id, $url, $owner) {
    $boolPasswordCheck = ErrorCheck::checkPassword($password);
    if ($boolPasswordCheck == true) {
      // Preformat the data
      $password = self::manipulateData($password);
      // Encrypt the password
      $encPassword = Encrypt::encryptOwnerEntry($password, $ownerEmail);
      // Generate a salt
      $salt = $encPassword['salt'];
      // Set the password variable
      $password = $encPassword['ciphertext'];
      $updatePassword = DB::update("UPDATE `identities` SET `password`=:password, `salt`=:salt WHERE `id`=:id AND `url`=:url AND `subscriber_id`=:owner", ['password' => $password, 'salt' => $salt, 'id' => $id, 'url' => $url, 'owner' => $owner]);
      if ($updatePassword == true) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }


  /**
   *
   *  Modifying the road field
   *
   *  @param    String     road           New road
   *  @param    Integer    id             ID number of the identity
   *  @param    String     url            URL that the identity is used on
   *  @param    Integer    owner          ID of the identity owner
   *
   *  @return   Boolean                   True upon success False otherwise
   *
   */
  public static function modifyRoad($road, $id, $url, $owner) {
    $boolRoadCheck = ErrorCheck::checkRoad($road);
    if ($boolRoadCheck == true) {
      $updateRoad = DB::update("UPDATE `identities` SET `road`=:road WHERE `id`=:id AND `url`=:url AND `subscriber_id`=:owner", ['road' => $road, 'id' => $id, 'url' => $url, 'owner' => $owner]);
      if ($updateRoad == true) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }


}
