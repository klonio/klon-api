<?php

namespace App\Http\Controllers;

use App\Mail\VerifySignup;
use Illuminate\Support\Facades\Mail;

class VerifySignupController extends Controller {
  /**
   *
   *  Send an email to the user that registered so they can activate their account
   *
   *  @param     $code     String       hexadecimal code generated during the signup process
   *  @param     $rcpt     String       recipient of the email
   *
   *  @return              Boolean      true|false based on successful operation of the function
   *
   */
  public static function sendVerificationEmail($code, $rcpt) {
    Mail::to($rcpt)->send(new VerifySignup($code));
  }
}
