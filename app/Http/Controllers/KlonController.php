<?php

namespace App\Http\Controllers;

use App\Klon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\AddressController as Address;
use App\Http\Controllers\AuthController as Auth;
use App\Http\Controllers\DataManipulationController as DataManipulation;
use App\Http\Controllers\BirthdayController as DOB;
use App\Http\Controllers\EmailController as Email;
use App\Http\Controllers\EncryptController as Encrypt;
use App\Http\Controllers\ErrorCheckController as ErrorCheck;
use App\Http\Controllers\NameController as Name;
use App\Http\Controllers\PasswordController as Password;

class KlonController extends Controller {
  // Let the controller know which functions require authentication
  public function __construct() {
    self::middleware('auth',['only' => [
      'createCandidateForURL',
      'useCandidateForURL',
      'modifyIdentityForURL',
      'deleteIdentity',
      'getAllIdentities',
      'getHash',
      'getIdentity',
      'getIdentityByProperty'
      ]]);
  }


  /**
   *
   *  Create a candidate Klon ID for a given URL
   *
   *  Method: POST
   *  http://klon.io/api/klon/createCandidateForURL
   *  (
   *
   *  )
   *
   *
   *
   *
   */
  // TODO: Add in error checking
  public function createCandidateForURL(Request $request) {
    if ($request->filled('payload')) {
      // Decrypt the payload
      $data = Encrypt::retrieveJSONData($request);
      // Fetch the name array
      $name = Name::getName();
      // Fetch the location array
      $location = Address::genAddress();
      // Fetch the DOB array
      $dob = DOB::genDOB();
      // Comebine into one array
      $candidate = array_merge($name, $location, $dob);
      // Fetch the password data
      $candidate['password'] = "lipsum";
      $candidate['email'] = Email::genEmail($name['firstName'] . " " . $name['lastName']);
      // Return the Klon candidate
      return $candidate;
    } else {
      return response()->json(["status" => "fail", "message" => "payload not found"], 400);
    }
  }


 /**
  *
  *  Register an email address while selecting a candidate for a url
  *
  *  @param      String       token       JWT Token given at login
  *  @param      String       email       assigned @klonmail address
  *
  *
  *
  */
  // TODO: Work on a clever way of sending and receiving the users assigned email address in the future
  // function registerEmail(Request $request) {
  //   // Check if the encrypted payload was set in the POST data
  //   if ($request->filled('payload')) {
  //     // Decrypt the payload
  //     $data  = Encrypt::retrieveJSONData($request);
  //     if ($data['token'] && $data['email']) {
  //       //
  //     }
  //     $token = $data['token'];
  //     $email = $data['email'];
  //   }
  // }


  /**
   *
   *  Select this candidate to use for the URL
   *
   *  Example:  POST ==> /klon/useCandidateForURL
   *            Key               Value
   *           --------------    --------------------
   *           firstName          Bob
   *           middleInitial      L
   *           lastName           Dole
   *           gender             M
   *           address            3247
   *           road               Main Street
   *           state              NY
   *           city               Buffalo
   *           zip                24316
   *           day                03
   *           month              04
   *           year               1973
   *           email              bdole3256@klonmail.com
   *           username           bdole345
   *           password           #yv*K*5W6u2#86h6pS#WeEvU
   *           token              eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJrbG9uLmlvIiwiZXhwIjoxNTM1Njg1ODExLCJuYmYiOjE1MzU2Nzg2MTAsImlhdCI6MTUzNTY3ODYxMX0.POqI6FNMjg_7YxkEXns8o6CF-p1D64P0gj-p-9Rin-w
   *           url                https://facebook.com/
   *
   *
   *  @param     $firstName     String      First name and gender
   *  @param     $lastName      String      Last name assigned
   *  @param     $mInitial      String      Middle initial assigned
   *  @param     $gender        String      Gender
   *  @param     $streetNumber  Integer     Street Number for the address
   *  @param     $road          String      Road name assigned
   *  @param     $city          String      City data assigned
   *  @param     $day           Date        Day of Birth
   *  @param     $month         Date        Month of Birth
   *  @param     $year          Date        Year of Birth
   *  @param     $mgemail       String      Email address assigned (base64)'d but not encrypted
   *  @param     $email         String      Email address assigned
   *  @param     $username      String      Username assigned
   *  @param     $password      String      Password assigned for the service
   *  @param     $token         JWT         JSON Web Token
   *  @param     $url           URL         URL to edit
   *
   *  @return                   Boolean     True if stored successfully, false otherwise
   *
   */
  public static function useCandidateForURL(Request $request) {
    // Check if the encrypted payload was set in the POST data
    if ($request->filled('payload')) {
      // Decrypt the payload
      $data = Encrypt::retrieveJSONData($request);
      // If all the fields are filled out setup all the variables
      $firstName     = $data['firstName'];
      $mInitial      = $data['middleInitial'];
      $lastName      = $data['lastName'];
      $gender        = $data['gender'];
      $streetAddress = $data['streetAddress'];
      $state         = $data['state'];
      $city          = $data['city'];
      $zip           = $data['zip'];
      $dobDay        = $data['day'];
      $dobMonth      = $data['month'];
      $dobYear       = $data['year'];
      // $mgemail       = $data['mgemail'];
      $rEmail        = base64_decode($data['rEmail']);
      $email         = $data['email'];
      $username      = $data['username'];
      $password      = $data['password'];
      $token         = $data['token'];
      $url           = $data['url'];
    } else {
      return response()->json(["status" => "fail", "message" => "payload not found"], 400);
    }
    // Check all of the user input details for errors
    // $errorCheck = ErrorCheck::errorCheckData($firstName, $lastName, $mInitial, $streetAddress, $state, $zip, $city, $dobDay, $dobMonth, $dobYear, $email, $username, $password);
    // If no errors were detected
    $errorCheck['status'] = true;
    if ($errorCheck['status'] == true) {
      // Check to see who owns the token provided
      $owner = Auth::tokenOwner($token);
      // Get the ID number of the subscriber with this email address
      $idOwner = $owner['id'];
      $ownerEmail = $owner['email'];
      if ($owner != false) {
        // Assign email address to user
        $assignEmail = Email::registerEmailAddress($rEmail, $idOwner);
        // $assignEmail = Mail::addForwardingAddress(base64_decode($mgemail), $owner);
        // If addForwardingAddress is successful...
        if ($assignEmail != false) {
          // Setup salt
          $iv = bin2hex(random_bytes(64));
          // Setup the creation time
          date_default_timezone_set('UTC');
          $created_at = strtotime('now');
          $updated_at = $created_at;
          //
          // Preformat data before encryption
          // $password = DataManipulation::manipulateData($password);
          // Encrypt the password provided
          $password = Encrypt::encryptOwnerEntry($password, $ownerEmail);
          $salt = $password['salt'];
          $password = $password['ciphertext'];
          //
          // TODO: Check to see if the email is already registered to anyone else on klon
          $storeID = DB::insert("INSERT INTO `identities` (subscriber_id, first_name, last_name, gender, street_address, city, state, zip, middle_initial, password, salt, assigned_email, route_id, username, dob_day, dob_month, dob_year, url, created_at, updated_at) VALUES(:owner, :firstName, :lastName, :gender, :streetAddress, :city, :state, :zip, :mInitial, :password, :salt, :email, :routeId, :username, :dobDay, :dobMonth, :dobYear, :url, :created_at, :updated_at)", ['owner' => $idOwner, 'firstName' => $firstName, 'lastName' => $lastName, 'gender' => $gender, 'streetAddress' => $streetAddress, 'city' => $city, 'state' => $state, 'zip' => $zip, 'mInitial' => $mInitial, 'password' => $password, 'salt' => $salt, 'email' => $email, 'routeId' => $assignEmail, 'username' => $username, 'dobDay' => $dobDay, 'dobMonth' => $dobMonth, 'dobYear' => $dobYear, 'url' => $url, 'created_at' => $created_at, 'updated_at' => $updated_at]);
          // print_r($storeID);
          return response()->json(["status" => "success", "message" => "candidate stored for this url"], 200);
        } else {
          return response()->json(["status" => "fail", "message" => "unable to assign KlonMail"], 400);
        }
      } else {
        // Not the correct owner of the token
        return response()->json(["status" => "fail", "message" => "invalid token/owner pair"], 401);
      }
    } else {
      return response()->json(["status" => "fail", "message" => "Error: " . $errorCheck['message']], 400);
    }
  }

  /**
   *
   *  Takes a function from camelCase and turns it into camel_case
   *
   */
  public static function fromCamelCase($input) {
    preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
    $ret = $matches[0];
    foreach ($ret as &$match) {
      $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
    }
    return implode('_', $ret);
  }


  /**
   *
   *  Modify the details of an identity
   *
   *  @method       PUT       ==>             /klon/modifyIdentityForURL
   *
   *  @param    Integer       $identity       The identity id
   *  @param    String        $url            URL the identity is used on
   *  @param    String        $token          JWT returned at login
   *  @param    String        $field(s)       Fields to modify the value for
   *  @param    String        $value(s)       New value(s) for the given field(s)
   *
   *  @return   JSONObject                    Returns a JSON Object with successful status
   *
   */
  public static function modifyIdentity(Request $request) {
    // Check if the encrypted payload was set in the POST data
    if ($request->filled('payload')) {
      // Decrypt the payload
      $data = Encrypt::retrieveJSONData($request);
      // Token of this feller
      $token = $data['token'];
      // Field or fields to be modified
      $mods = $data['mods'];
      $email = $data['email'];
      // print_r($mods);
      // Value of values to place in the field
      // $value = $data['value'];
      // Determine if this token holder owns the identity
      $ownerId = Auth::tokenOwner($token);
      if (isset($ownerId['email']) && isset($ownerId['id'])) {
        // Get the owner id of the identity
        $ownerEmail = $ownerId['email'];
        $ownerId = $ownerId['id'];
        // Establish what fields are modifiable
        $modifiableFields = array('firstName', 'middleInitial', 'lastName', 'gender', 'city', 'state', 'zip', 'streetAddress', 'username', 'password', 'dobMonth', 'dobDay', 'dobYear');
        // Loop through the array of fields / values
        foreach ($mods as $key => $value) {
          // If the field is in the array of modifiable fields, then play ball
          if (in_array($key, $modifiableFields)) {
            $key = self::fromCamelCase($key);
            // Make sure the thing is less than 128 characters otherwise it won't fit in the database properly
            if (strlen($value) < 129) {
              // echo "Key: " . $key;
              // echo "<br><br>";
              // echo "Value: " . $value;
              // Update the database
              $updateDB = DB::update('UPDATE `identities` SET ' . $key . '=:value WHERE subscriber_id=:tokenId AND assigned_email=:email', ['value' => $value, 'tokenId' => $ownerId, 'email' => $email]);
            } else {
              // Field is not modifiable
              return response()->json(["status" => "fail", "message" => "new change exceeds size limit"], 400);
            }
          } else {
            // Field is not modifiable
            return response()->json(["status" => "fail", "message" => "invalid field"], 400);
          }
        }
        // var_dump($updateDB);
        $query = DB::select("SELECT `id`, `first_name`, `middle_initial`, `last_name`, `street_address`, `gender`, `city`, `state`, `zip`, `username`, `password`, `salt`, `assigned_email`, `dob_month`, `dob_day`, `dob_year`, `url` FROM `identities` WHERE assigned_email=:email AND subscriber_id=:tokenId", ['email' => $email, 'tokenId' => $ownerId]);
        if (gettype($query) === "array") {
          if (count($query) > 0) {
            $idId = array_column($query, 'id');
            $idId = $idId[0];
            //
            $idFirstName = array_column($query, 'first_name');
            $idFirstName = $idFirstName[0];
            //
            $idMiddleInitial = array_column($query, 'middle_initial');
            $idMiddleInitial = $idMiddleInitial[0];
            //
            $idLastName = array_column($query, 'last_name');
            $idLastName = $idLastName[0];
            //
            $idStreetAddress = array_column($query, 'street_address');
            $idStreetAddress = $idStreetAddress[0];
            //
            $idGender = array_column($query, 'gender');
            $idGender = $idGender[0];
            //
            $idCity = array_column($query, 'city');
            $idCity = $idCity[0];
            //
            $idState = array_column($query, 'state');
            $idState = $idState[0];
            //
            $idZip = array_column($query, 'zip');
            $idZip = $idZip[0];
            //
            $idUsername = array_column($query, 'username');
            $idUsername = $idUsername[0];
            //
            $idSalt = array_column($query, 'salt');
            $idSalt = $idSalt[0];
            //
            $idPassword = array_column($query, 'password');
            $idPassword = $idPassword[0];
            $idPassword = Encrypt::decryptOwnerEntry($idPassword, $ownerEmail, $idSalt);
            //
            $idAssignedEmail = array_column($query, 'assigned_email');
            $idAssignedEmail = $idAssignedEmail[0];
            //
            $idDobMonth = array_column($query, 'dob_month');
            $idDobMonth = $idDobMonth[0];
            //
            $idDobDay = array_column($query, 'dob_day');
            $idDobDay = $idDobDay[0];
            //
            $idDobYear = array_column($query, 'dob_year');
            $idDobYear = $idDobYear[0];
            //
            $idUrl = array_column($query, 'url');
            $idUrl = $idUrl[0];
            // Return the Identity
            $id = array(
              'id'            => $idId,
              'firstName'     => $idFirstName,
              'lastName'      => $idLastName,
              'middleInitial' => $idMiddleInitial,
              'streetAddress' => $idStreetAddress,
              'gender'        => $idGender,
              'city'          => $idCity,
              'state'         => $idState,
              'zip'           => $idZip,
              'username'      => $idUsername,
              'password'      => $idPassword,
              'salt'          => $idSalt,
              'assignedEmail' => $idAssignedEmail,
              'dobMonth'      => $idDobMonth,
              'dobDay'        => $idDobDay,
              'dobYear'       => $idDobYear,
              'url'           => $idUrl
            );
            $id = json_encode($id, JSON_UNESCAPED_SLASHES);
            return response()->json(["status" => "success", "message" => "identity updated", "data" => $id], 200);
          } else {
            // Empty results returned
            return response()->json(["status" => "fail", "message" => "identity not found"], 400);
          }
        } else {
          return response()->json(["status" => "fail", "message" => "wrong type returned"], 400);
        }
      } else {
        // There was an error determining token owner
        return response()->json(["status" => "fail", "message" => "invalid token or syntax"], 400);
      }
    } else {
      return response()->json(["status" => "fail", "message" => "payload not found"], 400);
    }
    // If value can't be found in the database add it and reference the id? Maybe a security vulnerability
  }


  /**
   *
   *  Delete a particular identity based on the identity id and token holder's ID
   *
   *  @method  DELETE    ==>          /klon/deleteIdentity
   *
   *  @param   Integer   $identity    Identity ID
   *  @param   String    $token       JWT Token returned at login
   *
   *  @return  Boolean                Returns true if successful otherwise false
   *
   */
  public static function deleteIdentity(Request $request) {
    // Check if the encrypted payload was set in the POST data
    if ($request->filled('payload')) {
      // Decrypt the payload
      $data = Encrypt::retrieveJSONData($request);
      // ID of the identity
      $identity = $data['identityId'];
      $identity = filter_var($identity, FILTER_SANITIZE_NUMBER_INT);
      // Token for the identity
      $token = $data['token'];
      $token = filter_var($token, FILTER_SANITIZE_STRING);
      // Hash of the identity for reference in the email table
      $hash = $data['hash'];
      $hash = filter_var($hash, FILTER_SANITIZE_STRING);
      // Validate the hash
      if ((bool)preg_match('/^[0-9a-f]{40}$/i', $hash)) {
        // Get the token owner id
        $ownerId = Auth::tokenOwner($token);
        $ownerId = $ownerId['id'];
        // Delete the email address from `email_address`
        $delEmail = DB::delete("DELETE FROM `email_addresses` WHERE `hash`=:hash AND `owner_id`=:owner", ['hash' => $hash, 'owner' => $ownerId]);
        // Delete the identity
        $delete = DB::delete("DELETE FROM `identities` WHERE `id`=:id AND `subscriber_id`=:owner", ['id' => $identity, 'owner' => $ownerId]);
        // If more than 0 rows were affected return true otherwise return false
        if ($delete > 0) {
          return response()->json(["status" => "success", "message" => "identity deleted"], 200);
        } else {
          return response()->json(["status" => "fail", "message" => "identity not found" . $delete], 400);
        }
      } else {
        return response()->json(["status" => "fail", "message" => "invalid hash provided"], 400);
      }
    } else {
      return response()->json(["status" => "fail", "message" => "payload not found"], 400);
    }
  }


  /**
   *
   *  Get all of the identities for a user
   *
   *  @method  POST      ==>          /klon/getIdentities
   *
   *  @param   Integer   $identity    Identity ID
   *  @param   String    $token       JWT Token returned at login
   *
   *  @return  Boolean                Returns true if successful otherwise false
   *
   */
  public static function getAllIdentities(Request $request) {
    // Check if the encrypted payload was set in the POST data
    if ($request->filled('payload')) {
      // Decrypt the payload
      $data = Encrypt::retrieveJSONData($request);
      if ($data['token']) {
        // Authentication token
        $token = $data['token'];
        $token = filter_var($token, FILTER_SANITIZE_STRING);
        // Get the token owner id
        $ownerId = Auth::tokenOwner($token);
        if ($data['hash']) {
          // Get the identity timestamp sent to the server and compare to the currently stored hash
          $receivedHash = $data['hash'];
          $receivedHash = filter_var($receivedHash);
          $storedHash   = DB::select("SELECT `hash` FROM `subscribers` WHERE `id` = :id", ['id' => $ownerId['id']]);
          $storedHash   = array_column($storedHash, "hash");
          $storedHash   = $storedHash[0];
          // If the hash is the same as the one received then the user already has the most up-to-date version of their identities
          if ($storedHash == $receivedHash) {
            return response()->json(["status" => "success", "data" => "up-to-date"], 200);
          } else {
            // Update the database
            // DB::update("UPDATE `subscribers` SET `hash`=:hash WHERE `id`=:id",["hash" => $idHash, "id" => $ownerId['id']]);
            // Get all of the identities in the database for this user
            $results = DB::select("SELECT `id`, `first_name`, `middle_initial`, `last_name`, `street_address`, `gender`, `city`, `state`, `zip`, `username`, `password`, `salt`, `assigned_email`, `dob_month`, `dob_day`, `dob_year`, `url` FROM `identities` WHERE `subscriber_id`=:id", [':id' => $ownerId['id']]);
            // If more than 0 rows were affected return true otherwise return false
            $data = "";
            if (count($results) > 0) {
              $columns = "id, firstName, middleInitial, lastName, gender, streetAddress, city, state, zip, username, password, salt, assignedEmail, dobMonth, dobDay, dobYear, url";
              // Put the data into a cute little JSON object
              foreach ($results as $result) {
                $data    = $data . "|" . $result->id . "," . $result->first_name . "," . $result->middle_initial . "," . $result->last_name . "," . $result->gender . "," . $result->street_address . "," . $result->city . "," . $result->state . "," . $result->zip . "," . $result->username . "," . Encrypt::decryptOwnerEntry($result->password, $ownerId['email'], $result->salt) . "," . $result->salt . "," . $result->assigned_email . "," . $result->dob_month . "," . $result->dob_day . "," . $result->dob_year . "," . $result->url;
              }
              // Base 64 encode the data for response
              $ids = base64_encode($columns . $data);
              // Return the Identities
              return response()->json(["status" => "success", "data" => $ids], 200);
              // // Hash this with SHA1
              // $idHash = hash('sha1', $ids);
              // Return the identities
            } else {
              return response()->json(["status" => "success", "message" => "no identities found"], 200);
            }
          }
        } else {
          return response()->json(["status" => "fail", "message" => "hash missing from request."], 401);
        }
      } else {
        return response()->json(["status" => "fail", "message" => "token missing from request"], 401);
      }
    } else {
      return response()->json(["status" => "fail", "message" => "payload not found"], 400);
    }
  }


  /**
   *
   *  Update the users hash for their identities
   *
   *  @method  PUT   ==>  /klon/updateHash
   *
   *  @param   String   $token     JWT Token returned at login
   *  @param   String   $hash      New value of the hash
   *
   *  @return  Object              JSON {"status" : success|fail, "message" : "hash updated to $hash" | "hash could not be updated"}
   *
   */
  public static function updateHash(Request $request) {
    // Decrypt the payload
    $data = Encrypt::retrieveJSONData($request);
    if ($data['token'] && $data['hash']) {
      // Get the token
      $token = $data['token'];
      // Sanitize the token
      $token = filter_var($token, FILTER_SANITIZE_STRING);
      // Get the token owner id
      $ownerId = Auth::tokenOwner($token);
      $ownerId = $ownerId['id'];
      // Get the hash
      $hash  = $data['hash'];
      // Sanitize the hash
      $hash  = filter_var($hash, FILTER_SANITIZE_STRING);
      // Make sure the hash matches the regular expression
      if ((bool)preg_match('/^[0-9a-f]{40}$/i', $hash)) {
        // Update the thing
        $update = DB::update("UPDATE `subscribers` SET `hash`=:hash WHERE `id`=:id", ['hash' => $hash, 'id' => $ownerId]);
        // Update returns the number of rows affected by the changes
        if ($update > 0) {
          return response()->json(["status" => "success", "message" => "updated hash"], 200);
        } else {
          return response()->json(["status" => "fail", "message" => "could not update hash"], 400);
        }
      } else {
        // Return error invalid hash
        return response()->json(["status" => "fail", "message" => "invalid hash"], 400);
      }
    } else {
      // Return error invalid hash
      return response()->json(["status" => "fail", "message" => "bad request"], 400);
    }
  }


  /**
   *
   *  Get the users hash for their identities
   *
   *  @method  POST   ==>  /klon/getHash
   *
   *  @param   String   $token     JWT Token returned at login
   *
   *  @return  Object              JSON {"status" : success|fail, "message" : "$hash" || "hash could not be updated"}
   *
   */
  public static function getHash(Request $request) {
    // Decrypt the payload
    $data = Encrypt::retrieveJSONData($request);
    if ($data['token']) {
      // Get the token
      $token = $data['token'];
      $token = filter_var($token, FILTER_SANITIZE_STRING);
      // Get the token owner id
      $ownerId = Auth::tokenOwner($token);
      $ownerId = $ownerId['id'];
      // Update the thing
      $query = DB::select("SELECT `hash` FROM `subscribers` WHERE `id`=:id", ['id' => $ownerId]);
      $hash = array_column($query, 'hash');
      if (!empty($hash)) {
        $hash = $hash[0];
        // Update returns the number of rows affected by the changes
        return response()->json(["status" => "success", "message" => $hash], 200);
      } else {
        return response()->json(["status" => "fail", "message" => "could not retrieve hash"], 400);
      }
    } else {
      // Return error invalid hash
      return response()->json(["status" => "fail", "message" => "bad request"], 400);
    }
  }


  /**
   *
   *  Retrieves a particular identity based on the identity id and token holder's ID
   *
   *  @method  POST      ==>          /klon/getIdentities
   *
   *  @param   Integer   $identity    Identity ID
   *  @param   String    $token       JWT Token returned at login
   *
   *  @return  Object                Returns the identity if successful otherwise false
   *
   */
  public static function getIdentity(Request $request) {
    // Decrypt the payload
    $data = Encrypt::retrieveJSONData($request);
    if ($data['token'] && $data['id']) {
      // Authentication token
      $token = $data['token'];
      $token = filter_var($token, FILTER_SANITIZE_STRING);
      // Get the token owner id
      $ownerId = Auth::tokenOwner($token);
      // Get the identity ID
      $id = $data['id'];
      $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
      // Get all of the identities in the database for this user where the id is set to $id
      $results = DB::select("SELECT `id`, `first_name`, `middle_initial`, `last_name`, `street_address`, `gender`, `city`, `state`, `zip`, `username`, `password`, `salt`, `assigned_email`, `dob_month`, `dob_day`, `dob_year`, `url` FROM `identities` WHERE `subscriber_id`=:subid AND `id`=:id", ['subid' => $ownerId['id'], 'id' => $id]);
      $data = "";
      // If more than 0 rows were affected return the data
      if (count($results) > 0) {
        // Put the data into a cute little JSON object
        foreach ($results as $result) {
          $columns = "id, firstName, middleInitial, lastName, gender, streetAddress, city, state, zip, username, password, salt, assignedEmail, dobMonth, dobDay, dobYear, url";
          $data    = $data . "|" . $result->id . "," . $result->first_name . "," . $result->middle_initial . "," . $result->last_name . "," . $result->gender . "," . $result->street_address . "," . $result->city . "," . $result->state . "," . $result->zip . "," . $result->username . "," . Encrypt::decryptOwnerEntry($result->password, $ownerId['email'], $result->salt) . "," . $result->salt . "," . $result->assigned_email . "," . $result->dob_month . "," . $result->dob_day . "," . $result->dob_year . "," . $result->url;
        }
        // Base 64 encode the data for response
        $ids = base64_encode($columns . $data);
        // Return the Identities
        return response()->json(["status" => "success", "data" => $ids], 200);
      } else {
        return response()->json(["status" => "success", "message" => "identity not found"], 200);
      }
    } else {
      return response()->json(["status" => "fail", "message" => "invalid request"], 401);
    }
  }


  /**
   *
   *  Get an identity based on the properties sent
   *
   *  @param      String      token             The users authentication token
   *  @param      String      firstName         firstName of the identity
   *  @param      String      lastName          lastName of the identity
   *  @param      String      assignedEmail     assignedEmail of the identity
   *  @param      String      streetAddress     streetAddress of the identity
   *
   *  @return     Object                        Return the identity, or false in an Object
   *
   */
  public static function getIdentityByProperty(Request $request) {
    // Make sure we have all the required data
    if ($request->filled('token') && $request->filled('firstName') && $request->filled('lastName') && $request->filled('assignedEmail') && $request->filled('streetAddress')) {
      // Sanitize the data
      $token         = filter_var($request->input('token'), FILTER_SANITIZE_STRING);
      $firstName     = filter_var($request->input('firstName'), FILTER_SANITIZE_STRING);
      $lastName      = filter_var($request->input('lastName'), FILTER_SANITIZE_STRING);
      $assignedEmail = filter_var($request->input('assignedEmail'), FILTER_SANITIZE_STRING);
      $streetAddress = filter_var($request->input('streetAddress'), FILTER_SANITIZE_STRING);
      // Check to see who owns the token provided
      $owner = Auth::tokenOwner($token);
      // Get the ID number of the subscriber with this email address
      $subscriberId = $owner['id'];
      // Query the database
      $query = DB::select("SELECT `id`, `first_name`, `middle_initial`, `last_name`, `street_address`, `gender`, `city`, `state`, `zip`, `username`, `password`, `salt`, `assigned_email`, `dob_month`, `dob_day`, `dob_year`, `url` FROM `identities` WHERE `first_name`=:firstName AND `last_name`=:lastName AND `assigned_email`=:assignedEmail AND `street_address`=:streetAddress AND `subscriber_id`=:subscriberId", ["firstName" => $firstName, "lastName" => $lastName, "assignedEmail" => $assignedEmail, "streetAddress" => $streetAddress, "subscriberId" => $subscriberId]);
      // Check to see if there were results found
      if (gettype($query) === "array") {
        if (count($query) > 0) {
          // If more than 0 rows were affected return the data
          $idId = array_column($query, 'id');
          $idId = $idId[0];
          //
          $idFirstName = array_column($query, 'first_name');
          $idFirstName = $idFirstName[0];
          //
          $idMiddleInitial = array_column($query, 'middle_initial');
          $idMiddleInitial = $idMiddleInitial[0];
          //
          $idLastName = array_column($query, 'last_name');
          $idLastName = $idLastName[0];
          //
          $idStreetAddress = array_column($query, 'street_address');
          $idStreetAddress = $idStreetAddress[0];
          //
          $idGender = array_column($query, 'gender');
          $idGender = $idGender[0];
          //
          $idCity = array_column($query, 'city');
          $idCity = $idCity[0];
          //
          $idState = array_column($query, 'state');
          $idState = $idState[0];
          //
          $idZip = array_column($query, 'zip');
          $idZip = $idZip[0];
          //
          $idUsername = array_column($query, 'username');
          $idUsername = $idUsername[0];
          //
          $idSalt = array_column($query, 'salt');
          $idSalt = $idSalt[0];
          //
          $idPassword = array_column($query, 'password');
          $idPassword = $idPassword[0];
          $idPassword = Encrypt::decryptOwnerEntry($idPassword, $owner['email'], $idSalt);
          //
          $idAssignedEmail = array_column($query, 'assigned_email');
          $idAssignedEmail = $idAssignedEmail[0];
          //
          $idDobMonth = array_column($query, 'dob_month');
          $idDobMonth = $idDobMonth[0];
          //
          $idDobDay = array_column($query, 'dob_day');
          $idDobDay = $idDobDay[0];
          //
          $idDobYear = array_column($query, 'dob_year');
          $idDobYear = $idDobYear[0];
          //
          $idUrl = array_column($query, 'url');
          $idUrl = $idUrl[0];
          // Return the Identity
          $id = array(
            'id'            => $idId,
            'firstName'     => $idFirstName,
            'lastName'      => $idLastName,
            'middleInitial' => $idMiddleInitial,
            'streetAddress' => $idStreetAddress,
            'gender'        => $idGender,
            'city'          => $idCity,
            'state'         => $idState,
            'zip'           => $idZip,
            'username'      => $idUsername,
            'password'      => $idPassword,
            'salt'          => $idSalt,
            'assignedEmail' => $idAssignedEmail,
            'dobMonth'      => $idDobMonth,
            'dobDay'        => $idDobDay,
            'dobYear'       => $idDobYear,
            'url'           => $idUrl
          );
          $id = json_encode($id, JSON_UNESCAPED_SLASHES);
          return response()->json(["status" => "success", "data" => $id], 200);
        } else {
          return response()->json(["status" => "success", "message" => "identity not found", "data" => $id], 200);
        }
      } else {
        return response()->json(["status" => "failed", "message" => "identity not found"], 400);
      }
    } else {
      return response()->json(["status" => "failed", "message" => "missing arguments"], 400);
    }
  }


}
