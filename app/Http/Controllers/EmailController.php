<?php

namespace App\Http\Controllers;

use App\Klon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\EmailController as Email;

class EmailController extends Controller {
  /**
   *
   *  Generates an email address based on the $name supplied
   *
   *  @param   $name   String    The full name of the user to whom the address will belong
   *
   *  @return          String    Email address @klonmail.com
   *
   */
  public static function genEmail($name = null) {
    if (isset($name)) {
      // Get the first letter of the last name
      $spacePosition = strpos($name, " ", 0);
      $nameLength = mb_strlen($name, 'utf-8');
      if ($spacePosition != false) {
        // First name initial
        $firstInitial = mb_substr($name, 0, 1, 'utf-8');
        // Last name initial
        $lastName = mb_substr($name, (int)++$spacePosition, $nameLength);
        // Randomly place the underscore to increase randomness
        $underscore = rand(0,2);
        switch ($underscore) {
          // No underscore
          case 0:
          break;
          // Underscore after first initial
          case 1:
            $firstInitial = $firstInitial . "_";
          break;
          // Underscore after the last name
          case 2:
            $lastName = $lastName . "_";
          break;
          default:
          break;
        }
        // Set a loop counter
        $loop = 1;
        // Create an email account for the user until the email account is one that is not used already, or we make 10 attempts
        do {
          // Assemble the email and make it all lowercase
          $email = strtolower($firstInitial . $lastName . rand(1,99999) . "@klonmail.com");
          // Check to make sure this email address is not already registered
          $exist = DB::select("SELECT `id` FROM `email_addresses` WHERE `email`=:email", [":email" => $email]);
          // Increment the loop
          $loop++;
        } while (count($exist) > 0 && $loop < 10);
      } else {
        $error = 'Error';
        throw new Exception($error);
      }
      return $email;
    } else {
      // Return an error
      return "Error";
    }
  }


  /**
   *
   *  This function will register the klon email address in the database for the user
   *
   *  @param    $email       String      Email address to register
   *  @param    $ownerId     Integer     Owners ID
   *
   *  @return                Boolean     True if successful, False otherwise
   *
   */
  public static function registerEmailAddress($email = null, $ownerId = null) {
    // Make sure the email address is filled out and valid
    if ($email !== null && filter_var($email, FILTER_VALIDATE_EMAIL) !== false) {
      // Check to see if this email address is already registered
      try {
        $isRegistered = self::isEmailRegistered($email);
        if ($isRegistered === false) {
          // It is safe to register this email address for the user
          $query = DB::insert("INSERT INTO `email_addresses` (owner_id, email) VALUES(:ownerId, :email)", ['ownerId' => $ownerId, 'email' => $email]);
          if ($query === true || $query === 1) {
            // Success
            return true;
          } else {
            // Failure
            return false;
          }
        } else if ($isRegistered === true) {
          return false;
        }
      } catch(Exception $e) {
        return $e->getMessage();
      }
    } else {
      return false;
    }
  }


  /**
   *
   *  Check to see if this email address is already registered in the database
   *
   *  @param      $email      String      Email address to check for in the database
   *
   *  @return                 Boolean     True if registered, False if not
   *
   */
  public static function isEmailRegistered($email = null) {
    // Make sure the email address is filled out and valid
    if ($email !== null && filter_var($email, FILTER_VALIDATE_EMAIL) !== false) {
      // Query the table for the current email address
      $query = DB::select("SELECT `email` FROM `email_addresses` WHERE `email`=:email", ["email" => $email]);
      // If there are any results in the thing then return true
      if (count($query) > 0) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

}
