<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;

class VerifySignup extends Mailable
{
  protected $code;

  public function __construct($code) {
    $this->code = $code;
  }
  /**
   * Build the message.
   *
   * @return $this
   */
  public function build() {
    return $this->view('VerifySignupEmail',["code" => $this->code]);
  }
}
