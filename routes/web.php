<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
// $router->group(['prefix' => 'mail'], function () use ($router) {
//   $router->get('verify', 'VerifySignupController@sendVerificationEmail');
// });

// Test Endpoint
// $router->group(['prefix' => 'test'], function () use ($router) {
  // $router->get('/isSubscriber/{email}', 'AuthController@isSubscriber');
// });
// Stripe Endpoint
$router->group(['prefix' => 'stripe'], function () use ($router) {
  $router->post('/endpoint', 'StripeController@stripeEndpoint');
});

// Stripe Routes
$router->group(['prefix' => 'payment'], function () use ($router) {
  // Endpoint: https://api.klon.io/v1/payment/basic
  // Method  : POST
  // Desc    : Receives the incoming post data from the payment form on upgrade.html within the extension
  $router->get('/{email}', 'StripeController@chargeBasicPlan');
  $router->post('/{email}', 'StripeController@getBasicDetails');
});

//
$router->group(['middleware' => 'throttle:50,1'], function () use ($router) {
  $router->get('/', function () use ($router) {
    return redirect('https://klon.io');
    // header('Location:https://klon.io');
    // return $router->app->version();
  });
});
// Version 5.6.4

// $router->get('/', function () use ($router) {
//     return $router->app->version();
// });
$router->group(['prefix' => 'activate'], function () use ($router) {
  $router->get('email/{code}', 'SignupController@activateAccount');
  // $router->get('test', 'SignupController@test');
});

$router->group(['prefix' => 'mail', 'middleware' => 'throttle:120,1'], function () use ($router) {
  // Endpoint: https://api.klon.io/v1/mail/getMessage
  // Method  : POST
  // Data comes from mailgun API
  // Desc    : Receives the incoming email and places it in the SQL table `mail`
  $router->post('receive', 'MessageController@receiveMessage');
});

$router->group(['prefix' => 'mail', 'middleware' => 'throttle:50,1'], function () use ($router) {
  // Enpoint: https://api.klon.io/v1/mail/markSpam
  // Method : POST
  // Example: {token : "...", "sender" : "noreply@twitter.com", "receiver" : "me@klonmail.com", "msgId" : [1,3,4]}
  // Descrip: Mark a specific sender as spam for a particular email
  $router->post('markSpam', 'MessageController@markSpam');

  // Endpoint: https://api.klon.io/v1/mail/markGlobalSpam
  // Method  : POST
  // Example : {token : "...", "sender" : "noreply@twitter.com"}
  // Descrip : Mark a specific sender as spam for all emails
  $router->post('markGlobalSpam', 'MessageController@markGlobalSpam');

  // Endpoint: https://api.klon.io/v1/mail/deleteMail
  // Method  : POST
  // Example : {token : "...", "msgId" : [1,3,4]}
  // Descrip : Mark a specific sender as spam for all emails
  $router->post('deleteMail', 'MessageController@deleteMail');

  // Endpoint: https://api.klon.io/v1/mail/unreadMail
  // Method  : POST
  // Example : {token : "...", "msgId" : [1,3,4]}
  // Descrip : Mark a specific sender as spam for all emails
  $router->post('unreadMail', 'MessageController@unreadMail');

  // Endpoint: https://api.klon.io/v1/mail/notTrash
  // Method  : POST
  // Example : {token : "...", "msgId" : [1,3,4]}
  // Descrip : Mark a specific sender as spam for all emails
  $router->post('notTrash', 'MessageController@notTrash');

  // Endpoint: https://api.klon.io/v1/mail/getMail/{mailbox} (inbox, spam, trash, sent)
  // Method  : POST
  // Example : {token : "...", "sender" : "noreply@twitter.com"}
  // Descrip : Get all mail for the subscriber
  $router->post('getMail/{mailbox}', 'MessageController@getMessages');

  // Endpoint: https://api.klon.io/v1/mail/getMail/{mailbox} (inbox, spam, trash, sent)
  // Method  : POST
  // Example : {token : "...", "sender" : "noreply@twitter.com"}
  // Descrip : Get all mail for the subscriber
  $router->post('checkMail/{mailbox}', 'MessageController@checkMail');

  // Endpoint: https://api.klon.io/v1/mail/notSpam
  // Method  : POST
  // Example : {token : "..."}
  // Descrip : Get the content for the given message id
  $router->post('notSpam', 'MessageController@notSpam');

  // Endpoint: https://api.klon.io/v1/mail/getMessage/{msgId}
  // Method  : POST
  // Example : {token : "..."}
  // Descrip : Get the content for the given message id
  $router->post('getMessage/{msgId}', 'MessageController@getMessageContent');
  $router->post('getMessage/{msgId}/{isSent}', 'MessageController@getMessageContent');

  // Endpoint: https://api.klon.io/v1/mail/sendMail
  // Method  : POST
  // Example : {"to" : "example@mail.com", "from" : "me@host.com", "subject" : "Top secret info", "message" : "idk?", "token" : "..."}
  // Descrip : Get the content for the given message id
  // $router->post('sendMail', 'MailgunController@sendMail');
});

$router->group(['prefix' => 'mail', 'middleware' => 'throttle:4,1'], function() use ($router) {
  // Endpoint: https://api.klon.io/v1/mail/forwardMail
  // Method  : POST
  // Example : {"from" : "sjones2353@klonmail.com", "subject" : "Top secret info", "message" : "idk?", "token" : "..."}
  // Descrip : Get the content for the given message id
  $router->post('forwardMail', 'MailgunController@forwardMail');

  // Endpoint: https://api.klon.io/v1/mail/replyMail
  // Method  : POST
  // Example : {"mailFrom" : "sjones2353@klonmail.com", "mailTo" : "support@twitter.com", "mailSubject" : "Top secret info", "mailMessage" : "idk?", "token" : "..."}
  // Descrip : Get the content for the given message id
  $router->post('replyMail', 'MailgunController@replyMail');
});


$router->group(['prefix' => 'klon', 'middleware' => 'throttle:16,1'], function () use ($router) {
  // Endpoint: https://api.klon.io/v1/klon/createCandidateForURL
  // Method  : POST
  // Example : {"payload" : "..."}
  // Descrip : Return a new identity for the current URL, user may or may not select this candidate
  $router->post('createCandidateForURL', 'KlonController@createCandidateForURL');

  // Endpoint: https://api.klon.io/v1/klon/getIdentities
  // Method  : POST
  // Example : {"payload" : "..." } -- contains a JSON object with the user's JWT and a hash of the current identities
  // Descrip : Get all of the identities from the server if the sent hash does not match
  $router->post('getIdentities', 'KlonController@getAllIdentities');
});


/**
*
*  The klon prefix contains functions that have to do with the central idea of the service.
*
*/
$router->group(['prefix' => 'klon', 'middleware' => 'throttle:40,1'], function () use ($router) {

  // $router->post('createCandidateForURL', 'KlonController@createCandidateForURL');

  // Endpoint: https://api.klon.io/v1/klon/getHash
  // Method  : POST
  // Example : {"payload" : "..."} -- contains hash and token
  // Descrip : Get the latest hash for the users identities
  $router->post('getHash', 'KlonController@getHash');

  // Endpoint: https://api.klon.io/v1/klon/updateHash
  // Method  : PUT
  // Example : {"payload" : "..."} -- contains hash and token
  // Descrip : Update the hash for the users identities
  $router->put('updateHash', 'KlonController@updateHash');

  // Endpoint: https://api.klon.io/v1/klon/useCandidateForURL
  // Method  : POST
  // Example : {"payload" : "..."} -- contains a JSON object of the identity to use
  // Descrip : Put this identity in the database for later use. This identity counts as 1 identity for the particular URL
  $router->post('useCandidateForURL', 'KlonController@useCandidateForURL');

  // Endpoint: https://api.klon.io/v1/klon/modifyIdentity
  // Method  : PUT
  // Example : {"payload" : "..."}  -- contains a JSON object with the fields to modify and their new values
  // Descrip : Change the value of a given key
  $router->put('modifyIdentity', 'KlonController@modifyIdentity');

  // Endpoint: https://api.klon.io/v1/klon/deleteIdentity
  // Method  : DELETE
  // Example : {"payload" : "..."}  -- contains a JSON object with the fields to modify and their new values
  // Descrip : Change the value of a given key
  $router->delete('deleteIdentity', 'KlonController@deleteIdentity');

  // Endpoint: https://api.klon.io/v1/klon/getIdentity
  // Method  : POST
  // Example : {"payload" : "..."} -- contains a JSON object with the id number of the identity requested
  // Descrip : Get a particular identity for the user
  $router->post('getIdentity', 'KlonController@getIdentity');

  // Endpoint: https://api.klon.io/v1/klon/getIdentity
  // Method  : POST
  // Example : {"payload" : "..."} -- contains a JSON object with the id number of the identity requested
  // Descrip : Get a particular identity for the user
  $router->post('getIdentityByProperty', 'KlonController@getIdentityByProperty');

  // Endpoint: https://api.klon.io/v1/klon/updateIdentities
  // Method  : POST
  // Example : {"payload" : "..."}
  // Descrip :
  // $router->put('updateIdentities', 'KlonController@updateIdentities');

  // Endpoint: https://api.klon.io/v1/klon/markGlobalSpam
  // Method  : POST
  // Example : {token : "...", "sender" : "noreply@twitter.com"}
  // Descrip : Mark a specific sender as spam for all emails
  $router->post('attempt', 'AuthController@addLoginAttempt');
});


$router->group(['prefix' => 'subscriber', 'middleware' => 'throttle:50,1'], function () use ($router) {
  // klon.io/subscriber/hashpassword
  // $router->get('hashpassword/{password}', 'SubscribersController@hashPassword');
  //
  //
  // Endpoint: https://api.klon.io/v1/subscriber/create
  // Method  : POST
  // Example : {"email" : bob@mail.com, "password" : "something"}
  // Descrip : Creates an account for the user
  $router->post('create', 'SignupController@createAccount');

  // Endpoint: https://api.klon.io/v1/subscriber/isNew
  // Method  : POST
  // Example : {"token" : "a7d6f5c9e8a...9fbcbde34db92df1"} --> format it and wrap in payload
  // Descrip : Determines if the user is new or not
  $router->post('isNew', 'SubscribersController@isNew');

  // Endpoint: https://api.klon.io/v1/subscriber/getPubkey
  // Method  : GET
  // Example : N/A
  // Descrip : Retrieves the public key for the server
  $router->get('getPubKey', 'EncryptController@getPublicKey');

  // Endpoint: https://api.klon.io/v1/subscriber/updateCard
  // Method  : POST
  // Example : N/A
  // Descrip : Update the users credit card based on their token
  $router->post('updateCard', 'StripeController@showUpdateCreditCard');

  // Endpoint: https://api.klon.io/v1/subscriber/updateCard
  // Method  : POST
  // Example : N/A
  // Descrip : Update the users credit card based on their token
  $router->post('attemptUpdateCard', 'StripeController@updateCreditCard');

  // Endpoint: https://api.klon.io/v1/subscriber/getSettings
  // Method  : POST
  // Example : {"token" : "a2df4c32de6fd4a45c421ac..."}
  // Descrip : Retrieves the users settings from the server
  $router->post('getSettings', 'AuthController@getSettings');

  // Endpoint: https://api.klon.io/v1/subscriber/success
  // Method  : GET
  // Example : N/A
  // Descrip : Displays the success page
  $router->get('success', 'AuthController@success');

  // Endpoint: https://api.klon.io/v1/subscriber/expire
  // Method  : POST
  // Example : {"token" : "a2df4c32de6fd4a45c421ac..."}
  // Descrip : Get the users account expiration time
  $router->post('expire', 'SubscribersController@getExpirationTime');

  // Endpoint: https://api.klon.io/v1/subscriber/getSubscriptions
  // Method  : POST
  // Example : {"token" : "a2df4c32de6fd4a45c421ac..."}
  // Descrip : Get the users subscriptions
  $router->post('getSubscriptions', 'StripeController@getSubscriptionObj');

  // Endpoint: https://api.klon.io/v1/subscriber/cancel
  // Method  : POST
  // Example : {"token" : "a2df4c32de6fd4a45c421ac..."}
  // Descrip : Cancel the subscription
  // $router->post('cancel', 'StripeController@cancelSubscription');
  //

  // Endpoint: https://api.klon.io/v1/subscriber/autoRenew
  // Method  : POST
  // Example : {"token" : "a2df4c32de6fd4a45c421ac..."}
  // Descrip : Auto renew the account
  $router->post('autoRenew', 'StripeController@toggleAutoRenew');

  // Endpoint: https://api.klon.io/v1/subscriber/changePlan
  // Method  : POST
  // Example : {"token" : "a2df4c32de6fd4a45c421ac...", "subscriptionId" : "" "newPlan" : ""}
  // Descrip : Change the users subscriptions
  $router->post('changePlan', 'StripeController@changePlan');
});

// $router->group(['prefix' => 'password', 'middleware' => 'throttle:50,1'], function() use ($router) {
//   $router->post('generate/', 'PasswordController@generatePassword');
//   $router->post('generate/{length}', 'PasswordController@generatePassword');
// });

// $router->group(['prefix' => 'name', 'middleware' => 'throttle:50,1'], function() use ($router) {
  // $router->get('first', 'NameController@getFirstName');
  // $router->get('last', 'NameController@getLastName');
  // TODO: Swap to POST. POST the user credentials to get reponse upon success
//   $router->get('getname', 'NameController@getName');
// });

/**
 * * * * * * * * * * * * * * * * * * *
 *
 *  Authentication   Block
 *
 * * * * * * * * * * * * * * * * * * *
 */
$router->group(['prefix' => 'auth', 'middleware' => 'throttle:50,1'], function() use ($router) {
  //
  //
  //
  //
  $router->get('signup', 'SignupController@createAccount');

  // Endpoint: https://api.klon.io/v1/auth/getRole
  // Method  : POST
  // Example : {token : "..."}
  // Descrip : Get the users role from the server
  $router->post('getRole', 'AuthController@getRole');

  // Endpoint: https://api.klon.io/v1/auth/getRole
  // Method  : POST
  // Example : {token : "..."}
  // Descrip : Get the users role from the server
  $router->post('isnew', 'AuthController@getRole');

  // ** DEPRICATED **
  // Endpoint: https://api.klon.io/v1/auth/getToken
  // Method  : POST
  // Example : {email : "jdoe@protonmail.com", "password" : "l4h3ffeda134fe12...24cc3297ba241"}
  // Descrip : Returns a token for a given user
  // $router->post('getToken', 'AuthController@getToken');

  //
  //
  //
  //
  $router->post('checkToken', 'AuthController@checkToken');

  //
  //
  //
  //
  $router->post('login', 'AuthController@login');
});

// Testing DELETE before production
// $router->group(['prefix' => 'test'], function() use ($router) {
//   $router->post('gun', 'MailgunController@addForwardingAddress');
//   $router->post('test', 'MailgunController@test');
// });
